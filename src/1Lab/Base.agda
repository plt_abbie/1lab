{-# OPTIONS --rewriting #-}

import Agda.Primitive

module 1Lab.Base where

open Agda.Primitive public

-- Our default equality is heterogeneous ("John Major"). This makes
-- characterisation of dependent equalities (e.g. Functor≡) simpler.
data _≡_ {ℓ : _} {A : Set ℓ} (a : A) : {B : Set ℓ} → B → Set ℓ where
  refl : a ≡ a

-- John Major equality restricted to the homogeneous case.
_≡!_ : {ℓ : _} {A : Set ℓ} → A → A → Set ℓ
a ≡! b = a ≡ b

-- Martin-Löf equality is used exclusively as a rewriting relation,
-- hence stealing ↝. It's used in two places:
-- - rewrite clauses in function LHSes
-- - rewrite rules.
data _↝_ {ℓ : _} {A : Set ℓ} (a : A) : A → Set ℓ where
  refl : a ↝ a
{-# BUILTIN EQUALITY _↝_ #-}

-- Convert a homogeneous JMEq to a rewriting equality.
→rewrite : {ℓ : _} {A : Set ℓ} {x y : A} → x ≡! y → x ↝ y
→rewrite refl = refl

transport : {ℓ : _} {A B : Set ℓ} → A ≡! B → A → B
transport refl x = x

transportConst : {ℓ : _} {A B : Set ℓ} (p : A ≡! B) (x : A) → x ≡ transport p x
transportConst refl x = refl

record Σ {ℓ ℓ'} (A : Set ℓ) (B : A → Set ℓ') : Set (ℓ ⊔ ℓ') where
  constructor _,_
  field
    fst : A
    snd : B fst

record _×_ {ℓ ℓ'} (A : Set ℓ) (B : Set ℓ') : Set (ℓ ⊔ ℓ') where
  constructor _,_
  field
    fst : A
    snd : B

infixr 5 _,_
infixr 1 _≡_ _↝_ _≡!_

×Path : {ℓ ℓ' : _} {A : Set ℓ} {A' : Set ℓ} {B : Set ℓ'} {B' : Set ℓ'} {x : A × B} {y : A' × B'}
      → _×_.fst x ≡ _×_.fst y
      → _×_.snd x ≡ _×_.snd y
      → x ≡ y
×Path {x = .(_×_.fst y) , .(_×_.snd y)} {y = y} refl refl = refl

{-# BUILTIN REWRITE _↝_ #-}

lhs : {ℓ : _} {A B : Set ℓ} {x : A} {y : B} → x ≡ y → A
lhs {x = x} p = x

rhs : {ℓ : _} {A B : Set ℓ} {x : A} {y : B} → x ≡ y → B
rhs {y = y} p = y

sym : {ℓ : _} {A B : Set ℓ} {x : A} {y : B} → x ≡ y → y ≡ x
sym refl = refl

sym! : {ℓ : _} {A : Set ℓ} {x y : A} → x ↝ y → y ↝ x
sym! refl = refl

_∘p_ : {ℓ : _} {A : Set ℓ} {x y z : A} → x ≡ y → y ≡ z → x ≡ z
refl ∘p refl = refl

infixr 5 _∘p_

_∘!_ : {ℓ : _} {A : Set ℓ} {x y z : A} → x ↝ y → y ↝ z → x ↝ z
refl ∘! refl = refl

infix  1 begin_
infixr 2 _≡⟨⟩_ _≡⟨_⟩_ _≡⟨⟩!_ _≡⟨_⟩!_
infix  3 _∎

begin_ : {ℓ : _} {A : Set ℓ} {x y : A} → x ≡ y → x ≡ y
begin x≡y = x≡y

_≡⟨⟩_ : {ℓ : _} {A B : Set ℓ} (x : A) {y : B} → x ≡ y → x ≡ y
x ≡⟨⟩ x≡y = x≡y

_≡⟨⟩!_ : {ℓ : _} {A : Set ℓ} (x : A) {y : A} → x ≡ y → x ≡ y
x ≡⟨⟩! x≡y = x≡y

_≡⟨_⟩_ : {ℓ : _} {A B C : Set ℓ} (x : A) {y : B} {z : C} → x ≡ y → y ≡ z → x ≡ z
x ≡⟨ refl ⟩ refl = refl

_≡⟨_⟩!_ : {ℓ : _} {A : Set ℓ} (x : A) {y z : A} → x ≡ y → y ≡ z → x ≡ z
x ≡⟨ refl ⟩! refl = refl

_∎ : {ℓ : _} {A : Set ℓ} (x : A) → x ≡ x
x ∎ = refl

ap : {ℓ ℓ' : _} {A : Set ℓ} {B : Set ℓ'} (f : A → B) {x y : A} → x ≡ y → f x ≡ f y
ap f refl = refl

ap₂ : {ℓ ℓ₁ ℓ₂ : _}
      {A : Set ℓ} {B : Set ℓ₁} {C : Set ℓ₂} (f : A → B → C)
      {w x : A}
      {y z : B}
    → w ≡ x → y ≡ z → f w y ≡ f x z
ap₂ f refl refl = refl

hap : {ℓ ℓ' : _} {A : Set ℓ} {B : A → Set ℓ'} (f : (x : A) → B x) {x y : A} → x ≡ y → f x ≡ f y
hap f refl = refl

happly : {ℓ ℓ' : _} {A : Set ℓ} {B : A → Set ℓ'} {f g : (x : A) → B x} (p : f ≡ g) (x : A) → f x ≡ g x
happly refl x = refl

subst : {ℓ ℓ' : _} {A : Set ℓ} (P : A → Set ℓ') {x y : A} → x ≡ y → P x → P y
subst P refl x = x

substConst : {ℓ ℓ' : _} {A : Set ℓ} {P : A → Set ℓ'} {x y : A} {p : x ≡ y} {e : P x}
           → subst P p e ≡ e
substConst {p = refl} = refl

UIP : {ℓ : _} {A : Set ℓ} {B : Set ℓ} {x : A} {y : B} (p q : x ≡ y) → p ≡ q
UIP refl refl = refl

UIP! : {ℓ : _} {A : Set ℓ} {x y : A} (p q : x ↝ y) → p ≡ q
UIP! refl refl = refl

Σ-Path : {ℓ ℓ' : _} {A : Set ℓ} {B : A → Set ℓ'} {x y : Σ A B}
       → (p : Σ.fst x ≡ Σ.fst y)
       → Σ.snd x ≡ Σ.snd y
       → x ≡ y
Σ-Path refl refl = refl

Σ-Path! : {ℓ ℓ' : _} {A : Set ℓ} {B : A → Set ℓ'} {x y : Σ A B}
        → (p : Σ.fst x ≡! Σ.fst y)
        → subst B p (Σ.snd x) ≡! Σ.snd y
        → x ≡! y
Σ-Path! refl refl = refl

record isContr {ℓ : _} (A : Set ℓ) : Set ℓ where
  constructor contract
  field
    center : A
    paths : (y : A) → center ≡! y

data Bot {ℓ : _} : Set ℓ where

absurd : {ℓ' ℓ : _} {A : Set ℓ'} → Bot {ℓ} → A
absurd ()

record Top {ℓ : _} : Set ℓ where
  constructor tt

data Bool {ℓ : _} : Set ℓ where
  tt ff : Bool

data _∐_ {ℓ ℓ' : _} (A : Set ℓ) (B : Set ℓ') : Set (ℓ ⊔ ℓ') where
  left  : A → A ∐ B
  right : B → A ∐ B

record Lift {ℓ ℓ' : _} (A : Set ℓ) : Set (ℓ ⊔ ℓ') where
  constructor lift
  field
    unlift : A
