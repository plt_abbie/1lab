open import 1Lab.Function.Properties
open import 1Lab.Data.Decidable
open import 1Lab.HProp
open import 1Lab.Coeq
open import 1Lab.Base

module 1Lab.Isomorphism where

infixr 1 _≃_ _≃!_

-- Isomorphism of Sets.
record _≃_ {ℓ ℓ' : _} (A : Set ℓ) (B : Set ℓ') : Set (ℓ ⊔ ℓ') where
  constructor iso
  field
    to   : A → B
    from : B → A

    fromTo : (x : A) → from (to x) ≡ x
    toFrom : (x : B) → to (from x) ≡ x

-- Isomorphism of sets at the same level (compare with ≡! being the
-- homogeneous version of ≡).
_≃!_ : {ℓ : _} (A : Set ℓ) (B : Set ℓ) → Set ℓ
A ≃! B = A ≃ B

-- Isomorphisms have inverses
_≃¯¹ : {ℓ ℓ₁ : _} {A : Set ℓ} {B : Set ℓ₁}
     → A ≃ B
     → B ≃ A
iso to from fromTo toFrom ≃¯¹ = iso from to toFrom fromTo

-- And isomorphisms compose.
_≃∘_ : {ℓ ℓ₁ ℓ₂ : _} {A : Set ℓ} {B : Set ℓ₁} {C : Set ℓ₂}
     → B ≃ C → A ≃ B → A ≃ C
iso to from fromTo toFrom ≃∘ iso to₁ from₁ fromTo₁ toFrom₁ =
  iso (λ z → to (to₁ z)) (λ z → from₁ (from z)) (λ x → happly p x) (λ x → happly q x)
  where
    _∘_ : {ℓ ℓ₁ ℓ₂ : _} {A : Set ℓ} {B : Set ℓ₁} {C : Set ℓ₂} → (B → C) → (A → B) → A → C
    (f ∘ g) x = f (g x)
    infixr 7 _∘_

    id : {ℓ : _} {A : Set ℓ} → A → A
    id x = x

    p = from₁ ∘ from ∘ to ∘ to₁   ≡⟨ refl ⟩
        from₁ ∘ (from ∘ to) ∘ to₁ ≡⟨ ap (λ e → from₁ ∘ e ∘ to₁) (fun-ext fromTo) ⟩
        from₁ ∘ to₁               ≡⟨ fun-ext fromTo₁ ⟩
        _ ∎

    q = to ∘ to₁ ∘ from₁ ∘ from   ≡⟨ refl ⟩
        to ∘ (to₁ ∘ from₁) ∘ from ≡⟨ ap (λ e → to ∘ e ∘ from) (fun-ext toFrom₁) ⟩
        to ∘ from                 ≡⟨ fun-ext toFrom ⟩
        _                         ∎

-- Contractibility is closed under isomorphism
isContr-≃ : {ℓ ℓ' : _} {A : Set ℓ} {B : Set ℓ'}
          → isContr A
          → A ≃ B
          → isContr B
isContr-≃ (contract center paths) (iso to from fromTo toFrom) = contract (to center) paths' where
  paths' : (y : _) → to center ≡ y
  paths' y =
    to center             ≡⟨ sym (toFrom _) ⟩
    to (from (to center)) ≡⟨ ap to (fromTo _ ∘p paths _) ⟩
    to (from y)           ≡⟨ toFrom _ ⟩
    _ ∎

-- And all contractible types are isomorphic
isContr→≃ : {ℓ ℓ' : _} {A : Set ℓ} {B : Set ℓ'}
          → isContr A
          → isContr B
          → A ≃ B
isContr→≃ (contract ac ap) (contract bc bp) = iso (λ _ → bc) (λ _ → ac) ap bp

PathP≃Path : {ℓ ℓ' : _} {A : Set ℓ} {B : Set ℓ} {x : A} {y : B} (p : A ≡! B)
           → (transport p x ≡! y) ≃ (x ≡ y)
PathP≃Path refl = iso (λ z → z) (λ z → z) (λ x → refl) (λ x → refl)

idIso : {ℓ : _} {A : Set ℓ} → A ≃ A
idIso = iso (λ z → z) (λ z → z) (λ x → refl) (λ x → refl)

Discrete-≃ : {ℓ ℓ' : _} {A : Set ℓ} {B : Set ℓ'}
           → A ≃ B
           → Discrete A
           → Discrete B
Discrete-≃ (iso to from fromTo toFrom) disc x y with disc (from x) (from y)
... | yes p =
  let p' = ap to p
   in yes (sym (toFrom _) ∘p (p' ∘p toFrom _))
... | no ¬p = no λ p → absurd (¬p (ap from p))

bijection→isomorphism : {ℓ ℓ' : _} {A : Set ℓ} {B : Set ℓ'} {f : A → B}
                      → bijection f
                      → A ≃ B
bijection→isomorphism {A = A} {B} {f} (injection , surjection) = r where
  open _≃_

  unique-fibers : (y : B)
                → (f1 f2 : Σ A λ x → f x ≡ y)
                → f1 ≡ f2
  unique-fibers f (g , Fg≡f) (g' , Fg'≡f) =
    Σ-Path! (injection (Fg≡f ∘p sym Fg'≡f)) (UIP _ _)

  choose : (y : B) → Σ A λ x → f x ≡ y
  choose f = unsquash' (λ _ → record { carrier = _ ; prop = unique-fibers f })
                       (λ x → x) (surjection f)

  r : A ≃ B
  to r = f
  from r x = Σ.fst (choose x)
  fromTo r x = ap Σ.fst (unique-fibers (f x) (choose (f x)) (x , refl))
  toFrom r x = Σ.snd (choose x)
