open import 1Lab.Base

import 1Lab.Coeq.Base
import 1Lab.Coeq.FunExt

module 1Lab.Coeq where

open 1Lab.Coeq.FunExt public
open 1Lab.Coeq.Base public

rec2 : {a b a′ b′ p : _}
       {A : Set a} {B : Set b} {f g : B → A}
       {A′ : Set a′} {B′ : Set b′} {f′ g′ : B′ → A′}
       {P : Set p}
     → (func : A → A′ → P)
     → ({x : _} {y : _} → func (f x) y ≡ func (g x) y)
     → ({x : _} {y : _} → func x (f′ y) ≡ func x (g′ y))
     → Coeq f g → Coeq f′ g′ → P
rec2 {f = f} {g} {f′ = f′} {g′} func R-r L-r =
  A/R.rec (λ x → B/R'.rec (func x) λ y → L-r)
          λ y → fun-ext λ y → R-r
  where module A/R  = Coequalizer {f = f} {g = g}
        module B/R' = Coequalizer {f = f′} {g = g′}