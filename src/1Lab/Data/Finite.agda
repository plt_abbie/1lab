open import 1Lab.Isomorphism
open import 1Lab.Base

open import Agda.Builtin.Nat

module 1Lab.Data.Finite where

open Agda.Builtin.Nat public

_^_ : Nat → Nat → Nat
n ^ zero = suc zero
n ^ suc k = n * (n ^ k)

data Fin : Nat → Set where
  fzero : {n : _} → Fin (suc n)
  fsuc  : {n : _} → Fin n → Fin (suc n)

Finite : {ℓ : _} → Set ℓ → Set ℓ
Finite {ℓ} A = Σ Nat λ n → A ≃ Fin n
