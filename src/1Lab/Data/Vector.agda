open import 1Lab.Data.Finite
open import 1Lab.Isomorphism
open import 1Lab.Base

module 1Lab.Data.Vector where

data Vect {ℓ} (A : Set ℓ) : Nat → Set ℓ where
  nil : Vect A zero
  _::_ : {n : _} → A → Vect A n → Vect A (suc n)

infixr 40 _::_

_!!_ : {ℓ : _} {A : Set ℓ} {n : _} → Vect A n → Fin n → A
x :: xs !! fzero = x
x :: xs !! fsuc i = xs !! i

enumerate : {ℓ : _} {A : Set ℓ} → (f : Finite A) → Vect A (Σ.fst f)
enumerate {A = A} (A-size , A-iso) = go A-size (λ x → x) where
  go : (n : Nat) → (Fin n → Fin A-size) → Vect A n
  go zero f = nil
  go (suc n) f = (_≃_.from A-iso (f fzero)) :: (go n λ x → f (fsuc x))

v-map : {ℓ ℓ' : _} {A : Set ℓ} {B : Set ℓ'} {n : _}
      → (A → B)
      → Vect A n
      → Vect B n
v-map f nil = nil
v-map f (x :: xs) = (f x) :: (v-map f xs)

v-map-id : {ℓ : _} {A : Set ℓ} {n : _}
         → (xs : Vect A n)
         → v-map (λ x → x) xs ≡ xs
v-map-id nil = refl
v-map-id (x :: xs) = ap (_::_ x) (v-map-id xs)

v-map-comp : {ℓ ℓ' ℓ'' : _} {A : Set ℓ} {B : Set ℓ'} {C : Set ℓ''}
             {f : B → C} {g : A → B} {n : _}
           → (xs : Vect A n)
           → v-map (λ x → f (g x)) xs ≡ v-map f (v-map g xs)
v-map-comp nil = refl
v-map-comp (x :: xs) = ap (_::_ _) (v-map-comp xs)
