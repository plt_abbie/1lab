open import 1Lab.Base

module 1Lab.Data.Decidable where

data Dec {ℓ : _} (P : Set ℓ) : Set ℓ where
  yes : P → Dec P
  no : (P → Bot {ℓ}) → Dec P

Discrete : {ℓ : _} → Set ℓ → Set ℓ
Discrete A = (x y : A) → Dec (x ≡! y)
