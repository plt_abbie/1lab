open import 1Lab.Base

module 1Lab.Data.List where

data List {ℓ : _} (A : Set ℓ) : Set ℓ where
  nil  : List A
  cons : A → List A → List A

map : {ℓ ℓ' : _} {A : Set ℓ} {B : Set ℓ'} → (A → B) → List A → List B
map f nil = nil
map f (cons x xs) = cons (f x) (map f xs)

_++_ : {ℓ : _} {A : Set ℓ} → List A → List A → List A
nil ++ ys = ys
cons x xs ++ ys = cons x (xs ++ ys)

++-id-left : {ℓ : _} {A : Set ℓ} (x : List A) → x ≡ x ++ nil
++-id-left nil = refl
++-id-left (cons x xs) = ap (cons x) (++-id-left xs)

++-assoc : {ℓ : _} {A : Set ℓ} (x y z : List A) → x ++ (y ++ z) ≡ (x ++ y) ++ z
++-assoc nil y z = refl
++-assoc (cons x x₁) y z rewrite →rewrite (++-assoc x₁ y z) = refl

map-++ : {ℓ ℓ' : _} {A : Set ℓ} {B : Set ℓ'} {f : A → B} (x y : List A)
       → map f (x ++ y) ≡ map f x ++ map f y
map-++ nil y = refl
map-++ {f = f} (cons x x₁) y = ap (cons (f x)) (map-++ x₁ y)

map-id : {ℓ : _} {A : Set ℓ} (x : List A)
       → map (λ x → x) x ≡ x
map-id nil = refl
map-id (cons x x₁) = ap (cons x) (map-id x₁)

map-comp : {ℓ ℓ' ℓ'' : _}
           {A : Set ℓ} {B : Set ℓ'} {C : Set ℓ''}
           {f : B → C} {g : A → B} (x : List A)
         → map (λ x → f (g x)) x ≡ map f (map g x)
map-comp nil = refl
map-comp (cons x x₁) = ap (cons _) (map-comp x₁)