open import 1Lab.Isomorphism.Instances
open import 1Lab.Isomorphism.Reasoning
open import 1Lab.Data.Decidable
open import 1Lab.Isomorphism
open import 1Lab.Coeq
open import 1Lab.Base

import 1Lab.Data.Finite

import Agda.Builtin.Nat

module 1Lab.Data.Finite.Closure where

open Agda.Builtin.Nat public
open 1Lab.Data.Finite public

Fin-suc : {n : Nat} → Fin (suc n) ≃! Top ∐ Fin n
_≃_.to (Fin-suc {n}) fzero = left tt
_≃_.to (Fin-suc {n}) (fsuc x) = right x
_≃_.from (Fin-suc {n}) (left x) = fzero
_≃_.from (Fin-suc {n}) (right x) = fsuc x
_≃_.fromTo (Fin-suc {n}) fzero = refl
_≃_.fromTo (Fin-suc {n}) (fsuc x) = refl
_≃_.toFrom (Fin-suc {n}) (left x) = refl
_≃_.toFrom (Fin-suc {n}) (right x) = refl

-- Coproduct of finite sets is the same thing as addition - This is
-- shown by induction on n. 
Fin-coprod : {n m : Nat} → Fin n ∐ Fin m ≃! Fin (n + m)
Fin-coprod {zero} = iso (λ {(right x) → x}) right (λ { (right x) → refl }) (λ x → refl)
Fin-coprod {suc n} {m} =
  Fin (suc n) ∐ Fin m   ≃⟨ ∐-left Fin-suc ⟩
  (Top ∐ Fin n) ∐ Fin m ≃⟨ ∐-assoc ⟩
  Top ∐ (Fin n ∐ Fin m) ≃⟨ ∐-right Fin-coprod ⟩
  Top ∐ (Fin (n + m))   ≃⟨ Fin-suc ≃¯¹ ⟩
  Fin (suc (n + m))     ≃∎

-- Product of finite sets is the same thing as multiplication - This is
-- shown by induction on n.
Fin-prod : {n m : Nat} → Fin n × Fin m ≃ Fin (n * m)
Fin-prod {n = zero} = iso (λ {()}) (λ {()}) (λ {()}) λ{()}
Fin-prod {n = suc n} {m = m} = 
  Fin (suc n) × Fin m             ≃⟨ ×-left Fin-suc ⟩
  (Top ∐ Fin n) × Fin m           ≃⟨ ∐-×-distrib ⟩
  (Top × Fin m) ∐ (Fin n × Fin m) ≃⟨ ∐-both Set-left-unitor Fin-prod ⟩
  Fin m ∐ (Fin (_*_ n m))         ≃⟨ Fin-coprod ⟩
  Fin (m + (_*_ n m))             ≃∎

-- The set of functions between [m] and [n] is finite, and has
-- cardinality nᵐ. This is shown by induction on the size of the domain -
-- since that's how exponentiation is defined.
Fin-fun : {n m : Nat} → (Fin m → Fin n) ≃ Fin (n ^ m)
Fin-fun {m = zero} = iso (λ _ → fzero) (λ _ ()) (λ x → fun-ext (λ ())) λ { fzero → refl }
Fin-fun {n} {m = suc m} =
  (Fin (suc m) → Fin n)   ≃⟨ step ⟩
  Fin n × (Fin m → Fin n) ≃⟨ ×-right Fin-fun ⟩
  Fin n × Fin (n ^ m)     ≃⟨ Fin-prod ⟩
  Fin (n * (n ^ m))       ≃∎ 
  where
    open _≃_
    -- For this intermediate step, we show that [m+1]→[n] picks out an
    -- element of [n] - the image of fzero - and still has a way of
    -- picking [m] elements of [n].
    step : (Fin (suc m) → Fin n) ≃ Fin n × (Fin m → Fin n)
    to step f = f fzero , λ x → f (fsuc x)

    from step (fst , _) fzero = fst
    from step (_ , snd) (fsuc i) = snd i

    fromTo step x = fun-ext λ { fzero    → refl
                              ; (fsuc x) → refl }
    toFrom step (fst , snd) = refl

-- The canonical finite sets are discrete. Discreteness is preserved
-- under equivalence, so any b-finite set is also discrete.
Fin-discrete : {n : _} → Discrete (Fin n)
Fin-discrete fzero fzero = yes refl
Fin-discrete fzero (fsuc b) = no λ { () }
Fin-discrete (fsuc a) fzero = no λ { () }
Fin-discrete (fsuc a) (fsuc b) with Fin-discrete a b
... | yes p = yes (ap fsuc p)
... | no p = no λ { refl → p refl }

-- Decidable subsets of finite sets are finite
Fin-subset-fin : {ℓ : _} {n : _}
                 (P : Fin n → Set ℓ)
               → ({a : Fin n} (x y : P a) → x ≡! y)
               → ((x : Fin n) → Dec (P x))
               → Finite (Σ (Fin n) P)
-- In case the larger set is empty we don't need to do anything
Fin-subset-fin {n = zero} P prop dec = zero , iso Σ.fst (λ ()) (λ { () }) λ ()

-- Otherwise we decide whether the 0th element of the larger is in the
-- subset (whether P fzero) and inductively get finiteness for P (fsuc
-- ...)
Fin-subset-fin {n = suc n} P prop dec with dec fzero | Fin-subset-fin {n = n} (λ x → P (fsuc x)) prop (λ x → dec (fsuc x))
-- if it is in the subset then we have to build an isomorphism between
-- the finite subset of the rest of the set and the first element
-- (fzero). recall that Fin (suc n) ≃ 1 ∐ Fin n; We have the subset of
-- Fin n, and we know (left *) is also in the set, so we have to shift
-- all of our Fin n s to the right by one.
... | yes hier | sssize , morp = suc sssize , morp' where
  open _≃_
  morp' : (Σ (Fin (suc n)) P) ≃ Fin (suc sssize)
  to morp' (fzero , snd) = fzero
  to morp' (fsuc fst , snd) = fsuc (to morp (fst , snd))

  from morp' fzero = fzero , hier
  from morp' (fsuc x) with from morp x
  ... | fst , snd = fsuc fst , snd

  fromTo morp' (fzero , snd) = Σ-Path! refl (prop hier snd)
  fromTo morp' (fsuc fst , snd) = Σ-Path (ap (λ x → fsuc (Σ.fst x)) p) (hap Σ.snd p)
    where p = fromTo morp (fst , snd)

  toFrom morp' fzero = refl
  toFrom morp' (fsuc x) = ap fsuc (toFrom morp x)

-- Otherwise it's not in the set. the size of the subset stays the same,
-- and the isomorphism witnesses 0 ∐ Fin n ≃ Fin n, by eliminating all
-- the 0s away.
... | no ¬here | fst , morp = fst , morp' where
  open _≃_
  shift : Σ (Fin n) (λ x₁ → P (fsuc x₁)) → Σ (Fin (suc n)) P
  shift (fst , snd) = fsuc fst , snd

  morp' : (Σ (Fin (suc n)) P) ≃ Fin fst
  to morp' (fzero , snd) = absurd (¬here snd)
  to morp' (fsuc fst , snd) = to morp (fst , snd)

  from morp' x = shift (from morp x)

  fromTo morp' (fzero , snd) = absurd (¬here snd)
  fromTo morp' (fsuc fst , snd) = Σ-Path (ap (λ x → fsuc (Σ.fst x)) p) (hap Σ.snd p)
    where p = fromTo morp (fst , snd)
  toFrom morp' = toFrom morp

-- The same, but for B-finite sets instead of canonically finite sets.
Finite-subset-finite
  : {ℓ ℓ' : _} {A : Set ℓ'}
  → Finite A
  → (P : A → Set ℓ)
  → ({a : A} (x y : P a) → x ≡! y)
  → ((x : A) → Dec (P x))
  → Finite (Σ A P)
Finite-subset-finite {A = A} (size , fin) P prop dec = fixup (Fin-subset-fin (λ x → P (_≃_.from fin x)) prop dec') where
  dec' : (x : Fin size) → Dec (P (_≃_.from fin x))
  dec' x with dec (_≃_.from fin x)
  ... | yes p = yes p
  ... | no ¬p = no λ x → ¬p x

  fixup : _ → _
  fixup (fst , morp) = fst , morp' where
    cast : Σ (Fin size) (λ x → P (_≃_.from fin x)) → Σ A P
    cast (fst , snd) = _≃_.from fin fst , snd

    morp' : Σ A P ≃ Fin fst
    _≃_.to morp' (fst , snd) = _≃_.to morp ( _≃_.to fin fst
                                           , subst P (sym (_≃_.fromTo fin _)) snd
                                           )

    _≃_.from morp' idx = cast (_≃_.from morp idx)

    _≃_.fromTo morp' (fst , snd) = Σ-Path! l (prop _ _) where
      l = ap (λ e → _≃_.from fin (Σ.fst e)) (_≃_.fromTo morp _) ∘p _≃_.fromTo fin _
          
    _≃_.toFrom morp' x =
      ap (λ e → _≃_.to morp e) (Σ-Path! (_≃_.toFrom fin _)
                                        (prop _ (Σ.snd (_≃_.from morp x))))
      ∘p _≃_.toFrom morp _

Finite→Discrete : {ℓ : _} {A : Set ℓ} → Finite A → Discrete A
Finite→Discrete (_ , morp) = Discrete-≃ (morp ≃¯¹) Fin-discrete

instance
  Finite-⊤ : {ℓ : _} → Finite (Top {ℓ})
  Finite-⊤ = 1 , iso (λ x → fzero) (λ x → tt) (λ x → refl) λ { fzero → refl }

  Finite-⊥ : {ℓ : _} → Finite (Bot {ℓ})
  Finite-⊥ = 0 , iso (λ ()) (λ ()) (λ ()) (λ ())

instance
  Finite-Bool : {ℓ : _} → Finite (Bool {ℓ})
  Σ.fst Finite-Bool = 2
  _≃_.to (Σ.snd Finite-Bool) tt = fzero
  _≃_.to (Σ.snd Finite-Bool) ff = fsuc fzero
  _≃_.from (Σ.snd Finite-Bool) fzero = tt
  _≃_.from (Σ.snd Finite-Bool) (fsuc fzero) = ff
  _≃_.fromTo (Σ.snd Finite-Bool) tt = refl
  _≃_.fromTo (Σ.snd Finite-Bool) ff = refl
  _≃_.toFrom (Σ.snd Finite-Bool) fzero = refl
  _≃_.toFrom (Σ.snd Finite-Bool) (fsuc fzero) = refl

finitary : {ℓ : _} {A : Set ℓ} {{_ : Finite A}} → Σ (Set ℓ) Finite
finitary {A = A} {{f}} = A , f

instance
  Finite-→ : {ℓ ℓ' : _} {A : Set ℓ} {B : Set ℓ'}
           → {{_ : Finite A}}
           → {{_ : Finite B}}
           → Finite (A → B)
  Finite-→ {{a-size , ai}} {{b-size , bi}} = (b-size ^ a-size) , morp where
    morp = Fin-fun ≃∘ →-both ai bi

  Finite-× : {ℓ ℓ' : _} {A : Set ℓ} {B : Set ℓ'}
           → {{_ : Finite A}}
           → {{_ : Finite B}}
           → Finite (A × B)
  Finite-× {A = A} {B} {{a-card , a-bij}} {{b-card , b-bij}} = a-card * b-card , morp where
    morp = Fin-prod ≃∘ ×-both a-bij b-bij

  Finite-∐ : {ℓ ℓ' : _} {A : Set ℓ} {B : Set ℓ'}
           → {{_ : Finite A}}
           → {{_ : Finite B}}
           → Finite (A ∐ B)
  Finite-∐ {A = A} {B} {{a-card , a-bij}} {{b-card , b-bij}} = a-card + b-card , morp where
    morp = Fin-coprod ≃∘ ∐-both a-bij b-bij
