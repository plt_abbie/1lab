open import 1Lab.Isomorphism
open import 1Lab.Coeq
open import 1Lab.Base

module 1Lab.Isomorphism.Instances where

∐-both : {ℓ ℓ₁ ℓ₂ ℓ₃ : _} {A : Set ℓ} {B : Set ℓ₁} {C : Set ℓ₂} {D : Set ℓ₃}
       → A ≃ B
       → C ≃ D
       → A ∐ C ≃ B ∐ D
∐-both {A = A} {B} {C} {D} (iso to₁ from₁ fromTo₁ toFrom₁) (iso to₂ from₂ fromTo₂ toFrom₂) = r where
  open _≃_
  r : A ∐ C ≃ B ∐ D
  to r (left x) = left (to₁ x)
  to r (right x) = right (to₂ x)
  from r (left x) = left (from₁ x)
  from r (right x) = right (from₂ x)
  fromTo r (left x) = ap left (fromTo₁ x)
  fromTo r (right x) = ap right (fromTo₂ x)
  toFrom r (left x) = ap left (toFrom₁ x)
  toFrom r (right x) = ap right (toFrom₂ x)

∐-left : {ℓ ℓ₁ ℓ₂ : _} {A : Set ℓ} {B : Set ℓ₁} {C : Set ℓ₂}
       → A ≃ B
       → A ∐ C ≃ B ∐ C
∐-left morp = ∐-both morp idIso

∐-right : {ℓ ℓ₁ ℓ₂ : _} {A : Set ℓ} {B : Set ℓ₁} {C : Set ℓ₂}
        → A ≃ B
        → C ∐ A ≃ C ∐ B
∐-right morp = ∐-both idIso morp

∐-assoc : {ℓ ℓ₁ ℓ₂ : _} {A : Set ℓ} {B : Set ℓ₁} {C : Set ℓ₂}
        → (A ∐ B) ∐ C ≃ A ∐ (B ∐ C)
_≃_.to ∐-assoc (left (left x)) = left x
_≃_.to ∐-assoc (left (right x)) = right (left x)
_≃_.to ∐-assoc (right x) = right (right x)
_≃_.from ∐-assoc (left x) = left (left x)
_≃_.from ∐-assoc (right (left x)) = left (right x)
_≃_.from ∐-assoc (right (right x)) = right x
_≃_.fromTo ∐-assoc (left (left x)) = refl
_≃_.fromTo ∐-assoc (left (right x)) = refl
_≃_.fromTo ∐-assoc (right x) = refl
_≃_.toFrom ∐-assoc (left x) = refl
_≃_.toFrom ∐-assoc (right (left x)) = refl
_≃_.toFrom ∐-assoc (right (right x)) = refl

∐-×-distrib : {ℓ ℓ₁ ℓ₂ : _} {A : Set ℓ} {B : Set ℓ₁} {C : Set ℓ₂}
            → (A ∐ B) × C ≃ (A × C) ∐ (B × C)
_≃_.to ∐-×-distrib (left x , snd) = left (x , snd)
_≃_.to ∐-×-distrib (right x , snd) = right (x , snd)
_≃_.from ∐-×-distrib (left (fst , snd)) = left fst , snd
_≃_.from ∐-×-distrib (right (fst , snd)) = right fst , snd
_≃_.fromTo ∐-×-distrib (left x , snd) = refl
_≃_.fromTo ∐-×-distrib (right x , snd) = refl
_≃_.toFrom ∐-×-distrib (left x) = refl
_≃_.toFrom ∐-×-distrib (right x) = refl

Set-left-unitor : {ℓ ℓ' : _} {A : Set ℓ}
                → Top {ℓ'} × A ≃ A
Set-left-unitor = iso _×_.snd (λ z → tt , z) (λ x → refl) λ x → refl

Set-right-unitor : {ℓ ℓ' : _} {A : Set ℓ}
                 → A × Top {ℓ'} ≃ A
Set-right-unitor = iso _×_.fst (λ z → z , tt) (λ x → refl) (λ x → refl)


×-both : {ℓ ℓ₁ ℓ₂ ℓ₄ : _} {A : Set ℓ} {B : Set ℓ₁} {C : Set ℓ₂} {D : Set ℓ₄}
       → A ≃ B
       → C ≃ D
       → A × C ≃ B × D
×-both {A = A} {B} {C} {D} (iso to from fromTo toFrom) (iso to₁ from₁ fromTo₁ toFrom₁) = r where
  r : A × C ≃ B × D
  _≃_.to r (fst , snd) = to fst , to₁ snd
  _≃_.from r (fst , snd) = from fst , from₁ snd
  _≃_.fromTo r (fst , snd) = ×Path (fromTo fst) (fromTo₁ snd)
  _≃_.toFrom r (fst , snd) = ×Path (toFrom fst) (toFrom₁ snd)

×-left : {ℓ ℓ₁ ℓ₂ : _} {A : Set ℓ} {B : Set ℓ₁} {C : Set ℓ₂}
       → A ≃ B
       → A × C ≃ B × C
×-left morp = ×-both morp idIso

×-right : {ℓ ℓ₁ ℓ₂ : _} {A : Set ℓ} {B : Set ℓ₁} {C : Set ℓ₂}
        → A ≃ B
        → C × A ≃ C × B
×-right morp = ×-both idIso morp

→-both : {ℓ ℓ₁ ℓ₂ ℓ₄ : _} {A : Set ℓ} {B : Set ℓ₁} {C : Set ℓ₂} {D : Set ℓ₄}
       → A ≃ B
       → C ≃ D
       → (A → C) ≃ (B → D)
→-both {A = A} {B} {C} {D} (iso to from fT tF) (iso to₁ from₁ fT₁ tF₁) = r where
  ft : (f : A → C) (x : A) → _
  ft f x =
    from₁ (to₁ _)   ≡⟨ fT₁ _ ⟩
    f (from (to _)) ≡⟨ ap f (fT _) ⟩
    f x             ∎

  tf : (f : B → D) (x : B) → _
  tf f x =
    to₁ (from₁ _)   ≡⟨ tF₁ _ ⟩
    f (to (from _)) ≡⟨ ap f (tF _) ⟩
    f x             ∎

  r : (A → C) ≃ (B → D)
  r = iso (λ z z₁ → to₁ (z (from z₁))) (λ z z₁ → from₁ (z (to z₁)))
          (λ func → fun-ext (ft func))
          λ func → fun-ext (tf func)
