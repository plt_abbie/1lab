open import 1Lab.Isomorphism

module 1Lab.Isomorphism.Reasoning where

infixr 2 _≃⟨⟩_ _≃⟨_⟩_
infix  3 _≃∎

_≃⟨⟩_ : {ℓ ℓ₂ : _} (A : Set ℓ) {B : Set ℓ} → A ≃ B → A ≃ B
x ≃⟨⟩ x≡y = x≡y

_≃⟨_⟩_ : {ℓ ℓ₁ ℓ₂ : _} (A : Set ℓ) {B : Set ℓ₁} {C : Set ℓ₂} → A ≃ B → B ≃ C → A ≃ C
x ≃⟨ i ⟩ j = j ≃∘ i

_≃∎ : {ℓ : _} (A : Set ℓ) → A ≃ A
x ≃∎ = idIso
