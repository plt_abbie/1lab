open import 1Lab.Base

open import Agda.Primitive

open import Category.Structure.FinitelyComplete
open import Category.Structure.CartesianClosed
open import Category.Structure.Cartesian
open import Category

import Category.Diagrams

open Functor
open _=>_

module Topos.Base where

record SubobjectClassifier {o₁ h₁} {C : Category o₁ h₁} (Ca : Cartesian C) : Set (o₁ ⊔ h₁) where
  -- A subobject classifier is an object in a category that serves as
  -- the same purpose that Prop does in Set.
  --
  -- In more detail, it's an object Ω and a distinguished arrow
  --
  --    true : ⊤ ---> Ω
  --
  -- such that, if m : U ↪ X is a subobject inclusion (monomorphism),
  -- then there exists an arrow χ m : X ---> Ω making the square a
  -- pullback:
  --
  --            !
  --       U ───────> ⊤
  --       ╭          │
  --     m │          │ true
  --       V          V
  --       X ───────> Ω
  --           χ m
  --
  -- Commutativity of this diagram means χ m ∘ m = true ∘ ! - If we read
  -- "χ m" as "the characteristic map of m" (as we should), then this
  -- says that the characteristic map is 'true' everywhere in the image
  -- of m.
  private
    module Ca = Cartesian Ca
    module C = Category.Category C
  open Category.Diagrams C

  field
    -- "object of propositions"
    Ω    : C.Ob

    true : C.Hom Ca.⊤ Ω

    -- Characteristic map of a monomorphism
    χ    : {U X : C.Ob} → U ↪ X → C.Hom X Ω

    -- Witness that the square above commutes
    is-pullback : {U X : C.Ob} (h : U ↪ X)
                → Pullback Ca.! (_↪_.map h) true (χ h)

record ElementaryTopos {o₁ h₁} (C : Category o₁ h₁) : Set (lsuc (o₁ ⊔ h₁)) where
  -- An elementary topos is "a nice category to do (intuitionistic)
  -- higher-order logic in". Specifically, it's a category with:
  field
    cartesian           : Cartesian C
    finitelyComplete    : FinitelyComplete C
    -- ^ All finite limits
    -- (a terminal object, binary products, binary pushouts, binary
    -- equalisers - either of pullbacks or equalisers determines the
    -- other, but both are included as a convenience)

    closed              : CartesianClosed cartesian
    -- ^ Closed with respect to the monoidal structure induced by
    -- Cartesian product and ⊤
    -- (for every pair of objects A and B there is an object [ A , B ]
    -- which internalises Hom A B)

    subobjectClassifier : SubobjectClassifier cartesian
    -- ^ With a subobject classifier.
    
    -- The properties above imply that every elementary topos is also
    -- finitely cocomplete (having an initial object, coproducts, binary
    -- coequalizers, and binary pushouts)
    --
    -- Short proof (still todo): the functor [-,Ω] is adjoint to itself
    -- on the right, the adjunction is monadic, monadic adjunctions
    -- create all limits, so E^op is finitely complete; thus E is
    -- finitely cocomplete.
    --
    -- Since this is the case I've taken the liberty of mandating the
    -- existence of ⊥ and binary coproducts, as a convenience.

    cocartesian         : Cartesian (C ^op)

  open Cartesian           cartesian           public
  open CartesianClosed     closed              public
  open SubobjectClassifier subobjectClassifier public
  open FinitelyComplete    finitelyComplete    public
  open Category.Category   C                   public
  
  -- If you open an ElementaryTopos, you get access to all of its
  -- structure, and, in particular, sane names for the colimits in E^op.
  open Cartesian cocartesian
    renaming ( π₁ to inj₁
             ; π₂ to inj₂
             ; _h×_ to case
             ; _×C_ to +C
             ; h×-unique to case-unique
             ; π₁∘h×≡f to case∘inj₁≡f
             ; π₂∘h×≡g to case∘inj₂≡g
             ; ⊤ to ⊥
             ; terminal to initial
             ; is-Terminal to is-Initial
             ; ! to absurd
             ; h×-unique₂ to case-unique₂
             )
    public

-- A logical functor is one that commutes with all of the structure of
-- an elementary topos. In particular it preserves all finite limits,
-- exponentials and the subobject classifier.
module _ {o₁ h₁ o₂ h₂} {C : Category o₁ h₁} {D : Category o₂ h₂} where
  record Logical (E : ElementaryTopos C) (E' : ElementaryTopos D) (F : Functor C D) : Set (o₁ ⊔ h₁ ⊔ o₂ ⊔ h₂) where
    private
      module E = ElementaryTopos E
      module E' = ElementaryTopos E'
      module F = Functor F
    
    open Category.Diagrams D using (_≈_)

    field
      F-terminal            : F.F₀ E.⊤ ≈ E'.⊤

      F-subobjectClassifier : F.F₀ E.Ω ≈ E'.Ω
      F-true                : Σ.fst F-subobjectClassifier E'.∘ F.F₁ E.true
                           ≡! E'.true E'.∘ Σ.fst F-terminal

      F-products : {A B : E.Ob}
                 → F.F₀ (A E.×C B)
                 ≈ (F.F₀ A E'.×C F.F₀ B)
      F-π₁ : {A B : E.Ob}
           →  F.F₁ (E.π₁ {A} {B})
           ≡! E'.π₁ E'.∘ Σ.fst F-products
      F-π₂ : {A B : E.Ob}
           →  F.F₁ (E.π₂ {A} {B})
           ≡! E'.π₂ E'.∘ Σ.fst F-products
      F-h× : {A B C : E.Ob}
           → (f : E.Hom A C) (g : E.Hom A C)
           →  Σ.fst F-products E'.∘ F.F₁ (f E.h× g)
           ≡! F.F₁ f E'.h× F.F₁ g

      -- TODO: aaaaaa
      F-pullbacks : {A B C : E.Ob} {f : E.Hom A C} {g : E.Hom B C}
                  → F.F₀ (E.Pullback-of f g)
                  ≈ E'.Pullback-of (F.F₁ f) (F.F₁ g)

      F-equalisers : {A B : E.Ob} {f g : E.Hom A B}
                   → F.F₀ (E.Equaliser-of f g)
                   ≈ E'.Equaliser-of (F.F₁ f) (F.F₁ g)

      F-exponentials : {A B : E.Ob}
                     → F.F₀ E.[ A , B ]
                     ≈ E'.[ F.F₀ A , F.F₀ B ]
