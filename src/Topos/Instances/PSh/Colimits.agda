open import 1Lab.Coeq
open import 1Lab.Base

open import Category.Structure.Cartesian
open import Category.Equality
open import Category

import Category.Instances.PSh

open Functor
open _=>_

module Topos.Instances.PSh.Colimits {o₁ : _} (C : Category o₁ o₁) where

private
  module C = Category.Category C

open Category.Instances.PSh C

open Cartesian
PSh-Cocartesian : {ℓ : _} → Cartesian (PSh {ℓ} ^op)
PSh-Cocartesian {ℓ} = r where
  module PSh = Category.Category (PSh {ℓ} ^op)
  coprod : PSh.Ob → PSh.Ob → PSh.Ob
  coprod A B =
    record { F₀ = λ coord → F₀ A coord ∐ F₀ B coord
           ; F₁ = λ where
              map (left a) → left (F₁ A map a)
              map (right b) → right (F₁ B map b)
           ; F-id = fun-ext λ where
             (left a) → ap left (happly (F-id A) _)
             (right a) → ap right (happly (F-id B) _)
           ; F-∘ = λ f g → fun-ext λ where
             (left a) → ap left (happly (F-∘ A f g) _)
             (right b) → ap right (happly (F-∘ B f g) _)
           }

  inl : {A B : PSh.Ob} → PSh.Hom (coprod A B) A
  η inl x = left
  is-natural inl x y f = refl

  inr : {A B : PSh.Ob} → PSh.Hom (coprod A B) B
  η inr x = right
  is-natural inr x y f = refl

  r : Cartesian (PSh {ℓ} ^op)
  _×C_ r = coprod
  π₁ r = inl
  π₂ r = inr

  η (_h×_ r f g) x (left x₁) = η f x x₁
  η (_h×_ r f g) x (right x₁) = η g x x₁
  is-natural (_h×_ r f g) x y act = fun-ext λ where
    (left elem) → happly (is-natural f x y act) elem
    (right elem) → happly (is-natural g x y act) elem

  π₁∘h×≡f r f g = NT≡ λ x → refl
  π₂∘h×≡g r f g = NT≡ λ x → refl
  h×-unique r {q} {le} {ri} {f} {g} {h} π₁∘h π₂∘h =
    NT≡ λ ob → fun-ext p
    where
      p : {ob : _} (x : F₀ le ob ∐ F₀ ri ob) → η h ob x ≡ η (_h×_ r f g) ob x
      p {ob} (left x) = ap (λ e → η e ob x) (sym π₁∘h)
      p {ob} (right x) = ap (λ e → η e ob x) (sym π₂∘h)
      
  ⊤ r =
    record { F₀   = λ _ → Bot
            ; F₁   = λ x y → absurd y
            ; F-id = λ {x} → fun-ext λ x → absurd x
            ; F-∘  = λ {x} f g → fun-ext λ x → absurd x
            }
  η (isContr.center (terminal r {A})) x y = absurd {A = F₀ A x} y
  is-natural (isContr.center (terminal r)) _ _ _ = fun-ext λ x → absurd x
  isContr.paths (terminal r) y = NT≡ λ x → fun-ext λ x₁ → absurd x₁
