open import 1Lab.HProp
open import 1Lab.Coeq
open import 1Lab.Base

open import Category.Constructions.Sieve
open import Category.Structure.Cartesian
open import Category.Limit.Pullback
open import Category.Diagrams
open import Category.Equality
open import Category

open import Topos.Base

import Category.Associativity
import Category.Instances.PSh

import Topos.Instances.PSh.Limits
import Topos.Instances.Sets as Sets

open Functor
open _=>_

module Topos.Instances.PSh.Omega {o₁ : _} (C : Category o₁ o₁) where
  private
    module C = Category.Category C
    module Cop = Category.Category (C ^op)
    module AssC = Category.Associativity C

  open Category.Instances.PSh C

  private
    module PSh = Category.Category (PSh {o₁})

  open Topos.Instances.PSh.Limits C

  private
    module Ca = Cartesian (PSh-Cartesian {o₁})

  open SubobjectClassifier
  open Functor

  private
    nt-mono-lemma : {X E : PSh.Ob} {map : PSh.Hom X E}
                    (monic : is-Mono (PSh {o₁}) map)
                  → {o : C.Ob}
                  → is-Mono (Sets _) (η map o)
    nt-mono-lemma {map = map} monic {o} = monic' where
      monic' : is-Mono (Sets _) (F₁ (ev {o₁} o) map)
      monic' = Continuous→Monics {F = ev {o₁} o} (ev-continuous o) monic

  PSh-Omega : SubobjectClassifier (PSh-Cartesian {o₁})
  PSh-Omega = subobj where
    module S = Category.Constructions.Sieve C

    ω : PSh.Ob
    F₀ ω y = S.Sieve y
    F₁ ω map sieve =
      record
        { arrows = λ y f → Sieve.arrows sieve _ (map C.∘ f)
        ; closed = λ {x} {y} {f} g innit → 
            subst (λ e → e ∈ Sieve.arrows sieve x) 
                  (C.assoc₁ _ _ _)
                  (Sieve.closed sieve g innit)
        }

    F-id ω = fun-ext λ x → S.Sieve≡ _ λ _ f →
      ap (Sieve.arrows x _) (C.idl _)

    F-∘ ω f g = fun-ext λ x → S.Sieve≡ _ λ _ f →
      ap (Sieve.arrows x _) AssC.left→right

    tru : PSh.Hom Ca.⊤ ω
    tru = NT (λ x _ → S.maximal x) λ _ _ _ → fun-ext λ _ → S.Sieve≡ _ λ _ _ → refl 

    chi : {X E : PSh.Ob} (map : PSh.Hom X E)
          (monic : is-Mono (PSh {o₁}) map)
        → PSh.Hom E ω
    chi {X} {E} map monic = nat where
      arr : (x : C.Ob) → F₀ E x → S.Sieve x
      arr C e =
        record
          { arrows = λ y f → ∥ (Σ _ λ x → η map _ x ≡! F₁ E f e) ∥
          ; closed = λ g x → x ∥ _ ∥ λ where
            (fst , snd) → squash (F₁ X g fst , happly (is-natural map _ _ _) _
                                            ∘p ap (F₁ E g) snd
                                            ∘p happly (sym (F-∘ E _ _)) e)
          }

      nat = NT arr λ x y f → fun-ext λ x₁ → S.Sieve≡ _ λ x y →
          ∥ Σ (F₀ X x) (λ x₃ → η map x x₃ ≡ F₁ E y (F₁ E f x₁)) ∥ ≡⟨ ap (λ e → ∥ Σ _ e ∥) (fun-ext λ x₃ → ap (_≡!_ (η map x x₃)) (happly (sym (F-∘ E _ _)) x₁)) ⟩
          ∥ Σ (F₀ X x) (λ x₃ → η map x x₃ ≡ F₁ E (f C.∘ y) x₁)  ∥  ∎

    subobj : SubobjectClassifier (PSh-Cartesian {o₁})
    Ω subobj = ω
    true subobj = tru
    χ subobj {X} {E} record { map = map ; monic = monic } = chi map monic
    is-pullback subobj {X} {E} record { map = map ; monic = monic } =
      record
        { commutes = NT≡ λ x → fun-ext λ y → S.Sieve≡ _ λ o f →
            prop-ext (λ x₁ → squash (F₁ X f y , happly (is-natural map _ _ _) _))
                     (λ x₁ → tt)
        ; limiting = λ eq → limit eq
        ; unique = λ eq x mapi≡p₂' → monic (mapi≡p₂' ∘p sym (limit-comm eq))
        ; p₁∘limiting≡p₁' = λ eq → NT≡ λ x → refl
        ; p₂∘limiting≡p₂' = λ eq → limit-comm eq
        }
      where
        module _ {Q : Category.Ob (PSh {o₁})}
                 {p₁' : Q => Ca.⊤}
                 {p₂' : Q => E}
                 (eq : tru PSh.∘ p₁' ≡ chi map monic PSh.∘ p₂')
          where

          prf = ap (λ x y z → S.Sieve.arrows (η x y z)) eq

          rng : (x : C.Ob) (qx : F₀ Q x) → hProp _
          hProp.carrier (rng x qx) =
            Σ (F₀ X x) (λ x₁ → η map x x₁ ≡ F₁ E C.id (η p₂' x qx))
          hProp.prop (rng x qx) (a , snd) (b , snd₁) = Σ-Path! x≡x₁ (UIP _ _)
            where
              x≡x₁ : a ≡! b
              x≡x₁ = Sets.monic→injective (nt-mono-lemma monic) (snd ∘p sym snd₁)

          limit : PSh.Hom Q X
          limit = NT nat (λ x y f → fun-ext (isn x y f)) where
            nat : (x : C.Ob) → F₀ Q x → F₀ X x
            nat x qx = Σ.fst (p (rng x qx) (λ x → x))
              where
              p = transport (ap (λ e → hProp.carrier (e x qx x C.id)) prf) tt

            isn : (x y : C.Ob) (f : C.Hom y x) (e : _)
                → nat y (F₁ Q f e) ≡ F₁ X f (nat x e)
            isn x y f x₁ = goal where
              n_lhs =
                transport (ap (λ e → hProp.carrier (e y (F₁ Q f x₁) y C.id)) prf)
                          tt (rng y (F₁ Q f x₁)) (λ x → x)

              n_rhs =
                transport (ap (λ e → hProp.carrier (e x x₁ x C.id)) prf)
                          tt (rng x x₁) (λ x → x)

              goal : Σ.fst n_lhs ≡! F₁ X f (Σ.fst n_rhs)
              goal =
                Sets.monic→injective {f = η map y} (nt-mono-lemma monic) (
                  Σ.snd n_lhs
                  ∘p happly (F-id E) _
                  ∘p happly (is-natural p₂' _ _ _) _
                  ∘p ap (F₁ E f) (sym (Σ.snd n_rhs ∘p happly (F-id E) _))
                  ∘p sym (happly (is-natural map _ _ _) _)
                )
          
          limit-comm : map PSh.∘ limit ≡! p₂'
          limit-comm = NT≡ λ x → fun-ext λ qx → 
            let
              p = transport (ap (λ e → hProp.carrier (e x qx x C.id)) prf)
                            tt (rng x qx) (λ x → x)
            in Σ.snd p ∘p happly (F-id E) _
