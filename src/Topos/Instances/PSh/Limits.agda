open import 1Lab.Coeq
open import 1Lab.Base

open import Category.Structure.FinitelyComplete
open import Category.Structure.CartesianClosed
open import Category.Structure.Cartesian
open import Category.Equality
open import Category

import Category.Associativity
import Category.Instances.PSh

open Functor
open _=>_

module Topos.Instances.PSh.Limits {o₁ : _} (C : Category o₁ o₁) where
  private
    module C = Category.Category C
    module Cop = Category.Category (C ^op)
    module AssC = Category.Associativity C
  
  open Category.Instances.PSh C

  open Cartesian
  PSh-Cartesian : {ℓ : _} → Cartesian (PSh {ℓ})
  _×C_ PSh-Cartesian A B =
    record { F₀   = λ coord → F₀ A coord × F₀ B coord
           ; F₁   = λ map (a , b) → F₁ A map a , F₁ B map b
           ; F-id = fun-ext λ where
              (a , b) → ×Path (happly (F-id A) _) (happly (F-id B) _)
           ; F-∘  = λ f g → fun-ext λ where
              (a , b) → ×Path (happly (F-∘ A f g) _) (happly (F-∘ B f g) _)
           }
  π₁ PSh-Cartesian = NT (λ x p → _×_.fst p) λ x y f → refl
  π₂ PSh-Cartesian = NT (λ x p → _×_.snd p) λ x y f → refl
  _h×_ PSh-Cartesian f g = NT (λ { x p → η f x p , η g x p })
    λ x y act → fun-ext λ where
    elem → ×Path (happly (is-natural f x y act) elem)
                 (happly (is-natural g x y act) elem)

  π₁∘h×≡f PSh-Cartesian f g = NT≡ λ x → refl
  π₂∘h×≡g PSh-Cartesian f g = NT≡ λ x → refl
  h×-unique PSh-Cartesian π₁∘h π₂∘h =
    NT≡ λ x → fun-ext λ y → ×Path (sym (ap (λ e → η e _ _) π₁∘h))
                                  (sym (ap (λ e → η e _ _) π₂∘h))

  ⊤ PSh-Cartesian =
    record { F₀   = λ _ → Top
           ; F₁   = λ _ _ → tt
           ; F-id = refl
           ; F-∘  = λ _ _ → refl
           }
  isContr.center (terminal PSh-Cartesian) = NT (λ _ _ → tt) (λ _ _ _ → refl)
  isContr.paths (terminal PSh-Cartesian) y = NT≡ λ x → refl

  open FinitelyComplete

  {-# TERMINATING #-}
  PSh-Complete : {ℓ : _} → FinitelyComplete (PSh {ℓ})
  Pullback-of PSh-Complete {A} {B} {C} f g =
    record { F₀   = λ x → Σ (_ × _) λ { (a , b) → η f x a ≡ η g x b }
           ; F₁   = λ { {x} {y} map ((a , b) , l) →
                        let
                          p =
                            η f y (F₁ A map a) ≡⟨ happly (is-natural f x y map) a ⟩
                            F₁ C map (η f x a) ≡⟨ ap (F₁ C map) l ⟩
                            F₁ C map (η g x b) ≡⟨ sym (happly (is-natural g x y map) b) ⟩
                            _                  ∎
                        in (F₁ A map a , F₁ B map b) , p
                      }
           ; F-id = fun-ext λ where
              ((a , b) , p) →
                Σ-Path! (×Path (happly (F-id A) a) (happly (F-id B) b)) (UIP _ _)
           ; F-∘  = λ f₁ g₁ → fun-ext λ where
              ((a , b) , p) →
                Σ-Path! (×Path (happly (F-∘ A f₁ g₁) a) (happly (F-∘ B f₁ g₁) b))
                        (UIP _ _)
           }
  pb-π₁ PSh-Complete = NT (λ { x p → _×_.fst (Σ.fst p) }) λ x y f → refl
  pb-π₂ PSh-Complete = NT (λ { x p → _×_.snd (Σ.fst p) }) λ x y f → refl
  is-pullback PSh-Complete f g = record
    { commutes = NT≡ λ x → fun-ext λ where
        ((a , b) , p) → p
    ; limiting =
      λ {Q} {p₁} {p₂} q → NT (λ x seed → ( (η p₁ x seed , η p₂ x seed)
                                         , ap (λ e → η e x seed) q) )
        λ x y hom → fun-ext λ where
        p → Σ-Path! (×Path (happly (is-natural p₁ x y hom) p)
                           (happly (is-natural p₂ x y hom) p))
                    (UIP _ _)
    ; unique = λ {Q} {p₁} {p₂} eq fst≡ snd≡ → NT≡ λ coord → fun-ext λ where
        p → Σ-Path! (×Path (ap (λ e → η e coord p) fst≡)
                           (ap (λ e → η e coord p) snd≡))
                    (UIP _ _)
    ; p₁∘limiting≡p₁' = λ eq → NT≡ λ x → fun-ext λ where
        p → refl
    ; p₂∘limiting≡p₂' = λ eq → NT≡ λ x → fun-ext λ where
        p → refl
    }
  Equaliser-of PSh-Complete {A} {B} f g = 
    record { F₀   = λ c → Σ (F₀ A c) λ x → η f c x ≡ η g c x
           ; F₁   = λ { {x} {y} map (a , l) →
                        let
                          p = η f y (F₁ A map a)  ≡⟨ happly (is-natural f x y map) a ⟩
                              F₁ B map (η f x a)  ≡⟨ ap (F₁ B map) l ⟩
                              F₁ B map (η g x a)  ≡⟨ sym (happly (is-natural g x y map) a) ⟩
                              _                   ∎
                        in F₁ A map a , p
                     }
           ; F-id = fun-ext λ where
              (a , p) → Σ-Path! (happly (F-id A) a) (UIP _ _)
           ; F-∘  = λ f₁ g₁ → fun-ext λ where
              (a , p) → Σ-Path! (happly (F-∘ A f₁ g₁) a) (UIP _ _)
           }
  equalise PSh-Complete f g = NT (λ x x₁ → Σ.fst x₁) λ x y f₁ → refl
  is-equaliser PSh-Complete =
    record
    { commutes = NT≡ λ x → fun-ext λ { (_ , p) → p } 
    ; limiting =
      λ {E'} {e'} eq → NT (λ seed o → (η e' seed o , ap (λ e → η e seed o) eq))
        λ x y hom → fun-ext λ where
          p → Σ-Path! (happly (is-natural e' x y hom) p) (UIP _ _)
    ; unique = λ {E'} {e} eq e'≡ → NT≡ λ coord → fun-ext λ where
        p → Σ-Path! (sym (ap (λ e → η e coord p) e'≡)) (UIP _ _)
    ; e∘limiting≡e' = λ _ → NT≡ λ _ → fun-ext λ _ → refl
    }
