open import Category

open import Topos.Base

import Category.Associativity
import Category.Instances.PSh

import Topos.Instances.PSh.Exponentials
import Topos.Instances.PSh.Colimits
import Topos.Instances.PSh.Limits
import Topos.Instances.PSh.Omega

open Functor
open _=>_

module Topos.Instances.PSh {o₁ : _} (C : Category o₁ o₁) where
  open Topos.Instances.PSh.Exponentials C public
  open Topos.Instances.PSh.Colimits C public
  open Topos.Instances.PSh.Limits C public
  open Topos.Instances.PSh.Omega C public
  open Category.Instances.PSh C
  
  open ElementaryTopos

  PSh-Topos : ElementaryTopos (PSh {o₁})
  cartesian PSh-Topos = PSh-Cartesian {o₁}
  cocartesian PSh-Topos = PSh-Cocartesian {o₁}
  finitelyComplete PSh-Topos = PSh-Complete {o₁}
  closed PSh-Topos = PSh-Closed
  subobjectClassifier PSh-Topos = PSh-Omega
