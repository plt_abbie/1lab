open import 1Lab.Data.Finite.Closure
open import 1Lab.Data.Decidable
open import 1Lab.Isomorphism
open import 1Lab.HProp
open import 1Lab.Coeq
open import 1Lab.Base

open import Category.Structure.FinitelyComplete
open import Category.Structure.CartesianClosed
open import Category.Structure.Cartesian
open import Category.Diagrams using (is-Iso ; iso)
open import Category

open import Topos.Base

import Category.Diagrams

module Topos.Instances.FinSet where

module _ {ℓ : _} where
  open Category.Category

  -- The category of finite sets has as objects the B-finite sets ­--
  -- those equivalent to [n] for some n - and maps regular functions.
  -- This will simplify most of the constructions since computation
  -- still works
  FinSet : Category _ _
  Ob FinSet  = Σ (Set ℓ) Finite
  Hom FinSet (fst , _) (fst₁ , _) = fst → fst₁
  id FinSet x = x 
  _∘_ FinSet f g x = f (g x)
  idr FinSet f = refl
  idl FinSet f = refl
  assoc₁ FinSet f g h = refl
  assoc₂ FinSet f g h = refl

  open Cartesian

  -- This gives us a Cartesian monoidal structure on FinSet.
  FinSet-products : Cartesian FinSet
  _×C_ FinSet-products (A , Af) (B , Bf) = A × B , Finite-× {{Af}} {{Bf}}

  π₁ FinSet-products (fst , snd) = fst
  π₂ FinSet-products (fst , snd) = snd

  _h×_ FinSet-products f g x = f x , g x

  π₁∘h×≡f FinSet-products f g = refl
  π₂∘h×≡g FinSet-products f g = refl

  h×-unique FinSet-products l r = fun-ext λ x → ×Path (sym (happly l x)) (sym (happly r x))

  -- The type 1 is trivially finite, and is the terminal object of
  -- FinSet.
  ⊤ FinSet-products = Top , Finite-⊤
  terminal FinSet-products = contract (λ x → tt) λ y → refl

  -- We also have a *co*cartesian structure - a cartesian structure on
  -- FinSet^op - where the products are coproducts and the terminal
  -- object is an initial object.

  FinSet-coproducts : Cartesian (FinSet ^op)
  _×C_ FinSet-coproducts (A , Af) (B , Bf) = (A ∐ B) , Finite-∐ {{Af}} {{Bf}}
  π₁ FinSet-coproducts = left
  π₂ FinSet-coproducts = right
  _h×_ FinSet-coproducts l r (left x) = l x
  _h×_ FinSet-coproducts l r (right x) = r x
  π₁∘h×≡f FinSet-coproducts f g = refl
  π₂∘h×≡g FinSet-coproducts f g = refl
  h×-unique FinSet-coproducts hl hr = fun-ext λ where
    (left e)  → sym (happly hl e)
    (right e) → sym (happly hr e)
                                                      
  ⊤ FinSet-coproducts = Bot , Finite-⊥
  terminal FinSet-coproducts = contract (λ ()) λ y → fun-ext (λ ())

  absurd' : {ℓ' : _} {T : Set ℓ'} → Fin 0 → T  
  absurd' ()

  private
    -- The decision procedure (in-image f y) witnesses that being in the
    -- image of a function between finite sets is discrete We traverse
    -- the entire set X looking for an element that is equal to y.
    in-image : {X Y : Set ℓ}
            → Finite X → Finite Y
            → (f : X → Y)
            → (y : Y)
            → Dec (Σ X λ x → f x ≡ y)
    in-image {X} {Y} (X-size , X-iso) (Y-size , Y-iso) f y = fixup (go f') where
      Y-disc : Discrete Y
      Y-disc = Discrete-≃ (Y-iso ≃¯¹) Fin-discrete

      f' : Fin X-size → Y
      f' x = f (_≃_.from X-iso x)

      go : {n : Nat} (f : Fin n → Y) → Dec (Σ (Fin n) λ x → f x ≡ y) 
      -- Nothing is in the image of a function Fin 0 → Y
      go {n = zero} f = no λ { (() , snd) }

      -- There's a possibility that it's the 0th element in X. Decide equality
      go {n = suc n} f with Y-disc (f fzero) y
      ... | yes hier = yes (fzero , hier) -- yup we found it

      -- Otherwise we look for it in the larger set - shifting all our
      -- indices by one.
      ... | no ¬here with (go {n} λ x → f (fsuc x))

      -- If we found it, we know it's one to the right of where the
      -- inductive step tells us it is
      ... | yes (fst , snd) = yes (fsuc fst , snd)

      -- Otherwise can prove by cases that it's nowhere
      ... | no ¬there = no λ { (fzero , snd)    → ¬here snd
                             ; (fsuc fst , snd) → ¬there (fst , snd)
                             }

      -- then we just need to fixup the construction so it works for a
      -- B-finite set X and not a canonical finite set
      fixup : Dec (Σ (Fin X-size) λ x → f' x ≡ y) → Dec (Σ X λ x → f x ≡ y)
      fixup (yes (fst , snd)) = yes (_≃_.from X-iso fst , snd)
      fixup (no ¬p) = no λ { (fst , snd) → ¬p (_≃_.to X-iso fst , ap f (_≃_.fromTo X-iso fst) ∘p snd) }

    -- We can also get a boolean representing the result of looking for
    -- y in the image of f.
    in-image' : {ℓ' : _} {X Y : Set ℓ} → Finite X → Finite Y → (f : X → Y) → (y : Y) → Bool {ℓ'}
    in-image' Xf Yf f y with in-image Xf Yf f y
    ... | yes p = tt
    ... | no p = ff

    -- And prove that if this representative boolean is tt, then the
    -- element is indeed in the image!
    -- This is just by deciding the proposition again. The no case is
    -- impossible, since in-image' would compute to ff, and ff ≢ tt.
    in-image'≡tt : {X Y : Set ℓ}
                → (Xf : Finite X) (Yf : Finite Y)
                → (f : X → Y)
                → (y : Y)
                → in-image' Xf Yf f y ≡! tt {ℓ}
                → Σ X λ x → f x ≡ y
    in-image'≡tt Xf Yf f y p with in-image Xf Yf f y
    in-image'≡tt Xf Yf f y p | yes ev = ev

    -- Given an x, (f x) is trivially in the image of x.
    in-image-duh : {X Y : Set ℓ} → (Xf : Finite X) (Yf : Finite Y) (f : X → Y) (x : X)
                 → tt {ℓ} ≡! in-image' Xf Yf f (f x)
    in-image-duh Xf Yf f x with in-image Xf Yf f (f x)
    ... | yes p = refl
    ... | no ¬p = absurd (¬p (x , refl))

  open SubobjectClassifier
  open Category.Diagrams FinSet

  FinSet-Prop : SubobjectClassifier FinSet-products
  -- Even constructively, the subobject classifier of FinSet is {0, 1}.
  Ω FinSet-Prop = Bool , Finite-Bool

  -- The true map is the map constantly true. (lol)
  true FinSet-Prop = λ { x → tt }

  -- The characteristic function χ of a subobject decides if a given x is
  -- in the image of the monomorphism U ↪ X
  χ FinSet-Prop {U , Uf} {X , Xf} mono x = in-image' Uf Xf (_↪_.map mono) x

  is-pullback FinSet-Prop {U , Uf} {X , Xf} mono =
    record
    { commutes        = fun-ext λ x → in-image-duh Uf Xf map x
      -- ^ to show that the diagram commutes, we need to provide a
      -- witness that
      --    true ≡ χ mono ∘ mono
      -- that is, every object in the image of mono is mapped to tt by
      -- its classifying map. this is trivial
    ; limiting        = λ {Q} eq → limit {Q} eq
    ; p₂∘limiting≡p₂' = λ {Q} eq → fun-ext λ x → limit-comm {Q} eq x
    -- ^^ we construct the limiting map Q → U in an aux module

    ; p₁∘limiting≡p₁' = λ eq → fun-ext λ x → refl
    -- ^ this case is an equality between two functions into *

    ; unique          = λ {Q} {p₁'} {p₂'} eq _ mapi≡p₂' → monic {Q} (mapi≡p₂' ∘p sym (fun-ext λ x → limit-comm {Q} eq x))
    }
    where
      map = _↪_.map mono
      monic = _↪_.monic mono

      chi : X → Bool {ℓ}
      chi x = in-image' Uf Xf (_↪_.map mono) x

      -- if there is another Q, and a map p₂' : Q → X such that every
      -- p₂' is also in the image of mono (eq) 
      -- we can make a unique Q → U that makes everything commute.
      module _ {Q : Σ (Set ℓ) Finite}
               {p₁' : Σ.fst Q → Top {ℓ}}
               {p₂' : Σ.fst Q → X}
               (eq : (λ x → tt) ≡! (λ x → chi (p₂' x)))
               (q : Σ.fst Q)
             where
        -- we get the element of U - and the proof that things commute -
        -- by turning an equality between tt and is-image into a witness
        -- that (p₂' q) is in the image of map.
        limit' : Σ U (λ x → map x ≡ p₂' q)
        limit' = in-image'≡tt Uf Xf map (p₂' q) (sym (happly eq q))

        limit : U
        limit = Σ.fst limit'

        limit-comm : map limit ≡ p₂' q
        limit-comm = Σ.snd limit'

  open FinitelyComplete

  -- The category of finite sets has all finite limits. This is because
  -- finite limits in Set are computed like
  --
  --    A ×ₐ B = { (x, y) ∈ A × B | f x ≡ g y } (similarly for equalisers).
  --
  -- In the case where A and B are finite, and thus discrete, this is a
  -- decidable subset of a finite set - and thus finite.
  FinSet-Limits : FinitelyComplete FinSet
  Pullback-of FinSet-Limits {A , Af} {B , Bf} {C , Cf} f g =
      (Σ (A × B) λ { (x , y) → f x ≡ g y })
    , Finite-subset-finite
        (Finite-× {{Af}} {{Bf}}) _
        (λ x y → UIP _ _)
        (λ { (x , y) → Finite→Discrete Cf (f x) (g y) })

  -- All of the diagrams involving pullbacks are the same as in Set.
  pb-π₁ FinSet-Limits ((a , b) , pb) = a
  pb-π₂ FinSet-Limits ((a , b) , pb) = b
  is-pullback FinSet-Limits f g = record
    { commutes        = fun-ext λ { (_ , p) → p }
    ; limiting        = λ {Q} {p₁} {p₂} prf q → ((p₁ q , p₂ q) , ap (λ e → e q) prf)
    ; p₁∘limiting≡p₁' = λ eq → refl
    ; p₂∘limiting≡p₂' = λ eq → refl
    ; unique =
        λ {Q} {p₁'} {p₂'} {i} eq fst≡ snd≡ → fun-ext λ q →
          Σ-Path! (×Path (happly fst≡ q) (happly snd≡ q)) (UIP _ _) 
    }

  -- The equaliser of f, g : A → B is { x ∈ A | f x ≡ g x }. Again this
  -- is a decidable subset of a finite set, since B is discrete.
  Equaliser-of FinSet-Limits {A , Af} {_ , Bf} f g =
      (Σ _ λ x → f x ≡ g x)
    , Finite-subset-finite
        Af _
        (λ x y → UIP _ _)
        λ x → Finite→Discrete Bf (f x) (g x)

  -- The maps are also as in Set.
  equalise FinSet-Limits = λ f g x → Σ.fst x
  is-equaliser FinSet-Limits =
    record
    { commutes      = fun-ext Σ.snd
    ; limiting      = λ {E'} {e'} eq o → (e' o , ap (λ e → e o) eq)
    ; e∘limiting≡e' = λ eq → refl 
    ; unique        = λ eq x → fun-ext λ x₁ → Σ-Path! (sym (happly x x₁)) (UIP _ _)
    }

  -- By extending the isomorphism (Fin m → Fin n) ≃ Fin nᵐ along
  -- isomorphisms A ≃ Fin m and B ≃ Fin m, we get that the type A → B
  -- is finite.

  open CartesianClosed

  -- This immediately gives us that the category FinSet is closed wrt.
  -- its Cartesian monoidal structure
  fs-exp : CartesianClosed FinSet-products
  [_,_] fs-exp (A , Af) (B , Bf) = (A → B) , Finite-→ {{Af}} {{Bf}}
  eval fs-exp (fst , snd) = fst snd
  λf fs-exp f a b = f (a , b)
  β fs-exp = λ g → refl
  λ-unique fs-exp h commutes = fun-ext λ a → fun-ext λ b → happly commutes (a , b)

  -- An elementary topos is a..
  open ElementaryTopos
  Fs-Topos : ElementaryTopos FinSet
  -- Finitely complete cartegory ...
  cartesian           Fs-Topos = FinSet-products
  cocartesian         Fs-Topos = FinSet-coproducts
  finitelyComplete    Fs-Topos = FinSet-Limits

  -- ... closed with respect to its Cartesian monoidal structure ...
  closed              Fs-Topos = fs-exp

  -- ... with an object that classifies monomorphisms.
  subobjectClassifier Fs-Topos = FinSet-Prop
