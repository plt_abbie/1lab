open import 1Lab.Function.Properties
open import 1Lab.HProp
open import 1Lab.Coeq
open import 1Lab.Base

open import Agda.Primitive

open import Category.Instances.Sets.Cartesian
open import Category.Instances.Sets.Colimits
open import Category.Instances.Sets.Closed
open import Category.Instances.Sets.Limits
open import Category.Structure.Cartesian
open import Category

open import Topos.Base

import Category.Diagrams

module Topos.Instances.Sets where

-- (Assuming fun-ext, prop-ext, and propositional resizing, which I have
-- postulated), the category of o₁-small Sets is an elementary topos.
-- Most of the constructions are trivial:

-- ... coproducts are coproducts ...

-- The only complication is the subobject classifier.
Set-soCl : {l : _} → SubobjectClassifier (Set-products {l})
-- The object - type - that represents propositions is the type of
-- propositions. Propositional resizing ensures that it can be squashed
-- down to fit in the same level as the objects in Sets ℓ
SubobjectClassifier.Ω (Set-soCl {l}) = hProp l

-- The true arrow maps * → the true proposition, P⊤, which is carried
-- by the type Top
SubobjectClassifier.true Set-soCl x = P⊤

-- The classifying morphism for a mono maps an element x of the bigger
-- set, X, to the proposition (∃ y : X) map y ≡ x - that is, "x is in
-- the image of map"
SubobjectClassifier.χ Set-soCl record { map = map ; monic = monic } x =
  ∥ (Σ _ λ y → map y ≡ x) ∥ 

-- It remains to show that the square
-- 
--         λ _ → *
--    U ───────────> ⊤
--    ╭              │
--  m │              │ λ _ → P⊤
--    V              V
--    X ───────────> Ω
--          χ m
--
-- a) commutes
-- b) is a limit.
SubobjectClassifier.is-pullback (Set-soCl {l}) {X} {U} record { map = map ; monic = monic } =
  record
  { commutes = fun-ext λ x → prop-ext (λ _ → squash (x , refl)) λ _ → tt
    -- ^ To prove commutativity, we have to prove that χ m (m x) ≡ P⊤.
    -- We do this with prop-ext: ∃ (y : X) map y ≡ map x is inhabited by
    -- (x, refl), and prop-ext lets us prove that inhabited propositions
    -- are ≡-equal to P⊤.
  ; p₁∘limiting≡p₁' = λ eq → refl
    -- ^ This is trivial because p₁' and p₁ ∘ limiting are arrows into
    -- Top
  ; limiting = limit
  ; p₂∘limiting≡p₂' = λ {Q} {p₁'} {p₂'} eq → fun-ext λ q → limit-comm eq q

  ; unique = λ {Q} {p₁'} {p₂'} {i} eq _ mapi≡p₂' → monic (mapi≡p₂' ∘p sym (fun-ext λ q → limit-comm eq q))
    -- ^ Uniqueness of the limiting arrow follows from m being a
    -- monomorphism
  }
  where
    module S = Category.Category (Sets (lsuc l))

    -- χ, specialised at map
    χ : U → hProp l
    χ x = ∥ (Σ X λ y → map y ≡ x) ∥ 

    -- Construction of the limiting arrow.
    module _ {Q : Set l} {p₁' : Q → Top {l}} {p₂' : Q → U} (eq : (λ x → P⊤) ≡! (λ x → χ (p₂' x))) (q : Q) where
      -- First we prove that an inhabitant of ∃ (y : X) map y ≡ _
      -- actually exists - we get this from the given equality! since χ
      -- (p₂' x) ≡ P⊤, we can transport tt to get an inhabitant of the
      -- existential
      exists : hProp.carrier ∥ (Σ X λ y → map y ≡ _) ∥
      exists = transport (ap (λ e → hProp.carrier (e q)) eq) tt

      -- Then, we prove that (χ ...) is a proposition - meaning we can
      -- unsquash it (alternatively, meaning that the squash is
      -- redundant)
      rng : hProp l
      hProp.carrier rng = Σ X λ y → map y ≡ _
      hProp.prop rng (x , r) (x₁ , s) = (Σ-Path! x≡x₁ (UIP _ _))
        where
          mapx≡mapx₁ : map x ≡ map x₁
          mapx≡mapx₁ = r ∘p sym s

          x≡x₁ = ap (λ e → e tt) (monic {Z = Top} {g₁ = λ _ → x} {g₂ = λ x → x₁} (fun-ext λ x₂ → mapx≡mapx₁))

      -- We then unsquash to get
      --
      --   a) an inhabitant of X (the limiting arrow - it's a function
      --      because this is in a parametrised module)
      --
      --   b) a proof that p₂' q ≡ map limit, making the diagram
      --      commute.
      limit : X
      limit = Σ.fst (exists rng λ x → x)
          
      limit-comm : map limit ≡ p₂' q
      limit-comm = Σ.snd (exists rng λ x → x)


-- Putting it all together, we have a proof that Sets is an
-- elementary topos! Yay!
Set-Topos : {o₁ : _} → ElementaryTopos (Sets o₁)
ElementaryTopos.cartesian           Set-Topos = Set-products
ElementaryTopos.cocartesian         Set-Topos = Set-coproducts
ElementaryTopos.closed              Set-Topos = Set-exponentials
ElementaryTopos.subobjectClassifier Set-Topos = Set-soCl
ElementaryTopos.finitelyComplete    Set-Topos = Set-finitelyComplete

module Sets {ℓ : _} = ElementaryTopos (Set-Topos {ℓ})

module _ {o₁ : _} where
  open Category.Diagrams (Sets o₁)

  monic→injective : {A B : Set o₁} {f : A → B}
                  → is-Mono f
                  → injection f
  monic→injective monic {x} {y} eq =
    happly (monic {Z = Top} {λ _ → x} {λ _ → y} (fun-ext λ _ → eq)) tt

  injective→monic : {A B : Set o₁} {f : A → B}
                  → injection f
                  → is-Mono f
  injective→monic injective prf = fun-ext λ x → injective (happly prf x)
