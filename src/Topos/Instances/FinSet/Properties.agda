open import 1Lab.Data.Decidable
open import 1Lab.Isomorphism
open import 1Lab.HProp
open import 1Lab.Coeq
open import 1Lab.Base

open import Category.Diagrams using (is-Iso ; iso)
open import Category

open import Topos.Instances.FinSet
open import Topos.Instances.Sets
open import Topos.Base

import Category.Diagrams

import Topos.Logic

open Functor

module Topos.Instances.FinSet.Properties {o₁ : _} where

-- A lemma: The inclusion functor ι : FinSet → Set merely forgets that
-- a set is finite. It's almost logical: in fact, classically, it is.
-- All that's missing is excluded middle.
ι : Functor (FinSet {o₁}) (Sets o₁)
F₀ ι (fst , snd) = fst
F₁ ι x = x
F-id ι = refl
F-∘ ι f g = refl

open Category.Diagrams (Sets o₁) using (_≈_ ; id≈)

≈→≃ : {A B : _} → A ≈ B → A ≃! B
≈→≃ (fst , iso g linv rinv) = iso fst g (λ x → happly rinv x) (λ x → happly linv x)

EM→ι-logical : ({P : hProp o₁} → Dec (hProp.carrier P)) → Logical Fs-Topos Set-Topos ι
EM→ι-logical em = r where
  open Logical

  r : Logical _ _ ι
  -- This is preserved, up to a non-trivial isomorphism, iff. every proposition is
  -- decidable:
  F-subobjectClassifier r = bool→prop , iso prop→bool (fun-ext prop→bool→prop) (fun-ext bool→prop→bool) where
    -- Here's our morp: it maps true to truth and false to falsehood.
    bool→prop : Bool {o₁} → hProp o₁
    bool→prop tt = P⊤
    bool→prop ff = P⊥
    
    -- For the other way around, we need excluded middle to decide
    -- whether to send P to true or false.
    prop→bool : hProp o₁ → Bool {o₁}
    prop→bool P with em {P}
    ... | yes p = tt
    ... | no ¬p = ff

    -- We prove that it's the same - apply excluded middle again, and
    -- then propositional extensionality gives us the equality we
    -- need.
    -- The with rule makes the em in the type of prop→bool→prop reduce
    -- as well.
    prop→bool→prop : (x : hProp o₁) → bool→prop (prop→bool x) ≡ x
    prop→bool→prop P with em {P}
    ... | yes p = prop-ext (λ x → p) λ x → tt
    ... | no ¬p = prop-ext (λ ()) λ x → ¬p x

    -- In the bool case, we have *slightly* more work to do.
    bool→prop→bool : (x : Bool) → prop→bool (bool→prop x) ≡ x
    -- By pattern matching, this reduces to prop→bool (em P⊤) ≡ tt
    -- So we have to make excluded middle do its thing again
    bool→prop→bool tt with em {P⊤}
    ... | yes p = refl
    ... | no ¬p = absurd (¬p tt)
    -- In the no case, excluded middle *disagreed with itself*! We have
    -- to come up with a proof that tt ≡ ff, which is absurd. Which is
    -- [absurd], since ¬p is a witness that ⊤ → ⊥!

    bool→prop→bool ff with em {P⊥}
    ... | yes p = absurd p
    ... | no ¬p = refl
    -- The same thing happens here. In the yes case, EM disagreed with
    -- itself - sneaky bastard. Call it out!

  F-true r = refl

  -- All of these are preserved strictly:
  F-terminal r = id≈
  F-products r = id≈
  F-pullbacks r = id≈
  F-equalisers r = id≈
  F-exponentials r = id≈
  F-π₁ r = refl
  F-π₂ r = refl
  F-h× r f g = refl

-- It's not just missing, though: The statement "the inclusion ι :
-- FinSet ↪ Set is logical" also implies the principle of excluded
-- middle:

ι-logical→EM : {P : hProp o₁} → Logical Fs-Topos Set-Topos ι → Dec (hProp.carrier P)
ι-logical→EM {P} log = dec where
  module Log = Logical log

  -- We use the fact that logical morps preserve the object Ω to get
  -- an isomorphism between Bool and hProp. This isn't enough to
  -- derive excluded middle, though.
  bool→prop : Bool ≃ hProp o₁
  bool→prop = ≈→≃ Log.F-subobjectClassifier

  open _≃_

  -- Thankfully logical morps also preserve the true arrow, which is
  -- to say, the image of tt under the inclusion is P⊤.
  true→top : to bool→prop tt ≡ P⊤
  true→top = happly Log.F-true tt

  -- Applying some isomorphism reasoning we conclude that the image of
  -- P⊤ under the isomorphism between Ω and ι(2) is tt.
  top→true : from bool→prop P⊤ ≡ tt
  top→true = subst (λ e → from bool→prop e ≡ tt) true→top (fromTo bool→prop _)

  -- With this we can decide P.
  dec : Dec (hProp.carrier P)
  dec = go (from bool→prop P) (toFrom bool→prop P) where
    -- Here's how: from bool→prop gives us a boolean; tt means yes, ff
    -- means no. We also carry around a proof that x is the image of P
    -- under the inverse.
    go : (x : Bool) → to bool→prop x ≡ P → Dec (hProp.carrier P)
    -- In the true case we get a proof
    --    to bool→prop tt ≡ P
    -- which we turn into a proof
    --    P⊤ ≡ P
    -- which we can use to transport * → P, so we are done.
    go tt p = yes (transport (ap hProp.carrier (sym true→top ∘p p)) tt)

    -- In the false case, we say no!
    -- To say no, you need to disprove any witnesses of P. Let's see how.
    go ff refl = no contra where
      -- First, P ≡ to bool→prop ff, by pattern matching on refl,
      -- which we are allowed to do.

      -- And a tiny lemma that says false isn't true.
      tt≢ff : ff {o₁} ≡! tt → Bot {o₁}
      tt≢ff ()

      -- Then a proof that if there were an inhabitant of ff, ff would
      -- be equal to tt. There's the disproof we need.
      contra : hProp.carrier (to bool→prop ff) → Bot
      contra point = tt≢ff q where
        eq = ap hProp.carrier true→top

        -- Since [to bool→prop ff] and [to bool→prop tt] are both
        -- inhabited propositions, under propositional extensionality,
        -- they are the same.
        p : to bool→prop ff ≡ to bool→prop tt
        p = prop-ext (λ x → transport (sym eq) tt) λ x → point

        -- Since to is half of an isomorphism - we can invert it, to conclude ff ≡ tt!
        q : ff ≡ tt
        q = subst (λ e → e ff ≡ e tt) (fun-ext (fromTo bool→prop)) (ap (from bool→prop) p)
