open import 1Lab.Base

open import Category.Structure.FinitelyComplete
open import Category.Structure.Cartesian
open import Category.Slices
open import Category

open import Topos.Base

import Category.Diagrams

module Topos.Slice where

Topos→LocallyCartesian : {o₁ h₁ : _} {C : Category o₁ h₁}
                       → ElementaryTopos C
                       → (o : Category.Ob C)
                       → Cartesian (Slice C o)
Topos→LocallyCartesian {C = C} E base = r where
  open FinitelyComplete
  module C = Category.Category C
  module E = ElementaryTopos E
  open Category.Diagrams C

  r : Cartesian (Slice C base)
  Cartesian._×C_ r (A , A→base) (B , B→base) = (E.Pullback-of A→base B→base , A→base C.∘ E.pb-π₁)
  Cartesian.π₁ r = (E.pb-π₁ , refl)
  Cartesian.π₂ r {A , A→base} {B , B→base} =
    E.pb-π₂ ,
    sym (Pullback.commutes (FinitelyComplete.is-pullback E.finitelyComplete A→base B→base))
  Cartesian._h×_ r {A , A→base} {B , B→base} {C , C→base} (A→B , fib) (A→C , fib₁) =
    A→Pb , A→Pb-fibs
    where
      module Pb = Pullback (FinitelyComplete.is-pullback E.finitelyComplete B→base C→base)
      eq = fib ∘p sym fib₁
      A→Pb = Pb.limiting eq
      A→Pb-fibs =
          (_ C.∘ _) C.∘ A→Pb ≡⟨ C.assoc₁ _ _ _ ⟩
          _ C.∘ _ C.∘ A→Pb   ≡⟨ ap (C._∘_ B→base) (Pb.p₁∘limiting≡p₁' eq) ⟩
          _                  ≡⟨ fib ⟩
          A→base             ∎

  Cartesian.π₁∘h×≡f r {A , A→base} {B , B→base} {C , C→base} (A→B , fib) (A→C , fib₁) =
    Σ-Path! (Pb.p₁∘limiting≡p₁' (fib ∘p sym fib₁)) (UIP _ _)
    where module Pb = Pullback (FinitelyComplete.is-pullback E.finitelyComplete B→base C→base)

  Cartesian.π₂∘h×≡g r {A , A→base} {B , B→base} {C , C→base} (A→B , fib) (A→C , fib₁) =
    Σ-Path! (Pb.p₂∘limiting≡p₂' (fib ∘p sym fib₁)) (UIP _ _)
    where module Pb = Pullback (FinitelyComplete.is-pullback E.finitelyComplete B→base C→base)

  Cartesian.h×-unique r {A , A→base} {B , B→base} {C , C→base} {f , f-fib} {g , g-fib} {a→pb' , a→pb'-fib} fst snd =
    Σ-Path! (Pb.unique (f-fib ∘p sym g-fib) (sym (ap Σ.fst fst)) (sym (ap Σ.fst snd))) (UIP _ _)
    where
      module Pb = Pullback (FinitelyComplete.is-pullback E.finitelyComplete B→base C→base)

  Cartesian.⊤ r = base , C.id
  Cartesian.terminal r { A , A→base } =
    contract (A→base , C.idl _) λ { (A→base₁ , snd) → Σ-Path! (sym snd ∘p C.idl _) (UIP _ _) } 

Topos→LocallySubobjectClassifier : {o₁ h₁ : _} {C : Category o₁ h₁}
                                 → (p : ElementaryTopos C)
                                 → (o : Category.Ob C)
                                 → SubobjectClassifier {C = Slice C o} (Topos→LocallyCartesian p o)
Topos→LocallySubobjectClassifier {C = C} E base = r where
  Ca = Topos→LocallyCartesian E base
  module Ca = Cartesian Ca

  module C = Category.Category C
  module E = ElementaryTopos E

  module C/c = Category.Category (Slice C base)

  module ArrC/c = Category.Diagrams (Slice C base)
  
  open Category.Diagrams C
  open SubobjectClassifier
  open Ca

  /-mono→mono : {U X : C/c.Ob} → U ArrC/c.↪ X → Σ.fst U ↪ Σ.fst X
  /-mono→mono {U , U→base} {X , X→base} record {map = map , fibers; monic = monic} =
    record { map = map
           ; monic = λ {Z} {f} {g} x →
                       let 
                         p₁ = _ ≡⟨ ap (λ e → e E.∘ f) (sym fibers) ⟩
                              _ ≡⟨ E.assoc₁ _ _ _ ⟩
                              _ ∎

                         p₂ = _ ≡⟨ ap (λ e → e E.∘ g) (sym fibers) ⟩
                              _ ≡⟨ E.assoc₁ _ _ _ ⟩
                              _ ≡⟨ ap (λ e → X→base E.∘ e) (sym x) ⟩
                              _ ∎

                         e = monic {Z = Z , X→base C.∘ map C.∘ f }
                                   { f , p₁ }
                                   { g , p₂ }
                                   (Σ-Path! x (UIP _ _))
                        in ap Σ.fst e
           }

  r : SubobjectClassifier Ca
  Ω r    = E.Ω E.×C base , E.π₂
  true r = (E.true E.∘ E.!) E.h× C.id , E.π₂∘h×≡g _ _
  χ r { U , U→base } { X , X→base } mono =
    E.χ (/-mono→mono mono) E.h× X→base , E.π₂∘h×≡g _ _

  is-pullback r {U , U→base} {X , X→base} mono =
    record
    { commutes        =
        let
          p = Σ.snd (ArrC/c._↪_.map mono)
          map = Σ.fst (ArrC/c._↪_.map mono)

          commutes = Pb.commutes
          
          eq₁ = (E.true E.∘ isContr.center E.terminal) E.∘ X→base E.∘ map ≡⟨ E.assoc₁ _ _ _ ⟩
                E.true E.∘ isContr.center E.terminal E.∘ X→base E.∘ map   ≡⟨ ap (E._∘_ E.true) (sym (isContr.paths E.terminal _)) ⟩
                _                                                         ≡⟨ commutes ⟩
                (E.χ (/-mono→mono mono) E.∘ map) ∎

          eq =
            ((E.true E.∘ isContr.center E.terminal) E.h× E.id) E.∘ U→base          ≡⟨ ap (λ e → _ E.∘ e) (sym p) ⟩
            ((E.true E.∘ isContr.center E.terminal) E.h× E.id) E.∘ X→base E.∘ map  ≡⟨ h×-∘-dist E.cartesian ⟩
            (    ((E.true E.∘ isContr.center E.terminal) E.∘ X→base E.∘ map))
            E.h× (E.id E.∘ X→base E.∘ map)                                         ≡⟨ ap₂ E._h×_ eq₁ (E.idl _) ⟩
            _                                                                      ≡⟨ sym (h×-∘-dist E.cartesian) ⟩
            _                                                                      ∎ 
        in Σ-Path! eq (UIP _ _)
    ; limiting        = limiting
    ; unique          =
      λ comm p₁i p₂i → Σ-Path! (limiting-unique comm p₁i p₂i) (UIP _ _)
    ; p₁∘limiting≡p₁' = limiting-p1
    ; p₂∘limiting≡p₂' = limiting-p2
    }
    where
      over = /-mono→mono mono
      module Pb = Pullback (is-pullback E.subobjectClassifier over)

      module _ {Q×Q→base : C/c.Ob} {p₁'×fib : C/c.Hom Q×Q→base ⊤}
               {p₂'×fib : C/c.Hom Q×Q→base (X , X→base) }
               (comm : (true r C/c.∘ p₁'×fib) ≡! (χ r mono C/c.∘ p₂'×fib)) where

        private
          Q = Σ.fst Q×Q→base
          Q→base = Σ.snd Q×Q→base
          p₁' = Σ.fst p₁'×fib
          fib = Σ.snd p₁'×fib
          p₂' = Σ.fst p₂'×fib
          fib₁ = Σ.snd p₂'×fib

          fibers = Σ.snd (ArrC/c._↪_.map mono)
          map = Σ.fst (ArrC/c._↪_.map mono)

          p = ap Σ.fst comm

          p' = (E.true E.∘ E.!) E.h× p₁'                       ≡⟨ ap (λ e → (E.true E.∘ e) E.h× p₁') (isContr.paths E.terminal _) ⟩
               (E.true E.∘ E.! E.∘ p₁') E.h× p₁'               ≡⟨ ap (λ e → e E.h× _) (E.assoc₂ _ _ _) ⟩
               ((E.true E.∘ E.!) E.∘ p₁') E.h× p₁'             ≡⟨ ap (λ e → ((E.true E.∘ E.!) E.∘ p₁') E.h× e) (sym (E.idl _)) ⟩
               ((E.true E.∘ E.!) E.∘ p₁') E.h× (E.id E.∘ p₁')  ≡⟨ sym (h×-∘-dist E.cartesian) ⟩
               ((E.true E.∘ E.!) E.h× E.id) E.∘ p₁'            ≡⟨ p ⟩
               _                                               ≡⟨ h×-∘-dist E.cartesian ⟩
               _                                               ∎
      
        inner-commutes = h×≡h×→f≡f E.cartesian p'

        lim = Pb.limiting {p₁' = E.!} {p₂' = p₂'} inner-commutes

        map∘lim≡p₂' = Pb.p₂∘limiting≡p₂' inner-commutes

        U→base∘lim≡Q→base =
          U→base E.∘ lim            ≡⟨ ap (λ e → e E.∘ lim) (sym fibers) ⟩
          (X→base E.∘ map) E.∘ lim  ≡⟨ E.assoc₁ _ _ _ ⟩
          X→base E.∘ (map E.∘ lim)  ≡⟨ ap (E._∘_ X→base) map∘lim≡p₂' ⟩
          X→base E.∘ p₂'            ≡⟨ fib₁ ⟩
          Q→base                    ∎

        limiting : C/c.Hom Q×Q→base (U , U→base)
        limiting = (lim , U→base∘lim≡Q→base)
        
        limiting-unique : {i : C/c.Hom Q×Q→base (U , U→base) }
                        → (! C/c.∘ i) ≡! p₁'×fib
                        → (ArrC/c._↪_.map mono C/c.∘ i) ≡! p₂'×fib
                        → Σ.fst i ≡! Σ.fst limiting
        limiting-unique {i} p1 p2 =
          Pb.unique {i = Σ.fst i} inner-commutes
                    (sym (isContr.paths E.terminal _))
                    (ap Σ.fst p2)
        
        limiting-p1 : (! C/c.∘ limiting) ≡! p₁'×fib
        limiting-p1 = 
          let
            x = isContr.paths Ca.terminal p₁'×fib
            q = U→base E.∘ lim ≡⟨ U→base∘lim≡Q→base ⟩
                Q→base         ≡⟨ ap Σ.fst x ⟩
                _              ∎
          in Σ-Path! q (UIP _ _)

        limiting-p2 : (ArrC/c._↪_.map mono C/c.∘ limiting) ≡! p₂'×fib
        limiting-p2 = Σ-Path! map∘lim≡p₂' (UIP _ _)
