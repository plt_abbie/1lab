```
open import 1Lab.Base

open import Agda.Primitive

module Category where
```

# Categories

```
record Category (o h : Level) : Set (lsuc (o ⊔ h)) where
```

A _category_ is a "proof-relevant preorder". In a preordered set $(A,
\le)$, the inhabitants of a set $A$ are related by a _proposition_ $a
\le b$, which is

- _reflexive_: $a \le a$
- _transitive_: $a \le b \land b \le c \to a \le c$

In a category, the condition that $a \le b$ be a proposition is relaxed:
A category has a `set of objects`{.Agda ident=Ob} and, between each $x,
y$, a set $\mathrm{Hom}(x, y)$ of relations (or maps). The name Hom is
historical and it betrays the original context in which categories where
employed: algebra(ic topology), where the maps in question are
**hom**omorphisms.

```
  field
    Ob  : Set o
    Hom : Ob → Ob → Set h
```

If you are already familiar with the definition of category there are
two things to note here:

First is that out formalization has a _family_ of `Hom`{.Agda}-sets
rather than a single `Hom`{.Agda}-set and source/target maps. This
formulation is not unique to category theory done internally to type
theory, but it is the most reasonable way to formulate things in this
context.

Second is that the word "set" in the definition of Hom-set has nothing
to do with "size". Indeed, the "set"/"not a set" (alternatively
"small"/"large") distinction makes no sense in type theory (some may
argue it makes no sense in general).

Instead, the `Category`{.Agda} record is parametrised by two levels:
`o`, and `h`. The type of objects has to fit in the universe `Set o`,
and the family of Hom-sets is `Set h` valued. As an example, the thin
category corresponding to the natural numbers with their usual
ordering would live in `Category lzero lzero`.

This design decision has a couple consequences: As an example, if you
have any category $C$ (with objects `o` and homs `h`), there is always
a [category of categories](agda://Category.Instances.Cat.Base#Cat)
which has $C$ as one of its objects; This category has objects in a
universe big enough to fit `Set o` and `Set h` - so `lsuc (o ⊔ h)` -
and homs as large as `o` and `h`, as can be read off the type of
[Cat](agda://Category.Instances.Cat.Base#Cat).

Similarly, there is no single "category of sets" - there is a _family_
of categories of sets, parametrised by the level in which its objects
live.

```
  field
    id  : {x : _}     → Hom x x
    _∘_ : {x y z : _} → Hom y z → Hom x y → Hom x z

  infixr 40 _∘_
```

The "proof-relevant" version of the reflexivity and transitivity laws
are, respectively, the `identity morphisms`{.Agda} and `composition of
morphisms`{.Agda ident="_∘_"}. Unlike in the proof-irrelevant case, in
which an inhabitant of $x \le y$ just says two things are related, these
operations _matter_, and thus must satisfy laws:
  
```
  field
    idr : {x y : _} (f : Hom x y) → f ∘ id ≡! f
    idl : {x y : _} (f : Hom x y) → id ∘ f ≡! f
```

The two identity laws say that the identity morphisms serve as neutral
elements for the composition operation, both on the left and on the
right. The "two" associativity laws (below) say that both ways of writing
parentheses around a composition of three morphisms is equal: $(f \circ
g) \circ h = f \circ (g \circ h)$.
    
```
    assoc₁ : {w x y z : _} (f : Hom y z) (g : Hom x y) (h : Hom w x)
           → (f ∘ g) ∘ h ≡! f ∘ (g ∘ h)
    assoc₂ : {w x y z : _} (f : Hom y z) (g : Hom x y) (h : Hom w x)
           → f ∘ (g ∘ h) ≡! (f ∘ g) ∘ h
```

There are two, going in both directions, to facilitate _dualizing_[^1]:
Rather than having a single associativity equality and inverting it when
constructing opposite categories, we can swap the associativity
witnesses `assoc₁` and `assoc₂`. This yields a definition of [opposite
category](#opposite-category) which is _definitionally_ involutive.

[^1]: This trick is due to [Gross et. al., _Experience Implementing a
Performant Category-Theory Library in Coq_](https://arxiv.org/pdf/1401.7694.pdf).

In Martin-Löf type theory, a structure having multiple paths like this
would _not_ be the same as the structure having only one path - we would
need to add a coherence condition saying that `sym assoc₁ ≡ assoc₂`. In
our Agda, this is not an issue, since we assume [uniqueness of equality
proofs](agda://1Lab.Base#UIP): any two proofs of the same equality
are equal.

## Opposites

A common theme throughout category theory is that of _duality_: The dual
of a categorical concept is same concept, with "all the arrows
inverted". To make this formal, we introduce the idea of _opposite
categories_: The opposite of $C$, written $C^{op}$, has the same
`objects`{.Agda}, but with $\mathrm{Hom}_{C^{op}}(x, y) =
\mathrm{Hom}_{C}(y, x)$.

```
infixl 60 _^op
_^op : {o₁ h₁ : _} → Category o₁ h₁ → Category o₁ h₁
Category.Ob    (C ^op) = Category.Ob C
Category.Hom   (C ^op) x y = Category.Hom C y x
Category.id    (C ^op) = Category.id C
Category._∘_   (C ^op) f g = Category._∘_ C g f
```

Composition in the opposite category $C^{op}$ is "backwards" with
respect to $C$: $f \circ_{op} g = g \circ f$. This inversion, applied
twice, ends up equal to what we started with by the nature of
computation - An equality that arises like this, automatically from what
Agda computes, is called _definitional_.

```
Category.idr   (C ^op) = Category.idl C
Category.idl   (C ^op) = Category.idr C
```

The left and right identity laws are swapped for the construction of the
opposite category: For `idr`{.Agda} one has to show $f \circ_{op}
\mathrm{id} = f$, which computes into having to show that $\mathrm{id}
\circ_op{f} = f$. The case for `idl`{.Agda} is symmetric.

```
Category.assoc₁ (C ^op) = λ f g h → Category.assoc₂ C h g f
Category.assoc₂ (C ^op) = λ f g h → Category.assoc₁ C h g f
```

For associativity, consider the case of `assoc₁`{.Agda} for the
opposite category $C^{op}$. What we have to show is - by the type of
`assoc₁`{.Agda} - $f \circ_{op} (g \circ_{op} h) = (f \circ_{op} g)
\circ_{op} h$. This computes into $(h \circ g) \circ f = h \circ (g
\circ f)$ - which is exactly what `assoc₂`{.Agda} in $C$ shows! The case
for `assoc₂`{.Agda} is symmetric.

Taking opposite categories is an involution. By the
`assoc₁`{.Agda}/`assoc₂`{.Agda} trick, this is definitional:

```
_ : {o₁ h₁ : _} {C : Category o₁ h₁} → (C ^op) ^op ≡ C
_ = refl
```

## The category of Sets

Perhaps the most natural example of a category as "objects +
relationships" to formulate in type theory is the category of types
(relative to a level) and functions between them. 

```
Sets : (o : _) → Category (lsuc o) o
Category.Ob (Sets o) = Set o
Category.Hom (Sets o) A B = A → B
Category.id (Sets o) x = x
Category._∘_ (Sets o) f g x = f (g x)
Category.idr (Sets o) f = refl
Category.idl (Sets o) f = refl
Category.assoc₁ (Sets o) f g h = refl
Category.assoc₂ (Sets o) f g h = refl
```

This is not the simplest category - that honour goes to the empty
category - but it _is_ fundamental in many constructions, so it gets a
place in this module.


## The product of Categories

```
module _ {o₁ h₁ o₂ h₂ : _} (C : Category o₁ h₁) (D : Category o₂ h₂) where
  private
    module C = Category C
    module D = Category D

  _×Cat_ : Category _ _
```

Since categories assemble into a category, we can consider categorical
structure on categories themselves, and, specifically,
[limits](agda://Category.Limit.Base). Cat is a complete category, which
means it has all limits; Here, we construct products in Cat.

The product of two categories given here is more general than the
[Cartesian structure on Cat](agda://Category.Instances.Cat.Cartesian)
must be, since here we can take products of categories with objects/homs
in different universes.

```
  Category.Ob _×Cat_ = C.Ob × D.Ob
  Category.Hom _×Cat_ (A , A') (B , B') = C.Hom A B × D.Hom A' B'
  Category.id _×Cat_ = C.id , D.id
  Category._∘_ _×Cat_ (fc , fd) (gc , gd) = fc C.∘ gc , fd D.∘ gd 
  Category.idr _×Cat_ f = ×Path (C.idr _) (D.idr _)
  Category.idl _×Cat_ f = ×Path (C.idl _) (D.idl _)
  Category.assoc₁ _×Cat_ f g h = ×Path (C.assoc₁ _ _ _) (D.assoc₁ _ _ _)
  Category.assoc₂ _×Cat_ f g h = ×Path (C.assoc₂ _ _ _) (D.assoc₂ _ _ _)
```

# Functors

```
record
  Functor
    {o₁ h₁ o₂ h₂}
    (C : Category o₁ h₁)
    (D : Category o₂ h₂)
  : Set (o₁ ⊔ h₁ ⊔ o₂ ⊔ h₂)
  where

  private
    module C = Category C
    module D = Category D
```

Since a category is an algebraic structure, there is a natural
definition of _homomorphism of categories_ defined in the same fashion
as, for instance, a _homomorphism of groups_. Since this kind of
morphism is ubiquitous, it gets a shorter name: `Functor`{.Agda}.

Alternatively, functors can be characterised as the "proof-relevant
version" of a monotone map: A monotone map is a map $F : C \to D$ which
preserves the ordering relation, $x \le y \to F(x) \le F(y)$.
Categorifying, "preserves the ordering relation" becomes a function
between Hom-sets.

A great deal of mathematical constructions are functorial. As an
example, the projection of the object set of a category - `Ob`{.Agda} -
extends to a [forgetful functor] $\mathrm{Cat}_{o,h} \to
\mathrm{Sets}_{o}$. The construction of [endomorphism monoids] extends
to [a functor] from $\mathrm{Cat}_*$ (the category of [pointed
categories]) to $\mathrm{Mon}$, which yields the endomorphism monoid of
the distinguished object.

[forgetful functor]: agda://Category.Instances.Cat.Disc#Ob-functor
[endomorphism monoids]: agda://Algebra.Monoid#endo
[pointed categories]: agda://Category.Instances.Cat.Cartesian#Cat*
[a functor]: agda://Algebra.Monoid.Endomorphism#Endomorphism

In examples more familiar to programmers, most kinds of containers are
functors. Sometimes, these even admit descriptions in terms of abstract
nonsense: The type of [lists] can be described as the [monad from an
adjunction] of the [free-forgetful adjunction] characterising
$\mathrm{Mon}$ as an algebraic category.

[free-forgetful adjunction]: agda://Algebra.Monoid#Free⊣Forget
[monad from an adjunction]: agda://Category.Functor.Adjoints.Monad
[linked lists]: agda://1Lab.Data.List#List

```
  field
    F₀ : C.Ob → D.Ob
    F₁ : {x y : _} → C.Hom x y → D.Hom (F₀ x) (F₀ y)
```

A Functor $F : C \to D$ consists of a `function between the object
sets`{.Agda ident="F₀"} - $F_0 : \mathrm{Ob}(C) \to \mathrm{Ob}(D)$, and
a `function between Hom-sets`{.Agda ident="F₁"} - which takes $f : x \to
y \in C$ to $F_1(f) : F_0(x) \to F_0(y) \in D$.

```
  field
    F-id : {x : _} → F₁ (C.id {x}) ≡! D.id
    F-∘ : {x y z : _} (f : C.Hom y z) (g : C.Hom x y)
        → F₁ (f C.∘ g) ≡! F₁ f D.∘ F₁ g
```

Furthermore, the morphism mapping $F_1$ must be homomorphic: Identity
morphisms are taken to identity morphisms (`F-id`{.Agda}) and
compositions are taken to compositions (`F-∘`{.Agda}).

<!--
```
  -- Alias for F₀ for use in Functor record modules.
  ₀ : C.Ob → D.Ob
  ₀ = F₀

  -- Alias for F₁ for use in Functor record modules.
  ₁ : {x y : _} → C.Hom x y → D.Hom (F₀ x) (F₀ y)
  ₁ = F₁
```
-->

Functors also have duals: The opposite of $F : C \to D$ is $F^{op} :
C^{op} \to D^{op}$.

```
  op : Functor (C ^op) (D ^op)
  F₀ op      = F₀
  F₁ op      = F₁
  F-id op    = F-id
  F-∘ op f g = F-∘ g f
```

## Composition

```
_F∘_ : {o₁ h₁ o₂ h₂ o₃ h₃ : _}
       {C : Category o₁ h₁} {D : Category o₂ h₂} {E : Category o₃ h₃}
     → Functor D E → Functor C D → Functor C E
```

Functors, being made up of functions, can themselves be composed. The
object mapping of $(F \circ G)$ is given by $F_0 \circ G_0$, and
similarly for the morphism mapping. Alternatively, composition of
functors is a categorification of the fact that monotone maps compose.

```
_F∘_ {C = C} {D} {E} F G = record { F₀ = F₀ ; F₁ = F₁ ; F-id = F-id ; F-∘ = F-∘ }
  where
    module C = Category C
    module D = Category D
    module E = Category E

    module F = Functor F
    module G = Functor G

    F₀ : C.Ob → E.Ob
    F₀ x = F.F₀ (G.F₀ x)

    F₁ : {x y : C.Ob} → C.Hom x y → E.Hom (F₀ x) (F₀ y)
    F₁ f = F.F₁ (G.F₁ f)
```

To verify that the result is functorial, equational reasoning is employed, using
the witnesses that $F$ and $G$ are functorial.

```
    F-id : {x : C.Ob} → F₁ (C.id {x}) ≡ E.id {F₀ x}
    F-id {x} =
        F.F₁ (G.F₁ C.id) ≡⟨ ap F.F₁ G.F-id ⟩
        F.F₁ D.id        ≡⟨ F.F-id ⟩
        E.id             ∎

    F-∘ : {x y z : C.Ob} (f : C.Hom y z) (g : C.Hom x y)
        → F₁ (f C.∘ g) ≡ (F₁ f E.∘ F₁ g)
    F-∘ f g =
        F.F₁ (G.F₁ (f C.∘ g))     ≡⟨ ap F.F₁ (G.F-∘ f g) ⟩
        F.F₁ (G.F₁ f D.∘ G.F₁ g)  ≡⟨ F.F-∘ _ _ ⟩
        F₁ f E.∘ F₁ g             ∎
```

The identity function (twice) is a functor $C \to C$. These composition
and identities assemble into a category, where the objects are
categories: [Cat](agda://Category.Instances.Cat.Base#Cat). The
construction of Cat is not in this module for performance reasons.

```
Id : {o₁ h₁ : _} {C : Category o₁ h₁} → Functor C C
Functor.F₀ Id x = x
Functor.F₁ Id f = f
Functor.F-id Id = refl
Functor.F-∘ Id f g = refl
```

# Natural Transformations

Another common theme in category theory is that roughly _every_ concept
can be considered the objects of a category. This is the case for
functors, as well! The functors between $C$ and $D$ assemble into a
category, notated $[C, D]$ - the [functor category] between $C$ and $D$.
The notation $[-,-]$ stands, more generally, for the [internal hom] of a
category - functor categories are the [internal homs of Cat].


[internal homs of Cat]: agda://Category.Instances.Cat.Closed
[functor category]: agda://Category.Instances.FunctorCat#FunctorCat
[internal hom]: agda://Category.Structure.CartesianClosed#[-,-]

```
record _=>_ {o₁ h₁ o₂ h₂}
            {C : Category o₁ h₁}
            {D : Category o₂ h₂} 
            (F G : Functor C D)
      : Set (o₁ ⊔ h₁ ⊔ h₂)
  where
  constructor NT
```

The morphisms between functors are called **natural transformations**. A
natural transformation $F \Rightarrow G$ can be thought of as a way of
turning $F(x)$s into $G(x)$s that doesn't involve any "arbitrary
choices".

**For programmers**: Since the concept of natural transformation, by
itself, is not very enlightening, there are [concrete examples] of
natural transformations.

[concrete examples]: agda://Category.Functor.NatTrans.examples

```
  private
    module F = Functor F
    module G = Functor G
    module D = Category D

  field
    η : (x : _) → Category.Hom D (F.F₀ x) (G.F₀ x)
```

The transformation itself is given by `η`{.Agda}, the family of
_components_, where the component at $x$ is a map $F(x) \to G(x)$. The
"without arbitrary choices" part is encoded in the field
`is-natural`{.Agda}, which encodes commutativity of the square below:

~~~{.quiver}
\[\begin{tikzcd}
  {F_0(x)} && {F_0(y)} \\
  \\
  {G_0(x)} && {G_0(y)}
  \arrow["{\eta_x}"', from=1-1, to=3-1]
  \arrow["{\eta_y}", from=1-3, to=3-3]
  \arrow["{F_1(f)}", from=1-1, to=1-3]
  \arrow["{G_1(f)}"', from=3-1, to=3-3]
\end{tikzcd}\]
~~~

```
    is-natural : (x y : _) (f : Category.Hom C x y)
               → η y D.∘ F.F₁ f ≡! G.F₁ f D.∘ η x
```

Alternatively, natural transformations can be thought of as [homotopies
between functors](agda://Category.Functor.NatTrans.Homotopy). That
module contains a direct proof of the correspondence, but an argument by
abstract nonsense is even simpler to write down: Since [Cat is cartesian
closed](agda://Category.Instances.Cat.Closed#Cat-closed), there is [an
isomorphism of Hom-sets](agda://Category.Functor.Adjoints) from the
[tensor-hom
adjunction](agda://Category.Structure.CartesianClosed#Tensor⊣Hom)

$$
\mathrm{Hom}_{\mathrm{Cat}}(C \times \left\{0 \le 1\right\}, D) \simeq
\mathrm{Hom}_{\mathrm{Cat}}(\left\{0 \le 1\right\}, [C, D])
$$

Since a functor from [the interval
category](agda://Category.Instances.Interval) $\left\{0 \le 1\right\}$
amounts to a choice of morphism, we conclude that a functor $C \times
\left\{0\le 1\right\} \to D$ is the same as a natural transformation $C
\Rightarrow D$. There is more to this correspondence: the [geometric
realisation] of a natural transformation is a [homotopy in the
topological sense].

[geometric realisation]: https://ncatlab.org/nlab/show/geometric+realization+of+categories
[homotopy in the topological sense]: https://ncatlab.org/nlab/show/homotopy

Natural transformations also dualize. The opposite of $\eta : F
\Rightarrow G$ is $\eta^{op} : G^{op} \Rightarrow F^{op}$.

```
  op : Functor.op G => Functor.op F
  op = record
    { η = η
    ; is-natural = λ x y f → sym (is-natural _ _ f)
    }
```

## Natural Isomorphism

```
record
  _≅_ {o₁ h₁ o₂ h₂ : _} {C : Category o₁ h₁} {D : Category o₂ h₂}
       (F G : Functor C D)
  : Set (o₁ ⊔ h₁ ⊔ h₂)
  where
  private
    module D = Category D
  open _=>_
```

A natural transformation where all the `components`{.Agda ident=η} are
isomorphisms is called a `natural isomorphism`{.Agda ident=_≅_}. This
is equivalently a natural transformation with a two-sided inverse.
  
```
  field
    to   : F => G
    from : G => F
  
  field
    to-from : {x : Category.Ob C} → η from x D.∘ η to x ≡! D.id
    from-to : {x : Category.Ob C} → η to x D.∘ η from x ≡! D.id
```

Natural isomorphisms are the `isomorphisms`{.Agda ident=is-Iso} in
[functor categories](agda://Category.Instances.FunctorCat), but this
explicit characterisation improves compilation time by untangling the
dependency graph.

## Equivalence of Categories

```
record
  is-Equiv
    {o₁ h₁ o₂ h₂ : _}
    {C : Category o₁ h₁}
    {D : Category o₂ h₂}
    (F : Functor C D) : Set (o₁ ⊔ h₁ ⊔ o₂ ⊔ h₂)
  where
```

While we could consider [isomorphism of
categories](agda://Category.Diagrams#_≈_) as the notion of two
categories being the "same", a more useful notion is that of
_equivalence_ of categories.

A functor $F : C \to D$ is an equivalence of categories if there is a
functor $G : D \to C$, such that the composites $F \circ G$ and $G \circ
F$ are both _`naturally isomorphic`{.Agda ident=_≅_}_ to the identity
functor.

```
  field
    G : Functor D C
```

Using the perspective of [natural transformations as
homotopies](agda://Category.Functor.NatTrans.Homotopy), $G$ may be
called a _homotopy inverse_ to $F$. Indeed, we have that the geometric
realization of an equivalence of categories is a homotopy equivalence.

```
    linv : (F F∘ G) ≅ Id
    rinv : (G F∘ F) ≅ Id
```
