```agda
open import 1Lab.Base

open import Category

module Category.Diagrams {o₁ h₁} (C : Category o₁ h₁) where
```

Definitions of diagrams of various shapes in an arbitrary category. Of
course, the described diagrams might not exist - but they still have
descriptions.

```
private
  module C = Category.Category C
```

## Classes of Maps

An **isomorphism** is a morphism that `is iso`{.agda ident=is-Iso} -
that is, it has a two-sided inverse. The identity map is its own
inverse, and is therefore an iso independent of your choice of category.

```
record
  is-Iso
    {x y : Category.Ob C}
    (f : Category.Hom C x y)
  : Set h₁
  where
  constructor iso
  open Category.Category C

  field
    g : Hom y x
    linv : f ∘ g ≡! id
    rinv : g ∘ f ≡! id

_≈_ : C.Ob → C.Ob → Set h₁
_≈_ A B = Σ (C.Hom A B) is-Iso

id≈ : {A : C.Ob} → A ≈ A
id≈ = C.id , iso C.id (C.idl C.id) (C.idl C.id)
```

A morphism is _monic_ when precomposition with it is an injective function, that is:

$$
\mathrm{map} \circ f = \mathrm{map} \circ g \Longrightarrow f = g
$$

A _monomorphism_ is a morphism with a proof that it is monic.

```agda
is-Mono : {A B : C.Ob} → C.Hom A B → Set (o₁ ⊔ h₁)
is-Mono {A} map = {Z : C.Ob} {g₁ g₂ : C.Hom Z A} → map C.∘ g₁ ≡! map C.∘ g₂ → g₁ ≡ g₂

record _↪_ (A B : C.Ob) : Set (o₁ ⊔ h₁) where
  field
    map : C.Hom A B
    monic : is-Mono map
```

## Explicit descriptions of limits

More often than not, it's convenient to have a description of particular [limits] as
a concrete collection of a few morphisms and witnesses for commutativity
(and uniqueness) than it is to know there exists a terminal cone for a
particular diagram. Here are some:

[limits]: Category.Limit.Base.html#limits

### Pullbacks

```agda
record Pullback {P X Y Z : C.Ob}
                (p₁ : C.Hom P X) (p₂ : C.Hom P Y)
                (f : C.Hom X Z) (g : C.Hom Y Z)
       : Set (o₁ ⊔ h₁) where
```

A pullback is a limit diagram with the shape of a square, where the apex object is $P$:

```quiver
\[\begin{tikzcd}
  P & X \\
  Y & Z
  \arrow["g"', from=2-1, to=2-2]
  \arrow["{p_1}", from=1-1, to=1-2]
  \arrow["{p_2}"', from=1-1, to=2-1]
  \arrow["f", from=1-2, to=2-2]
  \arrow["\lrcorner"{anchor=center, pos=0.125}, draw=none, from=1-1, to=2-2]
\end{tikzcd}\]
```

```agda
  field
    -- This field witnesses commutativity of the square above
    commutes : f C.∘ p₁ ≡! g C.∘ p₂
```

The little corner decoration pointing from $P$ to $Z$ indicates that this square is indeed a limit
diagram, which means the object $P$ is universal: For any $Q$, $p_1'$, $p_2'$, the diagram below
commutes, and the dashed arrow is unique. `limiting`{.agda ident=Pb.limiting} is this arrow,
and `unique`{.agda ident=Pb.unique} witnesses its uniqueness.

~~~{.quiver .tall-1}
\[\begin{tikzcd}
  Q \\
  & P & X \\
  & Y & Z
  \arrow["g"', from=3-2, to=3-3]
  \arrow["{p_1}", from=2-2, to=2-3]
  \arrow["{p_2}"', from=2-2, to=3-2]
  \arrow["f", from=2-3, to=3-3]
  \arrow["\lrcorner"{anchor=center, pos=0.125}, draw=none, from=2-2, to=3-3]
  \arrow["{p_1'}", curve={height=-6pt}, from=1-1, to=2-3]
  \arrow["{p_2'}"', curve={height=6pt}, from=1-1, to=3-2]
  \arrow[dashed, from=1-1, to=2-2]
\end{tikzcd}\]
~~~

```agda
    limiting : {Q : C.Ob} {p₁' : C.Hom Q X} {p₂' : C.Hom Q Y}
             → f C.∘ p₁' ≡! g C.∘ p₂'
             → C.Hom Q P

    unique : {Q : C.Ob} {p₁' : C.Hom Q X} {p₂' : C.Hom Q Y} {i : _}
           → (eq : f C.∘ p₁' ≡! g C.∘ p₂')
           → p₁ C.∘ i ≡! p₁'
           → p₂ C.∘ i ≡! p₂'
           → i ≡ limiting eq
```

These two equations witness commutativity of the bigger diagram: $p_1'$
(resp $p_2'$) is a path from $Q$ to $X$ (resp $Y$), as is
$\operatorname{limiting} \circ\ p_1$ (resp $\operatorname{limiting} \circ\ 
p_2$), hence they must be equal.

```agda
    p₁∘limiting≡p₁' : {Q : C.Ob} {p₁' : C.Hom Q X} {p₂' : C.Hom Q Y}
                    → (eq : f C.∘ p₁' ≡! g C.∘ p₂')
                    → (p₁ C.∘ limiting eq) ≡! p₁'

    p₂∘limiting≡p₂' : {Q : C.Ob} {p₁' : C.Hom Q X} {p₂' : C.Hom Q Y}
                    → (eq : f C.∘ p₁' ≡! g C.∘ p₂')
                    → (p₂ C.∘ limiting eq) ≡! p₂'
```


### Monomorphisms as Pullbacks

Monomorphisms can also be characterised in terms of limits, as
witnessed by the theorems `Monic→Pullback`{.Agda} and `Pullback→Monic`{.Agda}. An
arrow $f : x \hookrightarrow y$ is a monomorphism if and only if the
square

~~~quiver
\[\begin{tikzcd}
  x & x \\
  x & y
  \arrow["f"', from=2-1, to=2-2]
  \arrow["{\operatorname{id}}", from=1-1, to=1-2]
  \arrow["{\operatorname{id}}"', from=1-1, to=2-1]
  \arrow["f", from=1-2, to=2-2]
\end{tikzcd}\]
~~~

is a pullback.

Why have the two characterisations? This correspondence is used in
proving that continuous functors preserve monomorphisms: "Being a
monomorphism" can be expressed in terms of structure a continuous
functor is guaranteed to preserve.

```agda
Monic→Pullback : {x y : C.Ob} {f : C.Hom x y}
               → is-Mono f
               → Pullback C.id C.id f f
Monic→Pullback mono =
  record
  { commutes = refl
  ; limiting = λ {Q} {p₁'} {p₂'} x₁ → p₁'
  ; unique = λ eq x₁ x₂ → sym (C.idl _) ∘p x₁
  ; p₁∘limiting≡p₁' = λ eq → C.idl _
  ; p₂∘limiting≡p₂' = λ eq → mono (ap (C._∘_ _) (C.idl _) ∘p eq)
  }

Pullback→Monic : {x y : C.Ob} {f : C.Hom x y}
               → Pullback C.id C.id f f
               → is-Mono f
Pullback→Monic f eq =
     sym (Pullback.p₁∘limiting≡p₁' f eq)
  ∘p Pullback.p₂∘limiting≡p₂' f eq
```

### Equalisers

```agda
record Equaliser {E A B : C.Ob} (f g : C.Hom A B) (e : C.Hom E A) : Set (o₁ ⊔ h₁)
  where
```

Given two morphisms $f, g : A \to B$, their _equaliser_ is the subobject
of the domain on which $f$ and $g$ agree. It's a limit diagram shaped
like a fork, in which the apex object is $E$:

~~~{.quiver .short-1}
\[\begin{tikzcd}
  E & A & B
  \arrow["f", shift left=1, from=1-2, to=1-3]
  \arrow["g"', shift right=1, from=1-2, to=1-3]
  \arrow["e", hook, from=1-1, to=1-2]
\end{tikzcd}\]
~~~

```agda
  field
    -- This field witnesses commutativity of the diagram above.
    commutes : f C.∘ e ≡! g C.∘ e
```

Since $E$ is a limit, if we have another arrow $e' : E' \to A$ which
equalises $f$ and $g$, then there exists a map - `limiting`{.Agda
ident=Eq.limiting} - taking $E' \to E$.

```agda
    limiting : {E' : C.Ob} {e' : C.Hom E' A}
             → f C.∘ e' ≡! g C.∘ e'
             → C.Hom E' E

    -- Uniqueness of the 'limiting' arrow
    unique : {E' : C.Ob} {e' : C.Hom E' A} {i : _}
           → (eq : f C.∘ e' ≡! g C.∘ e')
           → e' ≡! e C.∘ i
           → i ≡ limiting eq
```

This equation witnesses commutativity of the complete diagram, with $E'$ and $E$:

~~~quiver
\[\begin{tikzcd}
  E & A & B \\
  {E'}
  \arrow["f", shift left=1, from=1-2, to=1-3]
  \arrow["g"', shift right=1, from=1-2, to=1-3]
  \arrow["e", hook, from=1-1, to=1-2]
  \arrow["{\mathrm{limiting}}", from=2-1, to=1-1]
  \arrow["{e'}"', from=2-1, to=1-2]
\end{tikzcd}\]
~~~

```agda 
    e∘limiting≡e' : {E' : C.Ob} {e' : C.Hom E' A}
                  → (eq : f C.∘ e' ≡! g C.∘ e')
                  → (e C.∘ limiting eq) ≡! e'
```

If $e : E \to A$ is an equaliser arrow, then it is a monomorphism
(subobject inclusion): Given two arrows $h, i : E' \to E$, and a witness
that $e \circ h \equiv e \circ i$, we can build a proof that $h \equiv
i$.

```agda
  monic : {E' : _} {h i : C.Hom E' E} → e C.∘ h ≡ e C.∘ i → h ≡ i
  monic {h = h} {i} eq =
      h          ≡⟨ unique l (sym eq) ⟩
      limiting l ≡⟨ sym (unique l refl) ⟩
      i          ∎
```

The proof is by noticing that, since both $h, i : E' \to E$ compose
to give arrows $E' \to A$ which make the diagram below commute, then
they both have to be equal to the `limiting`{.Agda ident=Eq.limiting}
arrow.

```quiver
\[\begin{tikzcd}
  E & A & B \\
  {E'}
  \arrow["i"', shift right=1, from=2-1, to=1-1]
  \arrow["h", shift left=1, from=2-1, to=1-1]
  \arrow["e", from=1-1, to=1-2]
  \arrow["f", shift left=1, from=1-2, to=1-3]
  \arrow["g"', shift right=1, from=1-2, to=1-3]
  \arrow[shift left=1, from=2-1, to=1-2]
  \arrow[shift right=1, from=2-1, to=1-2]
\end{tikzcd}\]
```

```agda
    where
      l =
        f C.∘ e C.∘ i   ≡⟨ C.assoc₂ _ _ _ ⟩
        (f C.∘ e) C.∘ i ≡⟨ ap (λ e → e C.∘ i) commutes ⟩
        (g C.∘ e) C.∘ i ≡⟨ C.assoc₁ _ _ _ ⟩
        g C.∘ e C.∘ i   ∎ 
```

<!--
```agda
private
  module Pb = Pullback
  module Eq = Equaliser

_ = Pb.limiting
_ = Pb.unique
_ = Eq.limiting
_ = Eq.unique
```
-->
