open import 1Lab.Base

open import Category.Structure.Terminal
open import Category.Functor.Adjoints
open import Category.Limit.Base
open import Category

open Functor

module
  Category.Limit.Continuous.Adjoint
  {o₁ h₁ o₂ h₂ : _}
  {C : Category o₁ h₁}
  {D : Category o₂ h₂}
  {L : Functor C D}
  {R : Functor D C}
  (L⊣R : L ⊣ R)
  where

private
  module C = Category.Category C
  module D = Category.Category D
  module L = Functor L
  module R = Functor R

open _⊣_ L⊣R
open _=>_

module _ {o₃ h₃ : _} {J : Category o₃ h₃} {F : Functor J D} where

  Radj→Limit : Limit F → Limit (R F∘ F)
  Radj→Limit lim = record { ⊤ = ⊤ ; terminal = contract ! !-unique } where
    module lim = Cone (Terminal.⊤ lim)

    ⊤ = record
          { apex     = R.₀ lim.apex
          ; ψ        = λ x → R.₁ (lim.ψ x)
          ; commutes = λ f → sym (R.F-∘ _ _) ∘p ap R.₁ (lim.commutes f)
          }
    
    K' : Cone (R F∘ F) → Cone F
    K' K =
      record { apex     = L.₀ K.apex
             ; ψ        = λ x → η counit _ D.∘ L.₁ (K.ψ x)
             ; commutes = λ {x} {y} f →
                Functor.₁ F f D.∘ η counit _ D.∘ L.₁ (K.ψ x)              ≡⟨ D.assoc₂ _ _ _ ∘p ap (λ e → e D.∘ L.₁ _) (sym (is-natural counit _ _ _)) ∘p D.assoc₁ _ _ _ ⟩
                η counit (F₀ F y) D.∘ L.₁ (R.₁ (F₁ F f)) D.∘ L.₁ (K.ψ x)  ≡⟨ ap (λ e → η counit _ D.∘ e) (sym (L.F-∘ _ _)) ⟩
                η counit (F₀ F y) D.∘ L.₁ (R.₁ (F₁ F f) C.∘ K.ψ x)        ≡⟨ ap (λ e → η counit _ D.∘ L.₁ e) (K.commutes f) ⟩
                η counit _ D.∘ L.₁ _                                      ∎
             }
      where
        module K = Cone K

    ! : {K : _} → Cone=> (R F∘ F) K ⊤
    ! {K} =
      record { hom      = R.₁ hom' C.∘ η unit _
             ; commutes = comm
             }
      where
        hom' = Cone=>.hom (Terminal.! lim {K' K})
        commutes = Cone=>.commutes (Terminal.! lim {K' K})

        comm : {o : _} → R.₁ (lim.ψ o) C.∘ R.₁ hom' C.∘ η unit _ ≡! Cone.ψ K o
        comm {o} =
          R.₁ (lim.ψ o) C.∘ R.₁ hom' C.∘ η unit _                   ≡⟨ C.assoc₂ _ _ _ ∘p ap (λ e → e C.∘ η unit _) (sym (R.F-∘ _ _)) ⟩
          R.₁ (lim.ψ o D.∘ hom') C.∘ η unit _                       ≡⟨ ap (λ e → R.₁ e C.∘ _) commutes ⟩
          R.₁ (η counit _ D.∘ L.₁ (Cone.ψ K o)) C.∘ η unit _        ≡⟨ ap (λ e → e C.∘ η unit _) (R.F-∘ _ _) ∘p C.assoc₁ _ _ _ ⟩
          R.₁ (η counit _) C.∘ R.₁ (L.₁ (Cone.ψ K o)) C.∘ η unit _  ≡⟨ ap (λ e → R.₁ _ C.∘ e) (sym (is-natural unit _ _ _)) ∘p C.assoc₂ _ _ _ ⟩
          (R.F₁ (counit.η _) C.∘ η unit _) C.∘ Cone.ψ K o           ≡⟨ ap (λ e → e C.∘ Cone.ψ K _) zag ⟩
          C.id C.∘ Cone.ψ K o                                       ≡⟨ C.idl _ ⟩
          Cone.ψ K o                                                ∎
    
    !-unique : {K : _} (f : Cone=> _ K ⊤) → ! {K} ≡! f
    !-unique {K} f =
        Cone=>≡ _ (
          R.₁ hom' C.∘ η unit _                                       ≡⟨ ap (λ e → R.₁ e C.∘ _) (ap Cone=>.hom (isContr.paths (Terminal.terminal lim) f')) ⟩
          R.₁ (η counit _ D.∘ L.₁ (Cone=>.hom f)) C.∘ η unit _        ≡⟨ ap (λ e → e C.∘ η unit _) (R.F-∘ _ _) ∘p C.assoc₁ _ _ _ ⟩
          R.₁ (η counit _) C.∘ R.₁ (L.₁ (Cone=>.hom f)) C.∘ η unit _  ≡⟨ ap (λ e → R.₁ _ C.∘ e) (sym (is-natural unit _ _ _)) ∘p C.assoc₂ _ _ _ ⟩
          (R.F₁ (counit.η _) C.∘ η unit _) C.∘ Cone=>.hom f           ≡⟨ ap (λ e → e C.∘ _) zag ⟩
          C.id C.∘ Cone=>.hom f                                       ≡⟨ C.idl _ ⟩
          Cone=>.hom f                                                ∎
        )
      where
        hom' = Cone=>.hom (Terminal.! lim {K' K})
        commutes = Cone=>.commutes (Terminal.! lim {K' K})

        f' : Cone=> _ (K' K) (Terminal.⊤ lim)
        f' =
          record { hom      = η counit _ D.∘ L.₁ (Cone=>.hom f)
                 ; commutes = λ {o} →
                    lim.ψ o D.∘ η counit _ D.∘ L.₁ (Cone=>.hom f)               ≡⟨ D.assoc₂ _ _ _ ∘p ap (λ e → e D.∘ L.₁ _) (sym (is-natural counit _ _ _)) ∘p D.assoc₁ _ _ _ ⟩
                    η counit _ D.∘ (L.₁ (R.₁ (lim.ψ o)) D.∘ L.₁ (Cone=>.hom f)) ≡⟨ ap (λ e → _ D.∘ e) (sym (L.F-∘ _ _)) ⟩
                    η counit _ D.∘ (L.₁ (R.₁ (lim.ψ o) C.∘ Cone=>.hom f))       ≡⟨ ap (λ e → _ D.∘ L.₁ e) (Cone=>.commutes f) ⟩
                    η counit _ D.∘ L.₁ (Cone.ψ K o)                             ∎
                 }

Radj→Continuous : {o h : _} → Continuous {o} {h} R
Radj→Continuous L =
  subst (λ e → {A : Cone (R F∘ _)} → isContr (Cone=> _ A e))
        p
        (Terminal.terminal (Radj→Limit L))
    where
      p : _ ≡! F-map-Cone R (Terminal.⊤ L)
      p = Cone≡ _ refl λ o → refl
