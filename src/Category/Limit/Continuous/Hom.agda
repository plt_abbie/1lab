open import 1Lab.Coeq
open import 1Lab.Base

open import Category.Structure.Terminal
open import Category.Functor.Hom
open import Category.Limit.Base
open import Category

module Category.Limit.Continuous.Hom {o₁ h₁ : _} {C : Category o₁ h₁} where
  private
    module C = Category.Category C

  module _ {A : Category.Ob C} {o₂ h₂ : _}
           {J : Category o₂ h₂}
           {D : Functor J C}
           (lim : Limit D)
    where
    private
      module J = Category.Category J
      module D = Functor D
      open Terminal lim

      HomD : Functor J (Sets _)
      HomD = Hom[ A ,-] F∘ D
    
    ⊤' : Cone HomD
    ⊤' = F-map-Cone Hom[ A ,-] ⊤

    module _ (K : Cone HomD) where
      private
        module K = Cone K

      KA : Cone.apex K → Cone D
      KA x =
        record
          { apex = A
          ; ψ = λ X → K.ψ X x
          ; commutes = λ f →
            ap (C._∘_ (D.F₁ f)) (sym (C.idr _)) ∘p happly (K.commutes f) _
          }
      
      terminate : Cone=> HomD K ⊤'
      Cone=>.hom terminate x = Cone=>.hom (! {A = KA x})
      Cone=>.commutes terminate =
        fun-ext λ z →
          Cone.ψ ⊤ _ C.∘ _ C.∘ C.id ≡⟨ ap (C._∘_ _) (C.idr _) ⟩
          Cone.ψ ⊤ _ C.∘ _          ≡⟨ Cone=>.commutes (! {A = KA z}) ⟩
          K.ψ _ z                   ∎
      
    terminate-unique : {K : _} → (f : Cone=> HomD K ⊤') → f ≡! terminate K
    terminate-unique {K} f =
      Cone=>≡ _ (
        let
          f' : {x : _} → Cone=> D (KA K x) ⊤
          f' {x} = record { hom = Cone=>.hom f x
                          ; commutes = ap (C._∘_ _) (sym (C.idr _))
                                    ∘p happly (Cone=>.commutes f) _
                          }
        in fun-ext λ x →
          Cone=>.hom f x                                       ≡⟨ ap Cone=>.hom (sym (isContr.paths (Terminal.terminal lim) f')) ⟩
          Cone=>.hom (isContr.center (Terminal.terminal lim))  ∎
      )
      where
    
    Hom-respects-Lim : Limit HomD
    Hom-respects-Lim =
      record
        { ⊤ = ⊤'
        ; terminal = contract (terminate _) λ f → sym (terminate-unique f)
        }
  
  Hom-continuous : {o₁ h₁ : _} (Ob : _) → Continuous {o₁} {h₁} Hom[ Ob ,-]
  Hom-continuous Ob L = terminal
    where open Terminal (Hom-respects-Lim {A = Ob} L)
