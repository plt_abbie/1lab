```
open import 1Lab.Function.Properties
open import 1Lab.HProp
open import 1Lab.Coeq
open import 1Lab.Base

open import Category.Functor.Properties
open import Category.Functor.Adjoints
open import Category

open Category.Category
open Functor
open hProp

module Category.Instances.Props where
```

# The category of propositions

```
Props : (ℓ : _) → Category _ _
Ob (Props ℓ) = hProp ℓ
Hom (Props ℓ) x x₁ = carrier x → carrier x₁
```

The collection of `hProp`{.Agda}s determines a full subcategory of
`Sets`{.Agda} spanned by those sets which are h-propositions.

```
id (Props ℓ) x = x
_∘_ (Props ℓ) f g x = f (g x)
idr (Props ℓ) f = refl
idl (Props ℓ) f = refl
assoc₁ (Props ℓ) f g h = refl
assoc₂ (Props ℓ) f g h = refl
```

```
Forget : {ℓ : _} → Functor (Props ℓ) (Sets ℓ)
F₀ Forget = carrier
F₁ Forget f = f
F-id Forget = refl
F-∘ Forget f g = refl

Forget-ff : {ℓ : _} → FullyFaithful (Forget {ℓ})
Forget-ff = ff (λ x → x) (λ x → refl) (λ x → refl)
```

# The Propositional Truncation Functor

```
Free : {ℓ : _} → Functor (Sets ℓ) (Props ℓ)
```

The _propositional truncation_ `∥ X ∥`{.Agda ident="∥_∥"} of X
determines the object part of a functor from `Sets`{.Agda} to
`Props`{.Agda}.

```
Free = func where
  map : {X Y : _} (f : X → Y)
      → carrier ∥ X ∥ → carrier ∥ Y ∥
  map {Y = Y} f arg = unsquash {B = ∥ Y ∥} (λ x → squash (f x)) arg

  map-id : {X : _} (x : carrier ∥ X ∥)
         → map (λ x → x) x ≡ x
  map-id {X} x = unsquash' (λ x → P x) (λ x₁ → refl) x where
    P : carrier ∥ X ∥ → hProp _
    carrier (P x) = map (λ x → x) x ≡ x
    prop (P x) _ _ = UIP _ _

  map-comp : {X Y Z : _} (f : X → Y) (g : Y → Z)
             (x : carrier ∥ X ∥)
           → map g (map f x) ≡ map (λ x → g (f x)) x
  map-comp {X} f g x = unsquash' (λ x → P x) (λ x₁ → refl) x where
    P : carrier ∥ X ∥ → hProp _
    carrier (P x) = map g (map f x) ≡ map (λ x → g (f x)) x
    prop (P x) _ _ = UIP _ _

  func : Functor (Sets _) (Props _)
  F₀ func X = ∥ X ∥
  F₁ func = map
  F-id func {x} = fun-ext map-id
  F-∘ func f g = sym (fun-ext (map-comp g f))
```

## Free propositions

```
Free⊣Forget : {ℓ : _} → Free {ℓ} ⊣ Forget
```

Since the propositional truncation functor is `left adjoint`{.Agda
ident="_⊣_"} to the inclusion `Props ↪ Sets`{.Agda ident=Forget}, we can
regard objects in the image of `∥_∥`{.Agda} as being _free propositions_
on a Set.

```
_⊣_.unit Free⊣Forget = NT (λ x a → squash a) λ x y f → refl
_⊣_.counit Free⊣Forget = NT (λ x x₁ → x₁ x (λ x → x)) λ x y f → fun-ext λ sq →
  let
    p : carrier ∥ carrier x ∥ → hProp _
    p inh = record
           { carrier = inh ∥ carrier y ∥ (λ x Q r → r (f x)) y (λ x → x)
                     ≡! f (inh x (λ x → x))
           ; prop = λ _ _ → UIP _ _
           }
  in unsquash' p (λ x₁ → refl) sq
_⊣_.zig Free⊣Forget {A} = fun-ext λ x →
  let
    p : carrier ∥ A ∥ → hProp _
    p inh = record { carrier = inh (∥ carrier ∥ A ∥ ∥) (λ x Q r → r (λ Q r → r x))
                                   ∥ A ∥ (λ x → x)
                            ≡! inh
                  ; prop = λ _ _ → UIP _ _ }
  in unsquash' p (λ _ → refl) x
_⊣_.zag Free⊣Forget = refl
```
