open import 1Lab.Coeq
open import 1Lab.Base

open import Category.Instances.Cat.Base
open import Category.Diagrams
open import Category.Equality
open import Category.Slices
open import Category

open Functor

module
  Category.Instances.Slice.Twice
  {o h : _} {C : Category o h}
  {x y : Category.Ob C}
  (f : Category.Hom C x y)
  where

private
  module C = Category.Category C

Reindex : Functor (Slice (Slice C y) (x , f)) (Slice C x)
F₀ Reindex ((d , f) , p , q) = _ , p
F₁ Reindex ((fst , p) , q) = fst , ap Σ.fst q
F-id Reindex {(fst , snd₁) , fst₁ , snd} = Σ-Path refl (UIP _ _)
F-∘ Reindex ((_ , _) , _) ((_ , _) , _) = Σ-Path refl (UIP _ _)

reindex-iso : is-Iso (Cat _ _) Reindex
reindex-iso = iso unindex p q where
  unindex : Functor (Slice C x) (Slice (Slice C y) (x , f))
  F₀ unindex (d , r) = (d , f C.∘ r) , r , refl
  F₁ unindex (fst , snd) = (fst , C.assoc₁ _ _ _ ∘p ap (C._∘_ f) snd)
                         , Σ-Path! snd (UIP _ _)
  F-id unindex = Σ-Path! (Σ-Path refl (UIP _ _)) (UIP _ _)
  F-∘ unindex f g = Σ-Path! (Σ-Path refl (UIP _ _)) (UIP _ _)

  p : _
  p = Functor≡ refl λ _ → Σ-Path refl (UIP _ _)

  q : _
  q = Functor≡ (fun-ext lemma) λ x → lemma' x
    where
      lemma : (x₁ : Category.Ob (Slice (Slice C y) (x , f)))
            → ((Σ.fst (Σ.fst x₁) , f C.∘ Σ.fst (Σ.snd x₁)) , Σ.fst (Σ.snd x₁) , refl)
            ≡! x₁
      lemma ((fst , .((C Category.∘ f) fst₁)) , fst₁ , refl) = refl

      lemma' : {a b : _} (f : Category.Hom (Slice (Slice C y) (x , f)) a b)
             → F₁ (unindex F∘ Reindex) f ≡ f
      lemma' {a} {b} f with lemma a | lemma b
      ... | refl | refl = Σ-Path! (Σ-Path refl (UIP _ _)) (UIP _ _)
