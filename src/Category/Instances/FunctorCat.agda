open import 1Lab.Base

open import Category.Equality
open import Category

import Category.Associativity

module Category.Instances.FunctorCat where

module _ {o₁ h₁ o₂ h₂ : _} (C : Category o₁ h₁) (D : Category o₂ h₂) where
  private
    module C = Category.Category C
    module D = Category.Category D
  
  open _=>_

  -- Given a category C and D, we can consider the category of functors
  -- [C, D], where maps are natural transformations.
  --
  -- This category has object set big enough to fit all the data and C
  -- and D, and homs big enough to fit both C and D's morphisms /and/
  -- C's objects. This is because natural transformations have a
  -- dependent product over C.Ob.
  FunctorCat : Category (o₁ ⊔ o₂ ⊔ h₁ ⊔ h₂) (o₁ ⊔ h₁ ⊔ h₂)
  Category.Ob    FunctorCat = Functor C D
  Category.Hom   FunctorCat F G = F => G
  Category.id    FunctorCat {F} =
    NT (λ x → D.id)
       (λ x y f →
        D.id D.∘ Functor.F₁ F f ≡⟨ D.idl _ ⟩
        Functor.F₁ F f          ≡⟨ sym (D.idr _) ⟩
        Functor.F₁ F f D.∘ D.id ∎
       )
  Category._∘_ FunctorCat {F} {G} {H} n1 n2 = NT eta isn where
    eta : _
    eta x = η n1 x D.∘ η n2 x

    isn : _
    isn x y f = 
      (η n1 y D.∘ η n2 y) D.∘ (Functor.F₁ F f)      ≡⟨ D.assoc₁ _ _ _ ⟩
      η n1 y D.∘ (η n2 y D.∘ Functor.F₁ F f)        ≡⟨ ap (D._∘_ (η n1 y)) (is-natural n2 _ _ _) ⟩
      η n1 y D.∘ (Functor.F₁ G f D.∘ η n2 x)        ≡⟨ D.assoc₂ _ _ _ ⟩
      ((η n1 y) D.∘ (Functor.F₁ G f)) D.∘ (η n2 x)  ≡⟨ ap (λ e → e D.∘ (η n2 x)) (is-natural n1 _ _ _) ⟩
      (Functor.F₁ H f D.∘ (η n1 x)) D.∘ (η n2 x)    ≡⟨ D.assoc₁ _ _ _ ⟩
      Functor.F₁ H f D.∘ eta x                      ∎

  Category.idr   FunctorCat f = NT≡ λ _ → D.idr _
  Category.idl   FunctorCat f = NT≡ λ _ → D.idl _
  Category.assoc₁ FunctorCat f g h = NT≡ λ _ → D.assoc₁ _ _ _
  Category.assoc₂ FunctorCat f g h = NT≡ λ _ → D.assoc₂ _ _ _

_≅⟨_⟩_ : {o₁ o₂ h₁ h₂ : _} {C : Category o₁ h₁} {D : Category o₂ h₂}
            (F : Functor C D) {G H : Functor C D}
          → F ≅ G → G ≅ H → F ≅ H
_≅⟨_⟩_ {C = C} {D} F {G} {H} I J = r where
  open _=>_
  open _≅_
  module C = Category.Category C
  module D = Category.Category D
  module I = _≅_ I
  module J = _≅_ J
  module Func = Category.Category (FunctorCat C D)

  module AssD = Category.Associativity D

  r : F ≅ H
  to r = J.to Func.∘ I.to
  from r = I.from Func.∘ J.from
  to-from r = 
      (η I.from _ D.∘ η J.from _) D.∘ η J.to _ D.∘ η I.to _ ≡⟨ sym AssD.first2-out-of-4 ∘p AssD.second2-out-of-4 ⟩
      η I.from _ D.∘ (η J.from _ D.∘ η J.to _) D.∘ η I.to _ ≡⟨ ap (λ e → _ D.∘ e D.∘ _) J.to-from ⟩
      η I.from _ D.∘ D.id D.∘ η I.to _                      ≡⟨ ap (λ e → _ D.∘ e) (D.idl _) ⟩
      η I.from _ D.∘ η I.to _                               ≡⟨ I.to-from ⟩
      D.id                                                  ∎
  from-to r = 
      (η J.to _ D.∘ η I.to _) D.∘ η I.from _ D.∘ η J.from _ ≡⟨ sym AssD.first2-out-of-4 ∘p AssD.second2-out-of-4 ⟩
      η J.to _ D.∘ (η I.to _ D.∘ η I.from _) D.∘ η J.from _ ≡⟨ ap (λ e → _ D.∘ e D.∘ _) I.from-to ⟩
      η J.to _ D.∘ D.id D.∘ η J.from _                      ≡⟨ ap (λ e → _ D.∘ e) (D.idl _) ⟩
      η J.to _ D.∘ η J.from _                               ≡⟨ J.from-to ⟩
      D.id                                                  ∎

_≅∎ : {o₁ o₂ h₁ h₂ : _} {C : Category o₁ h₁} {D : Category o₂ h₂}
      (F : Functor C D)
    → F ≅ F
_≅∎ {C = C} {D} F =
  record { to = Func.id ; from = Func.id ; to-from = D.idl _ ; from-to = D.idl _ }
  where
    module Func = Category.Category (FunctorCat C D)
    module D = Category.Category D

infixr 2 _≅⟨_⟩_
infix  3 _≅∎
