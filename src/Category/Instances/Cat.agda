
module Category.Instances.Cat where

open import Category.Instances.Cat.Base public
open import Category.Instances.Cat.Cartesian public
open import Category.Instances.Cat.Closed public
open import Category.Instances.Cat.Whisker public
