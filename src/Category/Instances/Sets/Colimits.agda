open import 1Lab.Coeq
open import 1Lab.Base

open import Category.Structure.Cartesian
open import Category

module Category.Instances.Sets.Colimits where

Set-coproducts : {o₁ : _} → Cartesian (Sets o₁ ^op)
Cartesian._×C_ Set-coproducts A B = A ∐ B

-- ... the injections are constructors ...
Cartesian.π₁ Set-coproducts = left
Cartesian.π₂ Set-coproducts = right

-- ... case analysis is case analysis ...
Cartesian._h×_ Set-coproducts l r (left x) = l x
Cartesian._h×_ Set-coproducts l r (right x) = r x
Cartesian.π₁∘h×≡f Set-coproducts f g = refl
Cartesian.π₂∘h×≡g Set-coproducts f g = refl
Cartesian.h×-unique Set-coproducts p q =
  fun-ext λ { (left e)  → sym (happly p e)
            ; (right e) → sym (happly q e)
            }

-- ... the initial object is the empty set ...
Cartesian.⊤ Set-coproducts = Bot
Cartesian.terminal Set-coproducts = contract (λ ()) λ y → fun-ext (λ ())