open import 1Lab.Base

open import Category.Instances.Grpd.Base
open import Category.Structure.Cartesian
open import Category.Equality
open import Category.Groupoid
open import Category

module Category.Instances.Grpd.Cartesian where

Grpd-cartesian : {o₁ h₁ : _} → Cartesian (Grpd o₁ h₁)
Grpd-cartesian = r where
  open Functor
  open Cartesian

  r : Cartesian (Grpd _ _)
  _×C_ r A B =
    record { C = Groupoid.C A ×Cat Groupoid.C B
           ; _¯¹  = λ { (fst , snd) → (Groupoid._¯¹ A fst , Groupoid._¯¹ B snd) }
           ; linv =
              λ { (fst , snd) → ×Path (Groupoid.linv A fst) (Groupoid.linv B snd) }
           ; rinv =
              λ { (fst , snd) → ×Path (Groupoid.rinv A fst) (Groupoid.rinv B snd) }
           }
  π₁ r = record { F₀ = _×_.fst ; F₁ = _×_.fst ; F-id = refl ; F-∘ = λ f g → refl }
  π₂ r = record { F₀ = _×_.snd ; F₁ = _×_.snd ; F-id = refl ; F-∘ = λ f g → refl }
  _h×_ r F G =
    record { F₀   = λ x → F₀ F x , F₀ G x
           ; F₁   = λ x → F₁ F x , F₁ G x
           ; F-id = ×Path (F-id F) (F-id G)
           ; F-∘  = λ f g → ×Path (F-∘ F f g) (F-∘ G f g)
           }
  π₁∘h×≡f r f g = Functor≡ refl λ _ → refl
  π₂∘h×≡g r f g = Functor≡ refl λ _ → refl
  h×-unique r refl refl = Functor≡ refl λ _ → refl
  ⊤ r =
    record { C = record
                 { Ob = Top
                 ; Hom = λ x y → Top
                 ; id = tt
                 ; _∘_ = λ x x₁ → tt
                 ; idr = λ f → refl
                 ; idl = λ f → refl
                 ; assoc₁ = λ f g h → refl
                 ; assoc₂ = λ f g h → refl
                 }
           ; _¯¹  = λ x → tt
           ; linv = λ f → refl
           ; rinv = λ f → refl
           }
  
  terminal r =
    contract (record { F₀ = λ x → tt ; F₁ = λ x → tt
                                ; F-id = refl
                                ; F-∘ = λ f g → refl })
                        λ y → Functor≡ refl λ f → refl
