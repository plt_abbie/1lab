open import 1Lab.Base

open import Category.Diagrams
open import Category.Equality
open import Category.Groupoid
open import Category

module Category.Instances.Grpd.Base where

open Category.Category

Grpd : (o₁ h₁ : _) → Category _ _
Ob (Grpd o₁ h₁) = Groupoid o₁ h₁
Hom (Grpd o₁ h₁) G₁ G₂ = Functor (Groupoid.C G₁) (Groupoid.C G₂)
id (Grpd o₁ h₁) = Id
_∘_ (Grpd o₁ h₁) F G = F F∘ G
idr (Grpd o₁ h₁) f = Functor≡ refl λ x → refl
idl (Grpd o₁ h₁) f = Functor≡ refl λ x → refl
assoc₁ (Grpd o₁ h₁) f g h = Functor≡ refl λ x → refl
assoc₂ (Grpd o₁ h₁) f g h = Functor≡ refl λ x → refl

F-iso : {o₁ h₁ o₂ h₂ : _} {C : Category o₁ h₁} {D : Category o₂ h₂}
      → (F : Functor C D)
      → {x y : _} {f : Category.Hom C x y}
      → is-Iso C f → is-Iso D (Functor.F₁ F f)
F-iso {C = C} {D} F {f = f} (iso g linv rinv) = iso (Functor.F₁ F g) eq₁ eq₂ where
  module C = Category.Category C
  module D = Category.Category D
  open Functor F

  eq₁ = F₁ f D.∘ F₁ g ≡⟨ sym (F-∘ _ _) ⟩
        F₁ (f C.∘ g)  ≡⟨ ap F₁ linv ⟩
        F₁ C.id       ≡⟨ F-id ⟩
        D.id          ∎

  eq₂ = F₁ g D.∘ F₁ f ≡⟨ sym (F-∘ _ _) ⟩
        F₁ (g C.∘ f)  ≡⟨ ap F₁ rinv ⟩
        F₁ C.id       ≡⟨ F-id ⟩
        D.id          ∎
