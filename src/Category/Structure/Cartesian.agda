open import 1Lab.Base

open import Category.Structure.Terminal
open import Category

module Category.Structure.Cartesian where

record Cartesian {o₁ h₁ : _} (C : Category o₁ h₁) : Set (lsuc o₁ ⊔ lsuc h₁) where
  private
    module C = Category.Category C

  field
    _×C_ : C.Ob → C.Ob → C.Ob
  
  field
    π₁ : {a b : C.Ob} → C.Hom (a ×C b) a
    π₂ : {a b : C.Ob} → C.Hom (a ×C b) b
    _h×_ : {a b c : _} → C.Hom a b → C.Hom a c → C.Hom a (b ×C c)

    π₁∘h×≡f : {a b c : _} (f : C.Hom a b) (g : C.Hom a c)
            → π₁ C.∘ (f h× g) ≡! f

    π₂∘h×≡g : {a b c : _} (f : C.Hom a b) (g : C.Hom a c)
            → π₂ C.∘ (f h× g) ≡! g

    h×-unique : {a b c : _} 
                {f : C.Hom a b} {g : C.Hom a c}
                {h : C.Hom a (b ×C c)}
              → f ≡! π₁ C.∘ h
              → g ≡! π₂ C.∘ h
              → h ≡! (f h× g)

  field
    ⊤        : C.Ob
    terminal : {A : _} → isContr (C.Hom A ⊤)

  is-Terminal : Terminal C
  is-Terminal = record { ⊤ = ⊤ ; terminal = terminal }

  ! : {A : _} → C.Hom A ⊤
  ! = Terminal.! is-Terminal

  h×-unique₂ : {a b c d : _} {f g : C.Hom (a ×C b) (c ×C d)}
             → π₁ C.∘ f ≡ π₁ C.∘ g
             → π₂ C.∘ f ≡ π₂ C.∘ g
             → f ≡ g
  h×-unique₂ p q = h×-unique (sym p) (sym q) ∘p sym (h×-unique refl refl)

module _ {o₁ h₁ : _} {C : Category o₁ h₁} (ca : Cartesian C) where
  private
    module C = Category.Category C
  open C
  open Cartesian ca
  h×-∘-dist
    : {x a b c : Category.Ob C}
    → {f : Category.Hom C a b} {g : Category.Hom C a c}{h : Category.Hom C x a}
    → Category._∘_ C (Cartesian._h×_ ca f g) h
    ≡ Cartesian._h×_ ca (Category._∘_ C f h) (Category._∘_ C g h)

  h×-∘-dist {f = f} {g} {h} = h×-unique (sym p) (sym q) where

    p : (π₁ C.∘ (f h× g) C.∘ h) ≡ f C.∘ h
    p = _ ≡⟨ C.assoc₂ _ _ _ ⟩ _ ≡⟨ ap (λ e → e C.∘ h) (π₁∘h×≡f _ _) ⟩ _ ∎

    q : (π₂ C.∘ (f h× g) C.∘ h) ≡ g C.∘ h
    q = _ ≡⟨ C.assoc₂ _ _ _ ⟩ _ ≡⟨ ap (λ e → e C.∘ h) (π₂∘h×≡g _ _) ⟩ _ ∎

  h×≡h×→f≡f : {a b c : Category.Ob C}
            → {f f' : Category.Hom C a b} {g g' : Category.Hom C a c}
            → (Cartesian._h×_ ca f g) ≡! (Cartesian._h×_ ca f' g')
            → f ≡ f'
  h×≡h×→f≡f {f = f} {f'} {g} {g'} p =
        f                  ≡⟨ sym (π₁∘h×≡f f g) ⟩
        π₁ ∘ (f h× g)      ≡⟨ ap (C._∘_ π₁) p ⟩
        π₁ C.∘ (f' h× g')  ≡⟨ π₁∘h×≡f f' g' ⟩
        f'                 ∎

  π₁h×π₂≡id : {a b : Category.Ob C}
            → (π₁ h× π₂) ≡! id {a ×C b}
  π₁h×π₂≡id = sym (h×-unique (sym (C.idr _)) (sym (C.idr _)))

×C-Functor : {o₁ h₁ : _} {C : Category o₁ h₁} → Cartesian C → Functor (C ×Cat C) C
×C-Functor {C = C} Ca =
  record { F₀   = λ { (fst , snd) → fst Ca.×C snd }
         ; F₁   = ×C₁
         ; F-id = ×C₁-id
         ; F-∘  = λ { (f₁ , f₂) (g₁ , g₂) → sym (×C₁-∘ f₁ f₂ g₁ g₂) }
         }
  where
    module Ca = Cartesian Ca
    module C = Category.Category C

    ×C₁ : {a b a' b' : C.Ob}
        → C.Hom a a' × C.Hom b b'
        → C.Hom (a Ca.×C b) (a' Ca.×C b')
    ×C₁ (l , r) = (l C.∘ Ca.π₁) Ca.h× (r C.∘ Ca.π₂)

    ×C₁-id : {a b : C.Ob} → ×C₁ (C.id {a} , C.id {b})  ≡! C.id

    ×C₁-id = (C.id C.∘ Ca.π₁) Ca.h× (C.id C.∘ Ca.π₂)   ≡⟨ ap₂ (λ x y → x Ca.h× y) (C.idl _) (C.idl _) ⟩
             Ca.π₁ Ca.h× Ca.π₂                         ≡⟨ sym (Ca.h×-unique (sym (C.idr _)) (sym (C.idr _))) ⟩
             C.id                                      ∎

    ×C₁-∘ : {a₁ b₁ c₁ a₂ b₂ c₂ : C.Ob}
            (f₁ : C.Hom b₁ c₁) (f₂ : C.Hom b₂ c₂)
            (g₁ : C.Hom a₁ b₁) (g₂ : C.Hom a₂ b₂)
          →  ×C₁ (f₁ , f₂) C.∘ ×C₁ (g₁ , g₂)
          ≡! ×C₁ (f₁ C.∘ g₁ , f₂ C.∘ g₂)
    ×C₁-∘ f₁ f₂ g₁ g₂ =
            ((f₁ C.∘ Ca.π₁) Ca.h× (f₂ C.∘ Ca.π₂))
        C.∘ ((g₁ C.∘ Ca.π₁) Ca.h× (g₂ C.∘ Ca.π₂))
      ≡⟨ h×-∘-dist Ca ⟩
              ((f₁ C.∘ Ca.π₁) C.∘ ((g₁ C.∘ Ca.π₁) Ca.h× (g₂ C.∘ Ca.π₂)))
        Ca.h× ((f₂ C.∘ Ca.π₂) C.∘ ((g₁ C.∘ Ca.π₁) Ca.h× (g₂ C.∘ Ca.π₂)))
      ≡⟨ p ⟩
        ((f₁ C.∘ g₁) C.∘ Ca.π₁) Ca.h× ((f₂ C.∘ g₂) C.∘ Ca.π₂)
      ∎
      where
        p :  ((f₁ C.∘ Ca.π₁) C.∘ ((g₁ C.∘ Ca.π₁) Ca.h× (g₂ C.∘ Ca.π₂)))
               Ca.h×
             ((f₂ C.∘ Ca.π₂) C.∘ ((g₁ C.∘ Ca.π₁) Ca.h× (g₂ C.∘ Ca.π₂)))
          ≡! (((f₁ C.∘ g₁) C.∘ Ca.π₁) Ca.h× ((f₂ C.∘ g₂) C.∘ Ca.π₂))
        p =
          let
            fst = (f₁ C.∘ Ca.π₁) C.∘ ((g₁ C.∘ Ca.π₁) Ca.h× (g₂ C.∘ Ca.π₂))  ≡⟨ C.assoc₁ _ _ _ ⟩
                  f₁ C.∘ (Ca.π₁ C.∘ ((g₁ C.∘ Ca.π₁) Ca.h× (g₂ C.∘ Ca.π₂)))  ≡⟨ ap (C._∘_ f₁) (Ca.π₁∘h×≡f _ _) ⟩
                  f₁ C.∘ (g₁ C.∘ Ca.π₁)                                     ≡⟨ C.assoc₂ _ _ _ ⟩
                  (f₁ C.∘ g₁) C.∘ Ca.π₁                                     ∎

            snd = (f₂ C.∘ Ca.π₂) C.∘ ((g₁ C.∘ Ca.π₁) Ca.h× (g₂ C.∘ Ca.π₂))  ≡⟨ C.assoc₁ _ _ _ ⟩
                  f₂ C.∘ (Ca.π₂ C.∘ ((g₁ C.∘ Ca.π₁) Ca.h× (g₂ C.∘ Ca.π₂)))  ≡⟨ ap (C._∘_ f₂) (Ca.π₂∘h×≡g _ _) ⟩
                  f₂ C.∘ (g₂ C.∘ Ca.π₂)                                     ≡⟨ C.assoc₂ _ _ _ ⟩
                  (f₂ C.∘ g₂) C.∘ Ca.π₂                                     ∎
          in Ca.h×-unique (sym fst ∘p sym (Ca.π₁∘h×≡f _ _))
                          (sym snd ∘p sym (Ca.π₂∘h×≡g _ _))
