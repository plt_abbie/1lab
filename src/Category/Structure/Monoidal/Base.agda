open import 1Lab.Base

open import Category.Functor.Bifunctor
open import Category

import Category.Diagrams

module Category.Structure.Monoidal.Base where

open Functor

record Monoidal {o h : _} (C : Category o h) : Set (o ⊔ h) where
  open Category.Category C
  open Category.Diagrams C

  field
    -⊗-  : Functor (C ×Cat C) C
    Unit : Ob
  
  private
    module Bif-⊗ = Bifunctor -⊗-
  
  infixr 40 _⊗₀_ _⊗₁_

  _⊗₀_ : Ob → Ob → Ob
  X ⊗₀ Y = F₀ -⊗- (X , Y)

  _⊗₁_ : {a b x y : Ob} (f : Hom a b) (g : Hom x y)
       → Hom (a ⊗₀ x) (b ⊗₀ y)
  f ⊗₁ g = F₁ -⊗- (f , g)

  _⊗- : Ob → Functor C C
  X ⊗- = Bif-⊗.Right X

  -⊗_ : Ob → Functor C C
  -⊗ X = Bif-⊗.Left X

  field
    left-unitor  : (Unit ⊗-) ≅ Id
    right-unitor : (-⊗ Unit) ≅ Id

    associator : {X Y Z : _}
               → X ⊗₀ (Y ⊗₀ Z) ≈ (X ⊗₀ Y) ⊗₀ Z

  module left-unitor = _≅_ left-unitor
  module right-unitor = _≅_ right-unitor

  associator→ : {X Y Z : _} → Hom (X ⊗₀ (Y ⊗₀ Z)) ((X ⊗₀ Y) ⊗₀ Z)
  associator→ {X} {Y} {Z} = Σ.fst (associator {X} {Y} {Z})

  associator← : {X Y Z : _} → Hom ((X ⊗₀ Y) ⊗₀ Z) (X ⊗₀ (Y ⊗₀ Z))
  associator← {X} {Y} {Z} = is-Iso.g (Σ.snd (associator {X} {Y} {Z}))

  field
    associator-natural₁ : {A B C X Y Z : _} {f : Hom A X} {g : Hom B Y} {h : Hom C Z}
                        →  associator→ ∘ (f ⊗₁ (g ⊗₁ h))
                        ≡! ((f ⊗₁ g) ⊗₁ h) ∘ associator→

    associator-natural₂ : {A B C X Y Z : _} {f : Hom A X} {g : Hom B Y} {h : Hom C Z}
                        →  associator← ∘ ((f ⊗₁ g) ⊗₁ h)
                        ≡! (f ⊗₁ (g ⊗₁ h)) ∘ associator←

  field
    triangle : {X Y : Ob}
             →  _=>_.η right-unitor.to X ⊗₁ id
             ≡! (id ⊗₁ _=>_.η left-unitor.to Y) ∘ associator←

  field
    pentagon : {W X Y Z : Ob}
             → _≡!_ {A = Hom (((W ⊗₀ X) ⊗₀ Y) ⊗₀ Z) (W ⊗₀ (X ⊗₀ (Y ⊗₀ Z)))}
                    (associator← ∘ associator←)
                    ((id ⊗₁ associator←) ∘ associator← ∘ (associator← ⊗₁ id))
