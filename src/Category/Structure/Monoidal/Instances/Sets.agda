open import 1Lab.Base

open import Category.Instances.Sets.Cartesian
open import Category.Structure.Monoidal.Base
open import Category.Structure.Cartesian
open import Category.Diagrams
open import Category

open Monoidal

module Category.Structure.Monoidal.Instances.Sets where

Sets-monoidal : {ℓ : _} → Monoidal (Sets ℓ)
-⊗- Sets-monoidal = ×C-Functor Set-products
Unit Sets-monoidal = Top
left-unitor Sets-monoidal =
  record { to      = NT (λ { x (fst , snd) → snd }) λ x y f → refl
         ; from    = NT (λ { x x₁ → tt , x₁ }) λ x y f → refl
         ; to-from = refl
         ; from-to = refl
         }
right-unitor Sets-monoidal =
  record { to      = NT (λ { x (fst , snd) → fst }) λ x y f → refl
         ; from    = NT (λ x x₁ → x₁ , tt) λ x y f → refl
         ; to-from = refl
         ; from-to = refl
         }
associator Sets-monoidal =
    (λ { (fst , fst₁ , snd) → (fst , fst₁) , snd })
  , iso (λ { ((fst , snd₁) , snd) → fst , snd₁ , snd })
        refl
        refl
associator-natural₁ Sets-monoidal = refl
associator-natural₂ Sets-monoidal = refl
triangle Sets-monoidal = refl
pentagon Sets-monoidal = refl
