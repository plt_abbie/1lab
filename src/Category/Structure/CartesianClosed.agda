open import 1Lab.Base

open import Category.Structure.Cartesian
open import Category.Functor.Bifunctor
open import Category.Functor.Adjoints
open import Category

import Category.Associativity

module Category.Structure.CartesianClosed where

record CartesianClosed {o₁ h₁ : _} {C : Category o₁ h₁} (ca : Cartesian C) : Set (lsuc o₁ ⊔ lsuc h₁) where
  private
    module C = Category.Category C
    module Ca = Cartesian ca

    module P = Bifunctor (×C-Functor ca)

  field
    [_,_] : C.Ob → C.Ob → C.Ob

  field
    eval : {a b : C.Ob} → C.Hom ([ a , b ] Ca.×C a) b

  uncurry : {a b c : C.Ob} → C.Hom a [ b , c ] → C.Hom (a Ca.×C b) c
  uncurry hom = eval C.∘ P.first hom

  field
    λf   : {a b c : C.Ob} → C.Hom (a Ca.×C b) c → C.Hom a [ b , c ]
    β    : {a b c : C.Ob} (g : C.Hom (a Ca.×C b) c)
         → uncurry (λf g) ≡! g
    λ-unique : {a b c : C.Ob} {g : C.Hom (a Ca.×C b) c}
               (h : C.Hom a [ b , c ])
             → uncurry h ≡! g
             → h ≡ λf g

  curry-uncurry : {a b c : C.Ob} (c : C.Hom a [ b , c ])
                → λf (uncurry c) ≡ c
  curry-uncurry c = sym (λ-unique _ refl)

  λ-η : {a b : C.Ob} → λf (eval {a} {b}) ≡! C.id
  λ-η = sym (λ-unique _ p) where
    p = eval C.∘ P.first C.id ≡⟨ ap (C._∘_ eval) P.F-id ⟩
        eval C.∘ C.id         ≡⟨ C.idr _ ⟩
        eval                  ∎

  λ-unique₂ : {c a b : C.Ob} {f g : C.Hom c _}
            →  eval {a} {b} C.∘ P.first f
            ≡! eval {a} {b} C.∘ P.first g
            → f ≡! g
  λ-unique₂ eq = λ-unique _ eq ∘p sym (λ-unique _ refl)

[-,-] : {o₁ h₁ : _} {C : Category o₁ h₁} {Ca : Cartesian C}
      → CartesianClosed Ca
      → Functor (C ^op ×Cat C) C
[-,-] {C = C} {Ca} Cl = r where
  open Functor
  open Category.Category C
  open Cartesian Ca
  open CartesianClosed Cl
  open Category.Associativity C

  module P = Bifunctor (×C-Functor Ca)

  r : Functor (C ^op ×Cat C) C
  F₀ r (A , B) = [ A , B ]
  F₁ r (f , g) = λf (g ∘ eval ∘ P.second f)
  F-id r =
      λf (id ∘ eval ∘ P.F₁ (id , id)) ≡⟨ ap (λ e → λf (id ∘ eval ∘ e)) P.F-id ⟩
      λf (id ∘ eval ∘ id)             ≡⟨ ap λf (idl _) ⟩
      λf (eval ∘ id)                  ≡⟨ ap λf (idr _) ⟩
      λf eval                         ≡⟨ λ-η ⟩
      id                              ∎

  F-∘ r {x} {y} {z} (f , g) (h , i) = λ-unique₂ helper where
    inside = λf ((g ∘ i) ∘ eval ∘ P.second (h ∘ f))
    g-ev-f = g ∘ eval ∘ P.second f
    i-ev-h = i ∘ eval ∘ P.second h

    helper =
      eval ∘ P.first inside                                           ≡⟨ β _ ⟩
      (g ∘ i) ∘ eval ∘ P.second (h ∘ f)                               ≡⟨ ap (λ e → (g ∘ i) ∘ eval ∘ e) (P.second∘second h f) ⟩
      (g ∘ i) ∘ eval ∘ P.second h ∘ P.second f                        ≡⟨ sym first2-out-of-5 ∘p second3-out-of-5 ⟩
      g ∘ (i ∘ eval ∘ P.second h) ∘ P.second f                        ≡⟨ ap (λ e → g ∘ e ∘ P.second f) (sym (β _)) ⟩
      g ∘ (eval ∘ P.first (λf (i ∘ eval ∘ P.second h))) ∘ P.second f  ≡⟨ sym second2-out-of-4 ⟩
      g ∘ eval ∘ P.first (λf (i ∘ eval ∘ P.second h)) ∘ P.second f    ≡⟨ ap (λ e → g ∘ eval ∘ e) P.first∘second ⟩
      g ∘ eval ∘ P.second f ∘ P.first (λf i-ev-h)                     ≡⟨ first3-out-of-4 ⟩
      (g ∘ eval ∘ P.second f) ∘ P.first (λf i-ev-h)                   ≡⟨ ap (λ e → e ∘ P.first (λf (i ∘ eval ∘ P.second h))) (sym (β _)) ⟩
      (eval ∘ P.first (λf g-ev-f)) ∘ P.first (λf i-ev-h)              ≡⟨ left→right ⟩
      eval ∘ P.first (λf g-ev-f) ∘ P.first (λf i-ev-h)                ≡⟨ ap (λ e → eval ∘ e) (sym (P.first∘first _ _)) ⟩
      eval ∘ P.first (λf g-ev-f ∘ (λf i-ev-h))                        ∎


module _ {o₁ h₁ : _} {C : Category o₁ h₁} {Ca : Cartesian C} (Cl : CartesianClosed Ca) (A : Category.Ob C) where
  private
    module Prod = Bifunctor (×C-Functor Ca)
    module Arr = Bifunctor ([-,-] Cl)
    module C = Category.Category C
    module Ca = Cartesian Ca
    module Cl = CartesianClosed Cl
  
  open C hiding (Hom)
  open Ca
  open Cl
  
  open Category.Associativity C

  -×A : Functor C C
  -×A = Prod.Left A
  
  [A,-] : Functor C C
  [A,-] = Arr.Right A

  open _⊣_
  Tensor⊣Hom : -×A ⊣ [A,-]
  unit Tensor⊣Hom = NT (λ x → λf C.id) λ x y f →
    let
      p =
        eval ∘ Prod.first (λf id ∘ f)                                          ≡⟨ ap (_∘_ eval) (Prod.first∘first _ _) ∘p right→left ⟩
        (eval ∘ Prod.first (λf id)) ∘ Prod.first f                             ≡⟨ ap (λ e → e ∘ Prod.first f) (β _) ∘p C.idl _ ⟩
        Prod.first f                                                           ≡⟨ sym (C.idr _) ∘p ap (λ e → Prod.first f ∘ e) (sym (β _)) ∘p right→left ⟩
        (Prod.first f ∘ eval) ∘ Prod.first (λf id)                             ≡⟨ ap (λ e → (_ ∘ e) ∘ Prod.first (λf id)) (sym (C.idr _) ∘p ap (_∘_ eval) (sym Prod.first-id)) ⟩
        (Prod.first f ∘ eval ∘ Prod.first id) ∘ Prod.first (λf id)             ≡⟨ ap (λ e → e ∘ Prod.first (λf id)) (sym (β _)) ∘p left→right ⟩
        eval ∘ Prod.first (λf (Prod.first f ∘ eval ∘ _)) ∘ Prod.first (λf id)  ≡⟨ ap (_∘_ eval) (sym (Prod.first∘first _ _)) ⟩
        eval ∘ Prod.first (λf (Prod.first f ∘ eval ∘ _) ∘ λf id)               ≡⟨⟩
        _                                                                      ∎
    in λ-unique₂ p
    
  counit Tensor⊣Hom = NT (λ x → Cl.eval) λ x y f → 
      _ ≡⟨ Cl.β _ ⟩
      _ ≡⟨ ap (λ e → f C.∘ Cl.eval C.∘ e) Prod.second-id ⟩
      _ ≡⟨ right→left ⟩
      _ ≡⟨ C.idr _ ⟩
      _ ∎
  zig Tensor⊣Hom = β _
  zag Tensor⊣Hom =
    let
      p = eval ∘ Prod.first (λf (eval ∘ eval ∘ _) ∘ λf id)                 ≡⟨ ap (_∘_ eval) (Prod.first∘first _ _) ∘p right→left ⟩
          (eval ∘ Prod.first (λf (eval ∘ eval ∘ _))) ∘ Prod.first (λf id)  ≡⟨ ap (λ e → e ∘ Prod.first (λf id)) (β _) ⟩
          (eval ∘ (eval ∘ Prod.second id)) ∘ Prod.first (λf id)            ≡⟨ ap (λ e → (eval ∘ e) ∘ _) (ap (_∘_ eval) Prod.second-id ∘p C.idr _) ∘p left→right ⟩
          eval ∘ eval ∘ Prod.first (λf id)                                 ≡⟨ ap (_∘_ eval) (β _) ⟩
          eval ∘ _                                                         ≡⟨ ap (_∘_ eval) (sym Prod.first-id) ⟩
          _                                                                ∎
    in λ-unique₂ p
