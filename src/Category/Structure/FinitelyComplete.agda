open import 1Lab.Base

open import Category

import Category.Diagrams

module Category.Structure.FinitelyComplete where

record FinitelyComplete {o₁ h₁} (C : Category o₁ h₁) : Set (o₁ ⊔ h₁) where
  private
    module C = Category.Category C
  open Category.Diagrams C

  field
    Pullback-of : {A B C : C.Ob} (f : C.Hom A C) (g : C.Hom B C) → C.Ob
    pb-π₁ : {A B C : C.Ob} {f : C.Hom A C} {g : C.Hom B C} → C.Hom (Pullback-of f g) A
    pb-π₂ : {A B C : C.Ob} {f : C.Hom A C} {g : C.Hom B C} → C.Hom (Pullback-of f g) B
    is-pullback : {A B C : C.Ob} (f : C.Hom A C) (g : C.Hom B C)
                → Pullback (pb-π₁ {f = f} {g = g}) pb-π₂ f g

    Equaliser-of : {A B : C.Ob} (f g : C.Hom A B) → C.Ob
    equalise     : {A B : C.Ob} (f g : C.Hom A B) → C.Hom (Equaliser-of f g) A
    is-equaliser : {A B : C.Ob} {f g : C.Hom A B} → Equaliser f g (equalise f g)
