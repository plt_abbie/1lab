```
open import 1Lab.Base

open import Category.Functor.Properties
open import Category.Functor.Adjoints
open import Category

open Functor
open _=>_

module Category.Constructions.Monad {o h : _} {C : Category o h} where

private
  module C = Category.Category C
```

# Monads

A `Monad`{.Agda} in a category is an endofunctor `M`{.Agda} together
with a morphism $\mathrm{unit} : X \to M(X)$ and a morphism
$\mathrm{mult} : M(M(X)) \to M(X)$ (both of these are natural in $X$:
`unit`{.Agda} and `mult`{.Agda}).

```
record Monad : Set (o ⊔ h) where
  field
    M    : Functor C C
    unit : Id => M
    mult : (M F∘ M) => M
  
  module unit = _=>_ unit
  module mult = _=>_ mult
  
  M₀ = F₀ M
  M₁ = F₁ M
  M-id = F-id M
  M-∘ = F-∘ M
```

Furthermore, these natural transformations satisfy operations analogous
to those of a monoid:

```
  field
    left-ident  : {x : _} → mult.η x C.∘ M₁ (unit.η x) ≡! C.id
    right-ident : {x : _} → mult.η x C.∘ unit.η (M₀ x) ≡! C.id
  
  field
    assoc : {x : _} → mult.η x C.∘ M₁ (mult.η x) ≡! mult.η x C.∘ mult.η (M₀ x)
```

# Algebras for a monad

An `M -Algebra`{.Agda ident="_-Algebra"} for a monad $M$ is an object
$A$ together with a morphism $\nu : M(A) \to A$ such that $\nu$ is an _$M$-action_.

```agda
record _-Algebra (M : Monad) : Set (o ⊔ h) where
  open Monad M

  field
    object : C.Ob
    ν : C.Hom (M₀ object) object
```

A monad action is the categorification of a monoid action:

```
    ν-mult : ν C.∘ M₁ ν ≡! ν C.∘ mult.η object
    ν-unit : ν C.∘ unit.η object ≡! C.id
```

# Eilenberg-Moore Category

There is a natural definition of _`M-Algebra homomorphism`{.Agda
ident="_-Algebra_=>_"}_: a morphism of the underlying objects that
commutes with the action.

```agda
record _-Algebra_=>_ (M : Monad) (X Y : M -Algebra) : Set (o ⊔ h) where
  private
    module X = _-Algebra X
    module Y = _-Algebra Y

  open Monad M

  field
    morphism : C.Hom X.object Y.object
    commutes : morphism C.∘ X.ν ≡! Y.ν C.∘ M₁ morphism

open _-Algebra_=>_
```

For which there is a natural characterisation of equality, `Algebra=>≡`{.Agda}.

```
Algebra=>≡ : {M : Monad} {X Y : M -Algebra} {F G : M -Algebra X => Y}
           → morphism F ≡ morphism G
           → F ≡ G
Algebra=>≡ {F = F} {G} refl rewrite →rewrite (UIP (commutes F) (commutes G)) = refl
```

These naturally assemble into a category, the `Eilenberg-Moore`{.Agda}
category of $M$, denoted $C^\mathbf{M}$.

```agda
module _ (M : Monad) where
  private
    module M = Monad M
  open M hiding (M)
  open Category.Category
  open _-Algebra

  Eilenberg-Moore : Category _ _
  Ob Eilenberg-Moore = M -Algebra
  Hom Eilenberg-Moore X Y = M -Algebra X => Y

  id Eilenberg-Moore {x} =
    record
      { morphism = C.id
      ; commutes = C.id C.∘ ν x     ≡⟨ C.idl _ ⟩
                   ν x              ≡⟨ sym (C.idr _) ⟩
                   ν x C.∘ C.id     ≡⟨ ap (C._∘_ _) (sym M-id) ⟩
                   ν x C.∘ M₁ C.id  ∎
      }

  _∘_ Eilenberg-Moore {x} {y} {z} F G =
    record 
      { morphism = morphism F C.∘ morphism G
      ; commutes = (morphism F C.∘ morphism G) C.∘ ν x            ≡⟨ C.assoc₁ _ _ _ ⟩
                   morphism F C.∘ morphism G C.∘ ν x              ≡⟨ ap (C._∘_ _) (commutes G) ⟩
                   morphism F C.∘ ν y C.∘ M₁ (morphism G)         ≡⟨ C.assoc₂ _ _ _ ⟩
                   (morphism F C.∘ ν y) C.∘ M₁ (morphism G)       ≡⟨ ap (λ e → e C.∘ _) (commutes F) ⟩
                   (ν z C.∘ M₁ (morphism F)) C.∘ M₁ (morphism G)  ≡⟨ C.assoc₁ _ _ _ ⟩
                   ν z C.∘ M₁ (morphism F) C.∘ M₁ (morphism G)    ≡⟨ ap (C._∘_ _) (sym (M-∘ _ _)) ⟩
                   ν z C.∘ M₁ (morphism F C.∘ morphism G)         ∎
      }

  idr Eilenberg-Moore f = Algebra=>≡ (C.idr (morphism f))
  idl Eilenberg-Moore f = Algebra=>≡ (C.idl (morphism f))
  assoc₁ Eilenberg-Moore f g h = Algebra=>≡ (C.assoc₁ _ _ _)
  assoc₂ Eilenberg-Moore f g h = Algebra=>≡ (C.assoc₂ _ _ _)
```

There is an evident functor from $C^\mathbf{M}$ back into C.

```
  Forget : Functor Eilenberg-Moore C
  F₀ Forget = _-Algebra.object
  F₁ Forget = _-Algebra_=>_.morphism
  F-id Forget = refl
  F-∘ Forget f g = refl
```

This functor is faithful exactly by our characterisation of equality of algebra homomorphisms.

```
  Forget-faithful : Faithful Forget
  Forget-faithful proof = Algebra=>≡ proof
```

## Free Algebras

For every object $A$, there is a _free_ $M$-algebra, where the
underlying object is $M(A)$ and the action is multiplication.

```
  Free : Functor C Eilenberg-Moore
  F₀ Free A =
    record
      { object = M₀ A
      ; ν      = mult.η A
      ; ν-mult = assoc
      ; ν-unit = right-ident
      }
```

The functorial action of the free $M$-algebra construction is given by
the action of $M$, which commutes with the action since $M$'s
multiplication is a natural transformation.

```
  F₁ Free f =
    record
      { morphism = M₁ f
      ; commutes = sym (mult.is-natural _ _ _)
      }
  F-id Free = Algebra=>≡ M-id
  F-∘ Free f g = Algebra=>≡ (M-∘ f g)
```

This is a free construction in the precise sense of the word: it's left
adjoint to a forgetful functor.

```
  open _⊣_

  Free⊣Forget : Free ⊣ Forget
  unit Free⊣Forget = NT (λ x → M.unit.η x) λ x y f → M.unit.is-natural _ _ _
  η (counit Free⊣Forget) x =
    record { morphism = ν x
           ; commutes = sym (ν-mult x)
           }
  is-natural (counit Free⊣Forget) x y f = Algebra=>≡ (sym (commutes f)) 
  zig Free⊣Forget = Algebra=>≡ left-ident
  zag Free⊣Forget {x} = ν-unit x
```
