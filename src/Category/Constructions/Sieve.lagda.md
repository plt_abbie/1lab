```agda
open import 1Lab.HProp
open import 1Lab.Coeq
open import 1Lab.Base

open import Category

module Category.Constructions.Sieve {o₁ h₁ ℓ : _}
                                    (C : Category o₁ h₁)
                                    (c : Category.Ob C)
       where
```

Given a category $C$, a _sieve_ on an object $c$ is a subset of the maps
$a \to c$ closed under composition: If $f \in S$, then $(f \circ g) \in
S$. The data of a sieve on $c$ corresponds to the data of a subobject of
$よ(c)$ in $\operatorname{PSh}(C)$.

Here, the subset is represented as the function `arrows`{.agda}, which, given
an arrow $f : a \to c$ (and its domain), yields a proposition
representing inclusion in the subset.

```agda

private
  module C = Category.Category C

record Sieve : Set (o₁ ⊔ h₁ ⊔ ℓ) where
  field
    arrows : (y : C.Ob) → C.Hom y c → hProp ℓ
    closed : {y z : _} {f : _} (g : C.Hom y z)
            → f ∈ arrows _
            → (f C.∘ g) ∈ arrows _
open Sieve
```

The `maximal`{.agda} sieve on an object is the collection of _all_ maps $a \to
c$. This is clearly closed under composition, since all the maps are in
that collection.

```agda
maximal : Sieve
arrows maximal = λ y x → P⊤
closed maximal = λ g x → tt
```

A characterisation of equality in sieves: $S$ is equal to $S'$ iff they
assign the same propositions to every arrow $f : a \to c$.

```agda
Sieve≡ : {S S' : Sieve} → ((o : _) (f : _) → arrows S o f ≡! arrows S' o f) → S ≡! S'
```

<details>
<summary> Proof of `Sieve≡`{.agda}. </summary>
```agda
Sieve≡ {record {arrows = a;closed = y}} {record {arrows = b;closed = z}} p =
  go (fun-ext λ o → fun-ext λ f → p o f)
  where
    go : a ≡! b → record { arrows = a ; closed = y } ≡! record { arrows = b ; closed = z }
    go refl = go' lemma where
      T : Set _
      T = _≡!_ {A = {y₁ z₁ : C.Ob} {f : C.Hom z₁ c} (g : C.Hom y₁ z₁)
                  → f ∈ a _ → (f C.∘ g) ∈ a _}
                y z

      go' : T 
        →  record { arrows = a ; closed = y }
        ≡! record { arrows = a ; closed = z }
      go' refl = refl

      lemma : T
      lemma = fun-ext-invis λ _ →
              fun-ext-invis λ _ →
              fun-ext-invis λ _ →
              fun-ext λ g →
              fun-ext λ innit →
              hProp.prop (a _ _) _ _
```
</summary>
