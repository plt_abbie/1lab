open import 1Lab.Base

open import Category.Diagrams
open import Category.Equality
open import Category

import Category.Associativity

module Category.Groupoid where

record Groupoid (o₁ h₁ : _) : Set (lsuc (o₁ ⊔ h₁)) where
  field
    C : Category o₁ h₁

  open Category.Category C public
  open Category.Associativity C
    hiding (assoc₁ ; assoc₂)

  infix 60 _¯¹

  field
    _¯¹ : {x y : _} → Hom x y → Hom y x

  field
    linv : {x y : _} (f : Hom x y) → f ∘ f ¯¹ ≡! id
    rinv : {x y : _} (f : Hom x y) → f ¯¹ ∘ f ≡! id
  
  id¯¹≡id : {x : _} → id ¯¹ ≡! id {x}
  id¯¹≡id =
    id ¯¹      ≡⟨ sym (idr _) ⟩
    id ¯¹ ∘ id ≡⟨ rinv id ⟩
    id         ∎
  
  ¯¹-unique : {x y : _}
            → (f : Hom x y)
            → (g : Hom y x)
            → f ∘ g ≡! id
            → g ∘ f ≡! id
            → g ≡ f ¯¹
  ¯¹-unique f g p q =
    g              ≡⟨ sym (idr _) ⟩
    g ∘ id         ≡⟨ ap (_∘_ g) (sym (linv f)) ⟩
    g ∘ (f ∘ f ¯¹) ≡⟨ assoc₂ _ _ _ ⟩
    (g ∘ f) ∘ f ¯¹ ≡⟨ ap (λ e → e ∘ f ¯¹) q ⟩
    id ∘ f ¯¹      ≡⟨ idl _ ⟩
    f ¯¹ ∎

  f∘g¯¹ : {x y z : _} (f : Hom y z) (g : Hom x y)
        → (f ∘ g) ¯¹ ≡! g ¯¹ ∘ f ¯¹
  f∘g¯¹ f g = sym (¯¹-unique (f ∘ g) _ p q)
    where
      p = (f ∘ g) ∘ g ¯¹ ∘ f ¯¹ ≡⟨ sym first2-out-of-4 ∘p second2-out-of-4 ⟩
          f ∘ (g ∘ g ¯¹) ∘ f ¯¹ ≡⟨ ap (λ e → _ ∘ e ∘ _ ¯¹) (linv g) ⟩
          f ∘ id ∘ f ¯¹         ≡⟨ ap (_∘_ f) (idl _) ⟩
          f ∘ f ¯¹              ≡⟨ (linv f) ⟩
          id                    ∎
      q : _
      q = (g ¯¹ ∘ f ¯¹) ∘ f ∘ g ≡⟨ sym first2-out-of-4 ∘p second2-out-of-4 ⟩
          g ¯¹ ∘ (f ¯¹ ∘ f) ∘ g ≡⟨ ap (λ e → g ¯¹ ∘ e ∘ _) (rinv f) ⟩
          g ¯¹ ∘ id ∘ g         ≡⟨ ap (_∘_ (g ¯¹)) (idl _) ⟩
          g ¯¹ ∘ g              ≡⟨ (rinv g) ⟩
          id                    ∎

module _ {o₁ h₁ : _} (C : Category o₁ h₁) where
  private
    module C = Category.Category C

  open Groupoid hiding (C)

  private
    isIso-∘ : {x y z : _}
            → Σ (C.Hom y z) (is-Iso C)
            → Σ (C.Hom x y) (is-Iso C)
            → Σ (C.Hom x z) (is-Iso C)
    isIso-∘ (f , iso f¯¹ linv₁ rinv₁) (g , iso g¯¹ linv₂ rinv₂) = f C.∘ g , inv where
      inv : is-Iso C (f C.∘ g)
      inv = iso (g¯¹ C.∘ f¯¹) eq1 eq2 where
        eq1 =
            (f C.∘ g) C.∘ g¯¹ C.∘ f¯¹   ≡⟨ C.assoc₁ _ _ _ ⟩
            f C.∘ (g C.∘ g¯¹ C.∘ f¯¹)   ≡⟨ ap (C._∘_ f) (C.assoc₂ _ _ _) ⟩
            f C.∘ ((g C.∘ g¯¹) C.∘ f¯¹) ≡⟨ ap (λ e → f C.∘ (e C.∘ f¯¹)) linv₂ ⟩
            f C.∘ (C.id C.∘ f¯¹)        ≡⟨ ap (C._∘_ f) (C.idl _) ⟩
            f C.∘ f¯¹                   ≡⟨ linv₁ ⟩
            C.id                        ∎

        eq2 =
            (g¯¹ C.∘ f¯¹) C.∘ f C.∘ g ≡⟨ C.assoc₁ _ _ _ ⟩
            g¯¹ C.∘ f¯¹ C.∘ f C.∘ g   ≡⟨ ap (C._∘_ g¯¹) (C.assoc₂ _ _ _) ⟩
            g¯¹ C.∘ (f¯¹ C.∘ f) C.∘ g ≡⟨ ap (λ e → g¯¹ C.∘ (e C.∘ g)) rinv₁ ⟩
            g¯¹ C.∘ C.id C.∘ g        ≡⟨ ap (C._∘_ g¯¹) (C.idl _) ⟩
            g¯¹ C.∘ g                 ≡⟨ rinv₂ ⟩
            C.id                      ∎

  Core[_] : Groupoid o₁ h₁
  Groupoid.C Core[_] =
    record
    { Ob     = C.Ob
    ; Hom    = λ x y → Σ (C.Hom x y) (is-Iso C)
    ; id     = C.id , iso C.id (C.idl _) (C.idl _)
    ; _∘_    = isIso-∘
    ; idr    = λ f → Iso≡ (C.idr _) (C.idl _)
    ; idl    = λ f → Iso≡ (C.idl _) (C.idr _)
    ; assoc₁ = λ f g h → Iso≡ (C.assoc₁ _ _ _) (C.assoc₂ _ _ _)
    ; assoc₂ = λ f g h → Iso≡ (C.assoc₂ _ _ _) (C.assoc₁ _ _ _)
    }
  (Core[_] ¯¹) (f , iso g linv₁ rinv₁) = g , iso f rinv₁ linv₁
  linv Core[_] (f , iso g linv₁ rinv₁) = Iso≡ linv₁ linv₁
  rinv Core[_] (f , iso g linv₁ rinv₁) = Iso≡ rinv₁ rinv₁
