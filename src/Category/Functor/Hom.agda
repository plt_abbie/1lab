open import 1Lab.Coeq
open import 1Lab.Base

open import Category.Functor.Bifunctor
open import Category

import Category.Associativity

module Category.Functor.Hom where

open Bifunctor
open Functor

module _ {o₁ h₁ : _} {C : Category o₁ h₁} where
  private
    module C = Category.Category C

  open Category.Associativity C
  Hom[-,-] : Functor ((C ^op) ×Cat C) (Sets _)
  F₀ Hom[-,-] (fst , snd) = C.Hom fst snd
  F₁ Hom[-,-] (f , h) g = h C.∘ g C.∘ f
  F-id Hom[-,-] {x} = fun-ext λ g → C.idl _ ∘p C.idr _
  F-∘ Hom[-,-] (f , h) (i , j) = fun-ext λ p →
      (h C.∘ j) C.∘ p C.∘ i C.∘ f  ≡⟨ sym (first2-out-of-5 {f = h} {g = j} {h = p} {i = i} {j = f}) ⟩
      h C.∘ j C.∘ p C.∘ i C.∘ f    ≡⟨ second3-out-of-5 {f = h} {g = j} {h = p} {i = i} {j = f} ⟩
      h C.∘ (j C.∘ p C.∘ i) C.∘ f  ∎
  
  Hom[_,-] : C.Ob → Functor C (Sets _)
  Hom[ X ,-] = Right Hom[-,-] X

  module _ {o₂ h₂ : _} {D : Category o₂ h₂} where
    private
      module D = Category.Category D

    Hom[_-,-] : Functor D C → Functor (D ^op ×Cat C) (Sets _)
    F₀ Hom[ F -,-] (A , B) = C.Hom (F₀ F A) B
    F₁ Hom[ F -,-] (f , h) g = h C.∘ g C.∘ F₁ F f
    F-id Hom[ F -,-] = fun-ext λ g → C.idl _ ∘p (ap (λ e → g C.∘ e) (F-id F) ∘p C.idr _)
    F-∘ Hom[ F -,-] (f , h) (i , j) = fun-ext λ p →
        (h C.∘ j) C.∘ p C.∘ F₁ F (i D.∘ f)     ≡⟨ ap (λ e → _ C.∘ _ C.∘ e) (F-∘ F i f) ⟩
        (h C.∘ j) C.∘ p C.∘ F₁ F i C.∘ F₁ F f  ≡⟨ sym (first2-out-of-5 {f = h} {g = j} {h = p} {i = F₁ F i} {j = F₁ F f}) ⟩
        h C.∘ j C.∘ p C.∘ F₁ F i C.∘ F₁ F f    ≡⟨ second3-out-of-5 {f = h} {g = j} {h = p} {i = F₁ F i} {j = F₁ F f} ⟩
        h C.∘ (j C.∘ p C.∘ F₁ F i) C.∘ F₁ F f  ∎

    Hom[-,_-] : Functor D C → Functor (C ^op ×Cat D) (Sets _)
    F₀ Hom[-, G -] (A , B) = C.Hom A (F₀ G B)
    F₁ Hom[-, G -] (f , h) g = F₁ G h C.∘ g C.∘ f
    F-id Hom[-, G -] = fun-ext λ g → ap (λ e → e C.∘ _ C.∘ _) (F-id G) ∘p (C.idl _ ∘p C.idr _)
    F-∘ Hom[-, G -] (f , h) (i , j) = fun-ext λ p →
        F₁ G (h D.∘ j) C.∘ p C.∘ i C.∘ f       ≡⟨ ap (λ e → e C.∘ _) (F-∘ G h j) ⟩
        (F₁ G h C.∘ F₁ G j) C.∘ p C.∘ i C.∘ f  ≡⟨ sym (first2-out-of-5 {f = F₁ G h} {g = F₁ G j} {h = p} {i = i} {j = f}) ⟩
        F₁ G h C.∘ F₁ G j C.∘ p C.∘ i C.∘ f    ≡⟨ second3-out-of-5 {f = _} {g = _} {h = p} {i = i} {j = f} ⟩
        F₁ G h C.∘ (F₁ G j C.∘ p C.∘ i) C.∘ f  ∎
