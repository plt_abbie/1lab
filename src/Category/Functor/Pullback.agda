 
 
 
 
open import 1Lab.Base

open import Category.Structure.FinitelyComplete
open import Category.Slices
open import Category

import Category.Diagrams

open Functor

module Category.Functor.Pullback {o₁ h₁ : _} {C : Category o₁ h₁} (fl : FinitelyComplete C) where

private
  module C = Category.Category C
  module Fl = FinitelyComplete fl

open Category.Diagrams C

change-of-base : {X Y : C.Ob}
                → C.Hom X Y
                → Functor (Slice C Y) (Slice C X)
F₀ (change-of-base f) (K , K→Y) = Fl.Pullback-of f K→Y , Fl.pb-π₁
F₁ (change-of-base f) {K , K→Y} {K' , K'→Y} (K→K' , fibers) =
  map , Pullback.p₁∘limiting≡p₁' (Fl.is-pullback f K'→Y) (sym square)
  where
    square =
        K'→Y C.∘ K→K' C.∘ Fl.pb-π₂   ≡⟨ C.assoc₂ _ _ _ ⟩
        (K'→Y C.∘ K→K') C.∘ Fl.pb-π₂ ≡⟨ ap (λ e → e C.∘ _) fibers ⟩
        K→Y C.∘ Fl.pb-π₂             ≡⟨ sym (Pullback.commutes (Fl.is-pullback f K→Y)) ⟩
        f C.∘ Fl.pb-π₁               ∎

    map : C.Hom (Fl.Pullback-of f K→Y) (Fl.Pullback-of f K'→Y)
    map = Pullback.limiting (Fl.is-pullback f K'→Y) {p₁' = Fl.pb-π₁} {p₂' = K→K' C.∘ Fl.pb-π₂} (sym square)

F-id (change-of-base f) {X , X→Y} = Σ-Path! (sym p') (UIP _ _)
  where
    square = X→Y C.∘ C.id C.∘ Fl.pb-π₂ ≡⟨ ap (λ e → X→Y C.∘ e) (C.idl _) ⟩
              X→Y C.∘ Fl.pb-π₂         ≡⟨ sym (Pullback.commutes (Fl.is-pullback f X→Y)) ⟩
              f C.∘ Fl.pb-π₁           ∎

    p = Pullback.unique (Fl.is-pullback f X→Y) (sym square) (C.idr _) (C.idr _ ∘p sym (C.idl _))
    p' = subst (λ e → C.id ≡! Pullback.limiting (Fl.is-pullback f X→Y) e) (UIP _ _) p

F-∘ (change-of-base f) {A , A→Y} {B , B→Y} {C , C→Y} (g , g-fib) (h , h-fib) = Σ-Path! (sym goal) (UIP _ _)
  where
    that-path = _
    this-path = _
    composite-path = _

    p = C→Y C.∘ (g C.∘ h) C.∘ Fl.pb-π₂    ≡⟨ ap (C._∘_ C→Y) (C.assoc₁ _ _ _) ⟩
        C→Y C.∘ g C.∘ h C.∘ Fl.pb-π₂      ≡⟨ C.assoc₂ _ _ _ ⟩
        (C→Y C.∘ g) C.∘ (h C.∘ Fl.pb-π₂)  ≡⟨ ap (λ e → e C.∘ (h C.∘ Fl.pb-π₂)) g-fib ⟩
        B→Y C.∘ (h C.∘ Fl.pb-π₂)          ≡⟨ C.assoc₂ _ _ _ ⟩
        (B→Y C.∘ h) C.∘ Fl.pb-π₂          ≡⟨ ap (λ e → e C.∘ Fl.pb-π₂) h-fib ⟩
        A→Y C.∘ Fl.pb-π₂                  ≡⟨ sym (Pullback.commutes (Fl.is-pullback f A→Y)) ⟩
        f C.∘ Fl.pb-π₁                    ∎
    
    q :   Fl.pb-π₁ C.∘ Pullback.limiting (Fl.is-pullback f C→Y) this-path
      C.∘ Pullback.limiting (Fl.is-pullback f B→Y) that-path
      ≡   Fl.pb-π₁

    q = Fl.pb-π₁ C.∘ _ C.∘ _   ≡⟨ C.assoc₂ _ _ _ ⟩
        (Fl.pb-π₁ C.∘ _) C.∘ _ ≡⟨ ap (λ e → e C.∘ Pullback.limiting (Fl.is-pullback f B→Y) that-path) (Pullback.p₁∘limiting≡p₁' (Fl.is-pullback f C→Y) _) ⟩
        _                      ≡⟨ Pullback.p₁∘limiting≡p₁' (Fl.is-pullback f B→Y) _ ⟩
        Fl.pb-π₁               ∎

    r :  Fl.pb-π₂ C.∘ Pullback.limiting (Fl.is-pullback f C→Y) this-path
      C.∘ Pullback.limiting (Fl.is-pullback f B→Y) that-path
      ≡  (g C.∘ h) C.∘ Fl.pb-π₂
    r = Fl.pb-π₂ C.∘ _ C.∘ _    ≡⟨ C.assoc₂ _ _ _ ⟩
        (Fl.pb-π₂ C.∘ _) C.∘ _  ≡⟨ ap (λ e → e C.∘ Pullback.limiting (Fl.is-pullback f B→Y) that-path) (Pullback.p₂∘limiting≡p₂' (Fl.is-pullback f C→Y) this-path) ⟩
        _                       ≡⟨ C.assoc₁ _ _ _ ⟩
        _                       ≡⟨ ap (λ e → g C.∘ e) (Pullback.p₂∘limiting≡p₂' (Fl.is-pullback f B→Y) that-path) ⟩
        _                       ≡⟨ C.assoc₂ _ _ _ ⟩
        _                       ∎

    unique = Pullback.unique (Fl.is-pullback f C→Y) (sym p) q r
    goal : Pullback.limiting (Fl.is-pullback f C→Y) this-path 
        C.∘ Pullback.limiting (Fl.is-pullback f B→Y) that-path
          ≡ Pullback.limiting (Fl.is-pullback f C→Y) composite-path
            
    goal = subst (λ e → Pullback.limiting (Fl.is-pullback f C→Y) this-path
                    C.∘ Pullback.limiting (Fl.is-pullback f B→Y) that-path
                      ≡ Pullback.limiting (Fl.is-pullback f C→Y) e)
                  (UIP _ composite-path)
                  unique
