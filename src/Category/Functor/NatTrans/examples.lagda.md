```
open import 1Lab.Data.List
open import 1Lab.Coeq
open import 1Lab.Base

open import Category

open Functor
open _=>_
open _≅_

module
  Category.Functor.NatTrans.examples
  {ℓ : _}
  where
```

# Example: `reverse`

This module contains a concrete, worked example of a natural
transformation: The `reverse` function.

Fixing a universe level `ℓ`{.Agda} throughout, we must first make
explicit the construction of `List`{.Agda} as an endofunctor on the
category of `Sets`{.Agda}. The bulk of the construction - the list type
itself, `map`{.Agda}, and the two lemmas proving that map is functorial
- are built [in a separate module].

[in a separate module]: agda://1Lab.Data.List

```
List-functor : Functor (Sets ℓ) (Sets ℓ)
F₀ List-functor = List
F₁ List-functor = map
F-id List-functor = fun-ext map-id
F-∘ List-functor f g = fun-ext (map-comp {f = f} {g = g})
```

Now we can build the reverse function. The reversal of a list is built
recursively: the empty list (`nil`{.Agda}) reverses into itself, and if
there's an element in front of the list, it gets sent to the back (by
appending - `++`{.Agda}).

```
reverse : {A : Set ℓ} → List A → List A
reverse nil = nil
reverse (cons x xs) = reverse xs ++ (cons x nil)
```

We wish to prove that the function `reverse`{.Agda} is a `natural
transformation`{.Agda ident="_=>_"} from `List`{.Agda} to
itself. After, we'll prove that `reverse`{.Agda} is its own inverse
function, thus making it an automorphism of `List`{.Agda} in the
[functor category] $[\mathrm{Sets},\mathrm{Sets}]$.

[functor category]: agda://Category.Instances.FunctorCat

The obligation we have is to show $\mathrm{reverse} \circ
\mathrm{map}(f) = \mathrm{map}(f) \circ \mathrm{reverse}$. The inductive
case is shown by [equational
reasoning](1Lab.introduction.html#equational-reasoning): First that
`map`{.Agda} is homomorphic over `++`{.Agda}, then by an inductive
application of `reverse-natural`{.Agda}

[case split]: https://agda.readthedocs.io/en/v2.6.2/tools/emacs-mode.html#keybindings
[auto]: https://agda.readthedocs.io/en/v2.6.2/tools/auto.html

```
reverse-natural : {x y : Set ℓ} (f : x → y)
                → (l : List x)
                → map f (reverse l)
                ≡ reverse (map f l)
reverse-natural f nil = refl
reverse-natural f (cons x l) =
  map f (reverse l ++ cons x nil)         ≡⟨ map-++ (reverse l) (cons x nil) ⟩
  map f (reverse l) ++ map f (cons x nil) ≡⟨ refl ⟩
  map f (reverse l) ++ cons (f x) nil     ≡⟨ ap₂ _++_ (reverse-natural f l) refl ⟩
  reverse (map f l) ++ cons (f x) nil     ∎
```

This lemma is exactly what we need to show that `reverse`{.Agda} is a
`natural transformation`{.Agda ident="_=>_"}! It just needs a bit of
minor adjustment: First, `reverse-natural`{.Agda} is in the wrong
direction, which we can correct using `sym`{.Agda}, and it's _pointwise_
(quantified over `(l : List x)`) - we need to be an equality of
*functions*. That's what `fun-ext`{.Agda} is for - functions which are
"locally the same, everywhere" are identical.

```
reverse-nt : List-functor => List-functor
η reverse-nt A = reverse {A = A}
is-natural reverse-nt x y f = sym (fun-ext (reverse-natural f))
```

After showing that `reverse`{.Agda} can be the family of morphisms of `a
natural transformation`{.Agda ident="_=>_"}, we can show something even
better - it's an _isomorphism_. An isomorphism in a functor category is
called a _`natural isomorphism`{.Agda ident="_≅_"}_. The obligation now
is to show that $\mathrm{reverse}(\mathrm{reverse}(l)) = l$.

```
reverse-reverse : {A : Set ℓ} (l : List A) → reverse (reverse l) ≡ l
reverse-reverse nil = refl
```

The base case is automatic (thank you, computation), but the inductive
case needs a bit more work. The proof is written in equational
reasoning style so you can toggle equations (either on the sidebar if,
your screen is wide enough, or on the top of the page).

```
reverse-reverse {A} (cons x acc) =
  reverse (reverse acc ++ cons x nil) ≡⟨ reverse-++ (reverse acc) _ ⟩
  cons x nil ++ reverse (reverse acc) ≡⟨ refl ⟩
  cons x (reverse (reverse acc))      ≡⟨ ap₂ cons refl (reverse-reverse acc) ⟩
  cons x acc                          ∎
  where
    reverse-++ : (xs ys : List A) → reverse (xs ++ ys) ≡ reverse ys ++ reverse xs
```

`reverse-++`{.Agda} is a lemma - a helper function - expressing that
`reverse`{.Agda} acts on `++`{.Agda} like an [anti-homomorphism] - it
"distributes over" `++`{.Agda}, but flips the operands around in the
process.

[anti-homomorphism]: https://en.wikipedia.org/wiki/Antihomomorphism

```
    reverse-++ nil ys = ++-id-left _
    reverse-++ (cons x xs) ys =
      reverse (xs ++ ys) ++ cons x nil         ≡⟨ ap₂ _++_ (reverse-++ xs ys) refl ⟩
      (reverse ys ++ reverse xs) ++ cons x nil ≡⟨ sym (++-assoc (reverse ys) _ _) ⟩
      reverse ys ++ (reverse xs ++ cons x nil) ∎
```

This finishes the proof that `reverse`{.Agda} is a natural automorphism
of `List`{.Agda}.

```
reverse-ni : List-functor ≅ List-functor
to reverse-ni = reverse-nt
from reverse-ni = reverse-nt
to-from reverse-ni = fun-ext reverse-reverse
from-to reverse-ni = fun-ext reverse-reverse
```

# Example: `list→maybe`

A more illustrative example of natural transformation is one between
different functors, that doesn't even come close to being invertible -
`listToMaybe`{.Agda}. The idea is that we have a list of things, and
that _might_ have a first element. This possibility of failure is
described using the `Maybe`{.Agda} endofunctor of `Sets`{.Agda}:

```
data Maybe (A : Set ℓ) : Set ℓ where
  nothing : Maybe A
  just : A → Maybe A

Maybe-functor : Functor (Sets ℓ) (Sets ℓ)
F₀ Maybe-functor = Maybe
F₁ Maybe-functor f nothing = nothing
F₁ Maybe-functor f (just x) = just (f x)
F-id Maybe-functor = fun-ext λ where
  nothing → refl
  (just x) → refl
F-∘ Maybe-functor f g = fun-ext λ where
  nothing → refl
  (just x) → refl
```

If the list is a `cons`{.Agda}, it gets turned into `just`{.Agda} the
head - Otherwise, there's `nothing`.

```
list→maybe : {A : Set ℓ} → List A → Maybe A
list→maybe nil = nothing
list→maybe (cons x x₁) = just x
```

Unlike the `reverse`{.Agda} example, `list→maybe`{.Agda} is a natural
transformation between two _different_ functors - so the proof has two
different functor actions. `F₁ List-functor`{.Agda ident=map} on the one
side, and `F₁ Maybe-functor`{.Agda ident=Maybe-functor} on the other.

```
list→maybe-isNatural : {A B : Set ℓ} (f : A → B) (x : List A)
                     →  list→maybe (F₁ List-functor f x)
                     ≡! F₁ Maybe-functor f (list→maybe x)
list→maybe-isNatural f nil = refl
list→maybe-isNatural f (cons _ _) = refl
```

However - due to how simple the definition of `list→maybe`{.Agda} is -
after a case split, the proof is definitional. After applying
`fun-ext`{.Agda}, the proof is done.

```
List=>Maybe : List-functor => Maybe-functor
η List=>Maybe _ = list→maybe
is-natural List=>Maybe _ _ f = fun-ext (list→maybe-isNatural f)
```
