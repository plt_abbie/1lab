```
open import 1Lab.Base

open import Category.Functor.Adjoints.Monad
open import Category.Constructions.Monad
open import Category.Functor.Adjoints
open import Category

open Functor
open _=>_
```

# Monadic Adjunctions

An adjunction $F \dashv G$ between functors $F : C \to D$ and $G : D \to
C$ is _monadic_ if the induced `comparison functor`{.Agda
ident=Comparison} $D \to C^{R \circ L}$ (where the right-hand side is
the `Eilenberg-Moore`{.Agda} category of the [monad of the
adjunction](Category.Functor.Adjoints.Monad.html)) is an equivalence of
categories.

```
module
  Category.Functor.Adjoints.Monadic
  {o₁ h₁ o₂ h₂ : _}
  {C : Category o₁ h₁}
  {D : Category o₂ h₂}
  {L : Functor C D} {R : Functor D C}
  (L⊣R : L ⊣ R)
  where

private
  module C = Category.Category C
  module D = Category.Category D
  module L = Functor L
  module R = Functor R
  module adj = _⊣_ L⊣R

L∘R : Monad
L∘R = Adjunction→Monad L⊣R

open Monad L∘R
```

The composition of `R.₁`{.Agda} with the `adjunction counit`{.Agda
ident="adj.counit.η"} natural transformation gives `R`{.Agda} a
`M-Algebra`{.Agda ident="_-Algebra"} structure, thus extending `R` to a
functor $D \to C^{L \circ R}$.

```
Comparison : Functor D (Eilenberg-Moore L∘R)
F₀ Comparison x =
  record
    { object = R.₀ x
    ; ν      = R.₁ (adj.counit.η _)
    ; ν-mult =
      R.₁ (adj.counit.η _) C.∘ M₁ (R.₁ (adj.counit.η _))        ≡⟨ sym (R.F-∘ _ _) ⟩
      R.₁ (adj.counit.η _ D.∘ L.₁ (R.₁ (adj.counit.η _)))       ≡⟨ ap R.₁ (adj.counit.is-natural _ _ _) ⟩
      R.₁ (adj.counit.η x D.∘ adj.counit.η (L.₀ (R.₀ x)))       ≡⟨ R.F-∘ _ _ ⟩
      R.₁ (adj.counit.η x) C.∘ R.₁ (adj.counit.η (L.₀ (R.₀ x))) ∎
    ; ν-unit = adj.zag
    }
```

<details>
<summary> Construction of the functorial action of `Comparison`{.Agda} </summary>
```
F₁ Comparison x =
  record
    { morphism = R.₁ x
    ; commutes =
      R.₁ x C.∘ R.₁ (adj.counit.η _)        ≡⟨ sym (R.F-∘ _ _) ⟩
      R.₁ (x D.∘ adj.counit.η _)            ≡⟨ ap R.₁ (sym (adj.counit.is-natural _ _ _)) ⟩
      R.₁ (adj.counit.η _ D.∘ L.₁ (R.₁ x))  ≡⟨ R.F-∘ _ _ ⟩
      R.₁ (adj.counit.η _) C.∘ M₁ (R.₁ x)   ∎
    }
F-id Comparison = Algebra=>≡ R.F-id
F-∘ Comparison f g = Algebra=>≡ (R.F-∘ _ _)
```
</details>

An adjunction is _monadic_ if `Comparison`{.Agda} is an `equivalence of
categories`{.Agda ident="is-Equiv"}.

```
is-Monadic : Set _
is-Monadic = is-Equiv Comparison
```

<!--
```
_ = _-Algebra
```
-->
