open import 1Lab.Base

open import Category

open _=>_

module
  Category.Functor.NatIso.Properties
  where

module _
  {o₁ h₁ : _} {C : Category o₁ h₁} {F : Functor C C}
  (iso : F ≅ Id)
  where
  private
    module C = Category.Category C
    module F = Functor F
    module iso = _≅_ iso

  F≃id-natural₁ : {A : _} → η iso.to (F.₀ A) ≡ F.₁ (η iso.to A)
  F≃id-natural₁ {A} =
    η iso.to (F.₀ A)                                               ≡⟨ sym (C.idr _) ⟩
    η iso.to (F.₀ A) C.∘ C.id                                      ≡⟨ ap (C._∘_ _) (sym F.F-id) ⟩
    η iso.to (F.₀ A) C.∘ F.₁ C.id                                  ≡⟨ ap (λ e → η iso.to _ C.∘ F.₁ e) (sym iso.to-from) ⟩
    η iso.to (F.₀ A) C.∘ F.₁ (η iso.from A C.∘ η iso.to A)         ≡⟨ ap (λ e → η iso.to _ C.∘ e) (F.F-∘ _ _) ∘p C.assoc₂ _ _ _ ⟩
    (η iso.to (F.₀ A) C.∘ F.₁ (η iso.from A)) C.∘ F.₁ (η iso.to A) ≡⟨ ap (λ e → e C.∘ F.₁ _) (is-natural iso.to _ _ _) ⟩
    _ C.∘ F.₁ (η iso.to A)                                         ≡⟨ ap (λ e → e C.∘ F.₁ _) iso.to-from ∘p C.idl _ ⟩
    F.₁ (η iso.to A)                                               ∎

  F≃id-natural₂ : {A : _} → η iso.from (F.₀ A) ≡ F.₁ (η iso.from A)
  F≃id-natural₂ {A} =
    η iso.from (F.₀ A)                                               ≡⟨ sym (C.idl _) ⟩
    C.id C.∘ η iso.from (F.₀ A)                                      ≡⟨ ap (λ e → e C.∘ η iso.from _) (sym F.F-id) ⟩
    F.₁ C.id C.∘ η iso.from (F.₀ A)                                  ≡⟨ ap (λ e → F.₁ e C.∘ η iso.from _) (sym iso.to-from) ⟩
    F.₁ (η iso.from A C.∘ η iso.to A) C.∘ η iso.from (F.₀ A)         ≡⟨ ap (λ e → e C.∘ η iso.from _) (F.F-∘ _ _) ∘p C.assoc₁ _ _ _ ⟩
    F.₁ (η iso.from A) C.∘ F.₁ (η iso.to A) C.∘ η iso.from (F.₀ A)   ≡⟨ ap (C._∘_ (F.₁ _)) (sym (is-natural iso.from _ _ _)) ⟩
    F.₁ (η iso.from A) C.∘ η iso.from A C.∘ η iso.to A               ≡⟨ ap (C._∘_ (F.₁ _)) iso.to-from ⟩
    F.₁ (η iso.from A) C.∘ C.id                                      ≡⟨ C.idr _ ⟩
    F.₁ (η iso.from A)                                               ∎

module _
  {o₁ h₁ o₂ h₂ : _} {C : Category o₁ h₁} {D : Category o₂ h₂}
  {F : Functor C D} {G : Functor C D}
  (iso : F ≅ G)
  where

  private
    module iso = _≅_ iso
  
  ≅-inv : G ≅ F
  ≅-inv = record { to = iso.from ; from = iso.to ; to-from = iso.from-to ; from-to = iso.to-from }
