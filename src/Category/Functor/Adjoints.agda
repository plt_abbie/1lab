open import 1Lab.Isomorphism
open import 1Lab.Coeq
open import 1Lab.Base

open import Category.Functor.Hom
open import Category

import Category.Associativity

module Category.Functor.Adjoints where

-- Adjoint functors
record
  _⊣_
    {o₁ h₁ o₂ h₂ : _}
    {C : Category o₁ h₁} {D : Category o₂ h₂}
    (F : Functor C D) (G : Functor D C)
  : Set (o₁ ⊔ o₂ ⊔ h₁ ⊔ h₂)
  where
  private
    module C = Category.Category C
    module D = Category.Category D
    module F = Functor F
    module G = Functor G

  field
    unit : Id => (G F∘ F)
    counit : (F F∘ G) => Id
  
  module unit = _=>_ unit
  module counit = _=>_ counit

  field
    zig : {A : _} → counit.η (F.₀ A) D.∘ F.₁ (unit.η A) ≡! D.id
    zag : {B : _} → G.₁ (counit.η B) C.∘ unit.η (G.₀ B) ≡! C.id


  op : Functor.op G ⊣ Functor.op F
  op = record { unit = counit.op ; counit = unit.op ; zig = zag ; zag = zig }
  
module
  _ {o₁ o₂ h : _} {C : Category o₁ h} {D : Category o₂ h}
    (F : Functor C D) (G : Functor D C)
  where
  private
    module C = Category.Category C
    module D = Category.Category D
    module F = Functor F
    module G = Functor G

    module AssC = Category.Associativity C
    module AssD = Category.Associativity D
  
  open _≅_
  open _=>_
    
  adjoint→hom≃ : F ⊣ G → Hom[ F -,-] ≅ Hom[-, G -]
  adjoint→hom≃ x = r where
    open _⊣_ x

    r : Hom[ F -,-] ≅ Hom[-, G -]
    to r = NT (λ { (A , B) A→GB → G.₁ A→GB C.∘ η unit _ }) λ where
      (a , b) (a' , b') (f , g) → fun-ext λ e →
        G.₁ (g D.∘ e D.∘ F.₁ f) C.∘ η unit a'           ≡⟨ ap (λ e → e C.∘ η unit _) (G.F-∘ _ _) ⟩
        (G.₁ g C.∘ G.₁ (e D.∘ F.₁ f)) C.∘ η unit a'     ≡⟨ ap (λ e → (_ C.∘ e) C.∘ η unit _) (G.F-∘ _ _) ⟩
        (G.₁ g C.∘ G.₁ e C.∘ G.₁ (F.₁ f)) C.∘ η unit a' ≡⟨ sym AssC.first3-out-of-4 ⟩
        G.₁ g C.∘ G.₁ e C.∘ (G.₁ (F.₁ f) C.∘ η unit a') ≡⟨ ap (λ e → _ C.∘ _ C.∘ e) (sym (is-natural unit _ _ _)) ⟩
        G.₁ g C.∘ G.F₁ e C.∘ (η unit a C.∘ f)           ≡⟨ AssC.second2-out-of-4 ⟩
        G.₁ g C.∘ (G.F₁ e C.∘ η unit a) C.∘ f           ∎

    from r = NT (λ { (A , B) FA→B → η counit _ D.∘ F.₁ FA→B }) λ where
      (a , b) (a' , b') (f , g) → fun-ext λ e →
        η counit _ D.∘ F.₁ (G.₁ g C.∘ e C.∘ f)           ≡⟨ ap (λ e → _ D.∘ e) (F.F-∘ _ _) ⟩
        η counit _ D.∘ F.₁ (G.₁ g) D.∘ F.₁ (e C.∘ f)     ≡⟨ ap (λ e → _ D.∘ F.₁ (G.₁ g) D.∘ e) (F.F-∘ _ _) ⟩
        η counit _ D.∘ F.₁ (G.₁ g) D.∘ F.₁ e D.∘ F.₁ f   ≡⟨ AssD.first2-out-of-4 ⟩
        (η counit _ D.∘ F.₁ (G.₁ g)) D.∘ F.₁ e D.∘ F.₁ f ≡⟨ ap (λ e → e D.∘ _ D.∘ _) (is-natural counit _ _ g) ⟩
        (g D.∘ η counit _) D.∘ F.₁ e D.∘ F.₁ f           ≡⟨ sym AssD.first2-out-of-4 ∘p AssD.second2-out-of-4 ⟩
        g D.∘ (η counit _ D.∘ F.₁ e) D.∘ F.₁ f           ∎
        
    to-from r {(A , B)} = fun-ext λ f →
      η counit _ D.∘ F.₁ (G.₁ f C.∘ η unit _)         ≡⟨ ap (λ e → _ D.∘ e) (F.F-∘ _ _) ∘p AssD.right→left ⟩
      (η counit _ D.∘ F.₁ (G.₁ f)) D.∘ F.₁ (η unit _) ≡⟨ ap (λ e → e D.∘ F.₁ (η unit _)) (is-natural counit _ _ _) ∘p AssD.left→right ⟩
      f D.∘ η counit _ D.∘ F.₁ (η unit _)             ≡⟨ ap (D._∘_ f) zig ∘p D.idr _ ⟩
      f                                               ∎
    from-to r {(A , B)} = fun-ext λ f →
      G.₁ (η counit B D.∘ F.₁ f) C.∘ η unit A         ≡⟨ ap (λ e → e C.∘ η unit _) (G.F-∘ _ _) ∘p AssC.left→right ⟩
      G.₁ (η counit B) C.∘ (G.₁ (F.₁ f) C.∘ η unit A) ≡⟨ (ap (λ e → G.₁ _ C.∘ e) (sym (is-natural unit _ _ _))) ∘p AssC.right→left ⟩
      (G.₁ (η counit B) C.∘ η unit (G.₀ B)) C.∘ f     ≡⟨ ap (λ e → e C.∘ f) zag ∘p C.idl _ ⟩
      f                                               ∎

  adjoint→≃ : {X : _} {Y : _}
            → F ⊣ G
            → D.Hom (F.₀ X) Y ≃ C.Hom X (G.₀ Y)
  adjoint→≃ adj with adjoint→hom≃ adj
  ... | record { to = to ; from = from ; to-from = to-from ; from-to = from-to }
      = let
          to' = η to
          from' = η from
        in iso (λ x → to' (_ , _) x)
               (λ x → from' (_ , _) x)
               (happly to-from)
               (happly from-to)

module
  _ {o₁ o₂ h : _}
    {C : Category o₁ h} {D : Category o₂ h}
    {F : Functor C D} {G : Functor D C}
  where
  private
    module C = Category.Category C
    module D = Category.Category D
    module F = Functor F
    module G = Functor G

    module AssC = Category.Associativity C
    module AssD = Category.Associativity D
  
  open _≅_
  open _=>_
  open _⊣_
    
  hom≃→adjoint : Hom[ F -,-] ≅ Hom[-, G -] → F ⊣ G
  hom≃→adjoint morp = r where
    module morp = _≅_ morp

    r : F ⊣ G
    unit r = NT (λ ob → η morp.to _ D.id) λ x y f →
      η morp.to (y , F.₀ y) D.id C.∘ f                      ≡⟨ ap (λ e → e C.∘ f) (sym (C.idl _) ∘p ap (λ e → e C.∘ _) (sym G.F-id)) ∘p AssC.left→right ⟩
      G.₁ D.id C.∘ η morp.to (y , F.₀ y) D.id C.∘ f         ≡⟨ happly (sym (is-natural morp.to _ _ _)) _ ⟩
      η morp.to (x , F.₀ y) (D.id D.∘ D.id D.∘ F.₁ f)       ≡⟨ ap (η morp.to _) (AssD.right→left ∘p ap (λ e → e D.∘ _) (D.idl _) ∘p D.idl _) ⟩
      η morp.to (x , F.₀ y) (F.₁ f)                         ≡⟨ ap (η morp.to _) (sym (D.idr _) ∘p ap (λ e → _ D.∘ e) (sym (D.idl _)) ∘p ap (λ e → _ D.∘ _ D.∘ e) (sym F.F-id)) ⟩
      η morp.to (x , F.₀ y) (F.₁ f D.∘ D.id D.∘ F.₁ C.id)   ≡⟨ happly (is-natural morp.to _ _ _) _ ∘p AssC.right→left ⟩
      (G.₁ (F.₁ f) C.∘ η morp.to (x , F.₀ x) D.id) C.∘ C.id ≡⟨ C.idr _ ⟩
      G.₁ (F.₁ f) C.∘ η morp.to (x , F.₀ x) D.id            ∎

    counit r = NT (λ ob → η morp.from _ C.id) λ x y f →
      η morp.from (G.₀ y , y) C.id D.∘ F.₁ (G.₁ f)             ≡⟨ ap (λ e → e D.∘ _) (sym (D.idl _)) ∘p AssD.left→right ⟩
      D.id D.∘ η morp.from (G.₀ y , y) C.id D.∘ F.₁ (G.₁ f)    ≡⟨ happly (sym (is-natural morp.from _ _ _)) _ ⟩
      η morp.from (G.F₀ x , y) (G.F₁ D.id C.∘ C.id C.∘ G.F₁ f) ≡⟨ ap (η morp.from _) (AssC.right→left ∘p ap (λ e → e C.∘ _) (C.idr _ ∘p G.F-id) ∘p C.idl _) ⟩
      η morp.from (G.F₀ x , y) (G.F₁ f)                        ≡⟨ ap (η morp.from _) (sym (C.idr _) ∘p ap (λ e → _ C.∘ e) (sym (C.idr _))) ⟩
      η morp.from (G.F₀ x , y) (G.F₁ f C.∘ C.id C.∘ C.id)      ≡⟨ happly (is-natural morp.from _ _ _) _ ∘p AssD.right→left ⟩
      (f D.∘ η morp.from (G.F₀ x , x) C.id) D.∘ F.F₁ C.id      ≡⟨ ap (λ e → _ D.∘ e) F.F-id ∘p D.idr _ ⟩
      f D.∘ η morp.from (G.₀ x , x) C.id                       ∎

    zig r =
      η morp.from _ C.id D.∘ F.₁ (η morp.to _ D.id)          ≡⟨ ap (λ e → e D.∘ _) (sym (D.idl _)) ∘p AssD.left→right ⟩
      D.id D.∘ η morp.from _ C.id D.∘ F.₁ (η morp.to _ D.id) ≡⟨ happly (sym (is-natural morp.from _ _ _)) _ ⟩
      η morp.from _ (G.₁ D.id C.∘ C.id C.∘ η morp.to _ D.id) ≡⟨ ap (η morp.from _) (AssC.right→left ∘p ap (λ e → (e C.∘ η morp.to _ D.id)) (C.idr _ ∘p G.F-id) ∘p C.idl _) ⟩
      η morp.from _ (η morp.to _ D.id)                       ≡⟨ happly morp.to-from _ ⟩
      D.id                                                   ∎

    zag r =
      G.₁ (η morp.from _ C.id) C.∘ η morp.to _ D.id          ≡⟨ ap (λ e → _ C.∘ e) (sym (C.idr _)) ⟩
      G.₁ (η morp.from _ C.id) C.∘ η morp.to _ D.id C.∘ C.id ≡⟨ happly (sym (is-natural morp.to _ _ _)) _ ⟩
      η morp.to _ (η morp.from _ C.id D.∘ D.id D.∘ F.₁ C.id) ≡⟨ ap (η morp.to _) (AssD.right→left ∘p ap (λ e → _ D.∘ e) F.F-id ∘p D.idr _ ∘p D.idr _) ⟩
      η morp.to _ (η morp.from _ C.id)                       ≡⟨ happly morp.from-to _ ⟩
      C.id                                                   ∎
