```agda
open import 1Lab.Data.Finite
open import 1Lab.Data.Vector
open import 1Lab.Coeq
open import 1Lab.Base

open import Agda.Builtin.Nat

open import Category.Functor.Properties
open import Category

module Algebra.Theory where
```

⚠️ WIP ⚠️

```
record Signature (s : _) : Set (lsuc s) where
  field
    operations : Set s
    o-arities  : operations → Nat

data Term {s v : _} (S : Signature s) (n : Set v) : Set (s ⊔ v) where
  var : n → Term S n
  op  : (o : Signature.operations S)
      → Vect (Term S n) (Signature.o-arities S o)
      → Term S n

record Theory (s l : _) : Set (lsuc (s ⊔ l)) where
  field
    signature : Signature s

  open Signature signature public

  field
    laws       : Set l
    l-arities  : laws → Nat
    l-relates  : (l : laws) → Term signature (Fin (l-arities l))
                            × Term signature (Fin (l-arities l))

Interprets : {ℓ s : _} (A : Set ℓ) (S : Signature s) → Set (ℓ ⊔ s)
Interprets A S = (o : Signature.operations S) → Vect A (Signature.o-arities S o) → A

mutual
  evaluate : {ℓ s : _} {n : _} {A : Set ℓ} {S : Signature s}
           → Interprets A S
           → Vect A n
           → Term S (Fin n) → A

  evaluate' : {ℓ s : _} {n k : Nat} {A : Set ℓ} {S : Signature s}
            → Interprets A S
            → Vect A k
            → Vect (Term S (Fin k)) n → Vect A n

  evaluate opers vars (var idx) = vars !! idx
  evaluate opers vars (op o x) = opers o (evaluate' opers vars x)

  evaluate' f v nil = nil
  evaluate' f v (x :: xs) = evaluate f v x :: evaluate' f v xs


record _⊨_ {ℓ s l : _} (A : Set ℓ) (S : Theory s l) : Set (ℓ ⊔ s ⊔ l) where
  open Theory S
  field
    interpretation : Interprets A signature
    sound : (l : laws) (vars : Vect A (l-arities l))
          → let x = l-relates l
            in evaluate interpretation vars (_×_.fst x)
            ≡! evaluate interpretation vars (_×_.snd x)

record Model {ℓ s l : _} (S : Theory s l) : Set (lsuc ℓ ⊔ s ⊔ l) where
  field
    model : Set ℓ
    models : model ⊨ S

  open _⊨_ models public

record Homomorphism {ℓ ℓ' s l : _}
                    (S : Theory s l)
                    (M : Model {ℓ} S)
                    (M' : Model {ℓ'} S)
       : Set (ℓ ⊔ ℓ' ⊔ l ⊔ s)
  where
  private
    module S = Theory S
    module Dom = Model M
    module Cod = Model M'

  field
    function : Dom.model → Cod.model
    homomorphic : (o : S.operations) (args : Vect (Dom.model) (S.o-arities o))
                → function (Dom.interpretation o args)
                ≡ Cod.interpretation o (v-map function args)

Homomorphism≡ : {ℓ ℓ' s l : _}
                {S : Theory s l}
                {M : Model {ℓ} S}
                {M' : Model {ℓ'} S}
              → {H H' : Homomorphism S M M'}
              → ((x : _) → Homomorphism.function H x
                        ≡! Homomorphism.function H' x)
              → H ≡ H'
Homomorphism≡ {H = record { function = _ ; homomorphic = r }}
              {H' = record { function = _ ; homomorphic = s }}
              p
  rewrite →rewrite (fun-ext p)
  rewrite →rewrite (fun-ext λ o → fun-ext λ a → UIP (r o a) (s o a))
  = refl

_-Models : {ℓ s l : _} → Theory s l → Category _ _
Category.Ob (_-Models {ℓ} S) = Model {ℓ} S
Category.Hom (S -Models) = Homomorphism S

Category.id (S -Models) {x} =
  record { function = λ x → x
         ; homomorphic = λ o args → ap (_⊨_.interpretation (Model.models x) _)
                                        (sym (v-map-id args))
         }

Category._∘_ (S -Models) {x} {y} {z} f g =
  record { function = λ x → f.function (g.function x)
         ; homomorphic = homomorphic
         }
  where
    module f = Homomorphism f
    module g = Homomorphism g
    homomorphic : (o : _) (args : _)
                → f.function (g.function (Model.interpretation x o args))
                ≡ Model.interpretation z o _
    homomorphic o args
      rewrite →rewrite (g.homomorphic o args)
      rewrite →rewrite (f.homomorphic o (v-map g.function args))
      rewrite →rewrite (sym (v-map-comp {f = f.function} {g = g.function} args))
      = refl

Category.idr (S -Models) f = Homomorphism≡ λ x → refl
Category.idl (S -Models) f = Homomorphism≡ λ x → refl
Category.assoc₁ (S -Models) f g h = Homomorphism≡ λ x → refl
Category.assoc₂ (S -Models) f g h = Homomorphism≡ λ x → refl

Forget : {ℓ s l : _} {T : Theory s l}
       → Functor (T -Models) (Sets ℓ)
Functor.F₀ Forget = Model.model
Functor.F₁ Forget = Homomorphism.function
Functor.F-id Forget = refl
Functor.F-∘ Forget f g = refl

Forget-faithful : {ℓ s l : _} {T : Theory s l} → Faithful (Forget {ℓ} {T = T})
Forget-faithful proof = Homomorphism≡ λ x → happly proof x
```
