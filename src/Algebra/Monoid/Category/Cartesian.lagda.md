```agda
open import 1Lab.Data.Vector
open import 1Lab.Base

open import Algebra.Monoid
open import Algebra.Theory

open import Category.Structure.Cartesian

open Homomorphism
open Cartesian

module Algebra.Monoid.Category.Cartesian {ℓ : Level} where
```

# Limits in `Mon`{.Agda}

Since the forgetful functor $U : \mathrm{Mon} \to \mathrm{Set}$ is a
right adjoint, it preserves limits - therefore limits in $\mathrm{Mon}$
are limits in $\mathrm{Set}$, with the monoid operation computed
pointwise.

```agda
Mon-cartesian : Cartesian (Mon {ℓ})
_×C_ Mon-cartesian A B =
  record
    { model  = A.model × B.model
    ; models =
      record
        { interpretation = λ where
          unit nil → Model.interpretation A unit nil
                  , Model.interpretation B unit nil
          ⊗ ((ax , bx) :: (ay , by) :: nil) →
              Model.interpretation A ⊗ (ax :: ay :: nil)
            , Model.interpretation B ⊗ (bx :: by :: nil)
        ; sound = λ where
          associativity ((ax , bx) :: (ay , by) :: (az , bz) :: nil) →
            ×Path (Model.sound A associativity (ax :: ay :: az :: nil))
                  (Model.sound B associativity (bx :: by :: bz :: nil))
          left-identity ((ax , bx) :: nil) →
            ×Path (Model.sound A left-identity (ax :: nil))
                  (Model.sound B left-identity (bx :: nil))
          right-identity ((ax , bx) :: nil) →
            ×Path (Model.sound A right-identity (ax :: nil))
                  (Model.sound B right-identity (bx :: nil))
        }
    }
  where
    module A = Model A
    module B = Model B
```

There are monoid homomorphisms between the product and its factors.

```agda
function (π₁ Mon-cartesian) (fst , snd) = fst
homomorphic (π₁ Mon-cartesian) unit nil = refl
homomorphic (π₁ Mon-cartesian) ⊗ (x :: y :: nil) = refl

function (π₂ Mon-cartesian) (fst , snd) = snd
homomorphic (π₂ Mon-cartesian) unit nil = refl
homomorphic (π₂ Mon-cartesian) ⊗ (x :: y :: nil) = refl
```

The product of two homomorphisms is again a homomorphism.

```agda
_h×_ Mon-cartesian f g =
  record { function = λ x → function f x , function g x
         ; homomorphic = λ where
            unit nil → ×Path (homomorphic f unit nil) (homomorphic g unit nil)
            ⊗ (x :: y :: nil) →
              ×Path (homomorphic f ⊗ (x :: y :: nil))
                    (homomorphic g ⊗ (x :: y :: nil))
         }
```

The proofs that these make a limiting cone are all by computation.

```
π₁∘h×≡f Mon-cartesian f g = Homomorphism≡ λ x → refl
π₂∘h×≡g Mon-cartesian f g = Homomorphism≡ λ x → refl
h×-unique Mon-cartesian refl refl = Homomorphism≡ λ x → refl
```

The terminal object in `Mon`{.Agda} is the terminal object in
[Sets](agda://Category#Sets), with the unique monoid structure.

```agda
Model.model (⊤ Mon-cartesian) = Top
Model.models (⊤ Mon-cartesian) =
  record { interpretation = λ _ _ → tt
         ; sound = λ _ _ → refl
         }
terminal Mon-cartesian =
  contract
    (record
      { function    = λ x → tt
      ; homomorphic = λ o args → refl
      })
    λ y → Homomorphism≡ λ _ → refl
```
