```agda
open import 1Lab.Data.Vector
open import 1Lab.Base hiding (_×_)

open import Algebra.Monoid
open import Algebra.Theory

open Homomorphism
```

# Eckmann-Hilton Argument

If a Set has two monoid structures where one is homomorphic over the
other, then these coincide, and the monoid is commutative.

```agda
module
  Algebra.Monoid.EckmannHilton
    {ℓ : _}
    (S : Set ℓ)
    (m : S ⊨ monoid)
    (m' : S ⊨ monoid)
  where

  module m = _⊨_ m
  module m' = _⊨_ m'
```

<details>
<summary>
We begin by introducing friendly helpers for the operations in `m` and `m'`.
These are routine.
</summary>

```
  1· : S
  1· = m.interpretation unit nil

  1× : S
  1× = m'.interpretation unit nil

  _·_ : S → S → S
  x · y = m.interpretation ⊗ (x :: y :: nil)

  _×_ : S → S → S
  x × y = m'.interpretation ⊗ (x :: y :: nil)

  ·-id-l : {x : S} → x ≡ 1· · x
  ·-id-l {x} = m.sound left-identity (x :: nil)

  ·-id-r : {x : S} → x ≡ x · 1·
  ·-id-r {x} = m.sound right-identity (x :: nil)

  ·-assoc : {x y z : S} → x · (y · z) ≡ (x · y) · z 
  ·-assoc {x} {y} {z} = m.sound associativity (x :: y :: z :: nil)

  ×-id-l : {x : S} → x ≡ 1× × x
  ×-id-l {x} = m'.sound left-identity (x :: nil)

  ×-id-r : {x : S} → x ≡ x × 1×
  ×-id-r {x} = m'.sound right-identity (x :: nil)

  ×-assoc : {x y z : S} → x × (y × z) ≡ (x × y) × z 
  ×-assoc {x} {y} {z} = m'.sound associativity (x :: y :: z :: nil)
```
</details>

Now, the argument. Assuming that one operation is a homomorphism over
the other: First, establish that the unit elements $1_\circ$ and
$1_\times$ coincide:

```
  module _ (p : {a b c d : S} → (a × b) · (c × d) ≡ (a · c) × (b · d)) where

    units-coincide : 1× ≡ 1·
    units-coincide =
      1×                    ≡⟨ ×-id-l ⟩
      1× × 1×               ≡⟨ ap₂ _×_ ·-id-r ·-id-l ⟩
      (1× · 1·) × (1· · 1×) ≡⟨ sym p ⟩
      (1× × 1·) · (1· × 1×) ≡⟨ ap₂ _·_ (sym ×-id-l) (sym ×-id-r) ⟩
      1· · 1·               ≡⟨ sym ·-id-l ⟩
      1·                    ∎
```

This lets us prove that the units follow a "crossed" identity law:

```
    ×-crossed-l : {x : S} → x ≡ 1· × x
    ×-crossed-l {x} = ×-id-l ∘p ap (λ e → e × x) units-coincide

    ×-crossed-r : {x : S} → x ≡ x × 1·
    ×-crossed-r {x} = ×-id-r ∘p ap (λ e → x × e) units-coincide

    ·-crossed-l : {x : S} → x ≡ 1× · x
    ·-crossed-l {x} = ·-id-l ∘p ap (λ e → e · x) (sym units-coincide)

    ·-crossed-r : {x : S} → x ≡ x · 1×
    ·-crossed-r {x} = ·-id-r ∘p ap (λ e → x · e) (sym units-coincide)
```

Establishing that, we can move onto the main argument.

```
    Eckmann-Hilton : {a b : S} → a · b ≡ b · a
    Eckmann-Hilton {a} {b} =
      a · b               ≡⟨ ap₂ _·_ ×-id-l ×-id-r ⟩
      (1× × a) · (b × 1×) ≡⟨ p ⟩
      (1× · b) × (a · 1×) ≡⟨ ap₂ _×_ (sym ·-crossed-l) (sym ·-crossed-r) ⟩
      b × a               ≡⟨ ap₂ _×_ ·-id-r ·-id-l ⟩
      (b · 1·) × (1· · a) ≡⟨ sym p ⟩
      (b × 1·) · (1· × a) ≡⟨ ap₂ _·_ (sym ×-crossed-r) (sym ×-crossed-l) ⟩
      b · a               ∎
```
