```agda
open import 1Lab.Data.Vector
open import 1Lab.Base

open import Algebra.Monoid
open import Algebra.Theory

open import Category.Instances.Cat.Cartesian
open import Category.Functor.Adjoints
open import Category.Equality
open import Category

module Algebra.Monoid.Endomorphism where
```

## The Endomorphism-Delooping adjunction

We extend the `deloop`{.Agda}ing to a functor $\mathrm{Mon} \to
\mathrm{Cat}_*$, and the endomorphism monoid to a functor
$\mathrm{Cat}_* \to \mathrm{Mon}$. These are adjoints.

### Delooping functor

The `Delooping`{.Agda} functor takes each monoid to its delooping. The
delooping is a `pointed category`{.Agda ident=Cat*} - in fact, it only
has the single point.

```agda
open Functor

Delooping : {ℓ ℓ' : _} → Functor (Mon {ℓ}) (Cat* {ℓ'} {ℓ})
F₀ Delooping x =
    deloop x
  , record { F₀   = λ x → tt
           ; F₁   = λ x₁ → Model.interpretation x unit nil
           ; F-id = refl
           ; F-∘  = λ f g → Model.sound x left-identity (_ :: nil)
           }
```

A `monoid`{.Agda} `Homomorphism`{.Agda} determines precisely the data of
a `Functor`{.Agda} between the `deloop`{.Agda}ings. This functor is
`based`{.Agda} - preserves the basepoint - since monoid homomorphisms
take `unit`{.Agda}s to units.

```agda
F₁ Delooping {x} {y} H = func , sym based where
  func : Functor (deloop x) (deloop y)
  F₀ func x = tt
  F₁ func f = Homomorphism.function H f
  F-id func = Homomorphism.homomorphic H _ _
  F-∘ func f g = Homomorphism.homomorphic H _ _

  based : _
  based = Functor≡ refl λ f → Homomorphism.homomorphic H _ _
F-id Delooping = Σ-Path (Functor≡ refl λ f → refl) (UIP _ _)
F-∘ Delooping f g = Σ-Path! (Functor≡ refl λ f → refl) (UIP _ _)
```

### Endomorphism monoid functor

```
Endomorphism : {ℓ ℓ' : _} → Functor (Cat* {ℓ'} {ℓ}) (Mon {ℓ})
```

The `endomorphism monoid functor`{.Agda ident=Endomorphism} takes a
pointed category to the `endomorphism monoid`{.Agda ident=Endo} on its
basepoint.

```
F₀ Endomorphism p = Endo (Σ.fst p) (F₀ (Σ.snd p) tt)
```

A functor between pointed categories determines exactly the data of a
monoid homomorphism between the corresponding endomorphism monoids.

```
F₁ Endomorphism {C , x} {D , y} (fst , snd)
  = record { function = hom snd
           ; homomorphic = λ where
              unit nil          → p snd
              ⊗ (a :: b :: nil) → q snd
           }
  where
    module C = Category.Category C
    module D = Category.Category D
```

This construction is slightly trickier since we have to use the fact
that a morphism in `Cat*`{.Agda} preserves basepoints.

```
    hom : (p : _) → C.Hom (F₀ x tt) (F₀ x tt) → D.Hom (F₀ y tt) (F₀ y tt)
    hom p x = subst (λ e → D.Hom e e) (sym (happly (ap F₀ p) tt)) (F₁ fst x)

    p : (snd : _) → hom snd (C.id {F₀ x tt}) ≡ D.id
    p refl = F-id fst

    q : {a b : _} → (snd : _) → hom snd (a C.∘ b) ≡ hom snd a D.∘ hom snd b
    q {a} {b} refl = F-∘ fst a b

F-id Endomorphism {fst , snd} = Homomorphism≡ λ x → substConst ∘p refl
F-∘ Endomorphism (f , refl) (g , refl) =
  Homomorphism≡ λ x → substConst ∘p refl 
```

### Adjunction

The adjunction can now be built.

```
Delooping⊣Endomorphism : {ℓ : _} → Delooping {ℓ} {ℓ} ⊣ Endomorphism
Delooping⊣Endomorphism {ℓ} = adj where
  open _⊣_
  adj : _ ⊣ _
```

The adjunction unit is a family of monoid homomorphisms between $M$ and
$\mathrm{Endo}(\mathbf{B}M, *)$. These are the same, up to a bit of
pattern matching, and a sneaky `subst`{.Agda} coming from the
`Endomorphism`{.Agda} functor.

```
  _=>_.η (_⊣_.unit adj) x =
    record { function    = λ x₁ → x₁
           ; homomorphic = λ where
              Monoid-operations.unit nil → refl
              ⊗ (x :: x₁ :: nil) → refl
           }
  _=>_.is-natural (_⊣_.unit adj) x y f = Homomorphism≡ λ x → sym substConst
```

The adjunction counit is a family of basepoint-preserving functors
between $\mathbf{B}(\mathrm{Endo}(C, *))$ and $C$. We're forced to take
the basepoint of the delooping to the delooping of the category, and
embed endomorphisms back into the category they came from (that's `λ x →
x`).

```
  _=>_.η (counit adj) (fst , snd) =
         record { F₀   = λ _ → F₀ snd tt
                ; F₁   = λ x → x
                ; F-id = refl
                ; F-∘  = λ f g → refl
                }
    , Functor≡ refl λ f → F-id snd
  _=>_.is-natural (counit adj) x (fst₁ , .(fst F∘ Σ.snd x)) (fst , refl) =
    Σ-Path! (Functor≡ refl λ f → refl) (UIP _ _)
```

The simplicity of these natural transformations makes Agda work out
`zig` and `zag` basically by itself, after the appropriate
characterisation of `_≡_`{.Agda} is invoked.

```
  zig adj = Σ-Path! (Functor≡ refl λ f → refl) (UIP _ _)
  zag adj = Homomorphism≡ λ x → substConst
```

<!--
```agda
_ = _≡_
_ = Homomorphism
```
-->
